<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>

        <c:set var="listaData" value="${requestScope.datas}" />
        <c:set var="tipoOrdenacao" value="${requestScope.ordenaTabela}" />
        <c:set var="usuario" value="${sessionScope.usuario}"/>
        <c:set var="nivel" value="${sessionScope.usuario.nivel}"/>
        <c:set var="listaZona" value="${requestScope.zonasList}" />


        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Consultar Mes�rios Volunt�rios</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <script language="javascript" type="text/javascript">

            function limparConsulta() {
                //    document.consultarMesario.tipoTelefone.ano[0].selected=true;
                var tabela = document.getElementById('tabela');
                var combo = document.getElementById('unidade');
                combo.selectedIndex = 0;


                var qtdLinhas = tabela.rows.length - 2;
                while (tabela.rows.length > 0)
                {
                    tabela.deleteRow(1);
                }

            }

            function exibirMensagem(msg)
            {
                alert(msg);
            }

            function novaJanela(titulo, nome, email, dddFone, telefone, dddFoneOpcional, foneOpcional, endereco, bairro, cidade, cep)
            {
                window.open("<%=basePath%>informacoesMesarioVoluntario.jsp?titulo=" + titulo + "&nome=" + nome + "&email=" + email + "&dddFone=" + dddFone + "&telefone=" + telefone + "&dddFoneOpcional=" + dddFoneOpcional + "&foneOpcional=" + foneOpcional + "&endereco=" + endereco + "&bairro=" + bairro + "&cidade=" + cidade + "&cep=" + cep + "",
                        "", "height = 270 , width = 630, left=100, top = 250, scrollbars = yes");
            }

            $(document).ready(function ()
            {
                //Oculta a Tabela quando n�o h� dados
                if ($("#mostraTabela").val() == "") {

                    $("#tabela").hide();
                }

                //var data = new Date();
                //var ano = data.getYear()+1900;
                if ($("#preencher").val() == "preencher") {
                    /* $("#unidade option").each(function(index){
                     if($(this).text() == ano){
                     $(this).attr("selected",true);
                     }
                     });*/
                    var ultimoElemento = $("#unidade option").size() - 1;
                    $("#unidade option:eq(" + ultimoElemento + ")").attr("selected", true);
                    //$("#unidade option").last().attr("selected",true);
                }

                //Pesquisa as datas para a zona escolhida pelo ADM
                $("#zonaEleitoral").change(function () {

                    $.ajax({
                        url: '${basePath}ServletConsultarMesarioVoluntario.do',
                        type: "GET",
                        dataType: 'json',
                        data: {
                            'zona': $('#zonaEleitoral').val(),
                            'chamadaAjax': 'true'

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {

                            alert("erro" + textStatus);
                        },
                        success: function (resposta) {

                            $('#unidade').empty();
                            $('#unidade').append($('<option>', {
                                value: '0',
                                text: 'Selecione'
                            }));

                            $.each(resposta.datas, function (i, valor) {

                                $('#unidade').append($('<option>', {
                                    value: valor,
                                    text: valor
                                }));

                            });

                        }
                    });
                });

            });

            function  verificaDados()
            {
                //Verificar se o usu�rio selecionou alguma zona eleitoral.
                if (document.getElementById("nivel2").value == "2" && document.consultarMesario.zonaEleitoral.value == "0")

                {
                    alert("Por favor selecione alguma zona eleitoral v�lida.");
                    return false;
                }

                //Verificar se o usu�rio selecionou algum ano.
                if (document.consultarMesario.unidade.value == "0")

                {
                    alert("Por favor selecione algum ano de inscri��o v�lido.");
                    return false;
                } else {
                    document.getElementById("consultarMesario").action = "${basePath}ServletConsultarMesarioVoluntario.do";
                    return true;
                }
            }

            function mostraTabela()
            {
                $("#tabela").show();
            }


            //Envia a zona eleitoral para gerar o relat�rio
            function gerarRelatorio()
            {
                document.getElementById("consultarMesario").action = "${basePath}ServletGerarRelatorioConsulta.do";
                document.getElementById("consultarMesario").submit();
            }

            function gerarRelatorioCSV()
            {
                document.getElementById("consultarMesario").action = "${basePath}ServletGerarRelatorioConsultaCSV.do";
                document.getElementById("consultarMesario").submit();
            }
        </script>

    </head>

    <% if (mensagem != null) {%> <body onload="javascript:exibirMensagem('<%=mensagem%>')" > <% } else { %> <body> <% }%>

            <c:if test="${sessionScope.usuario==null}">
                <c:redirect url="${basePath}usuarioNaoLogado.jsp"/>
            </c:if>

            <%@include file="/WEB-INF/includes/debug.jsp"%>

            <div id="tudo">
                <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
                <%@include file="/WEB-INF/includes/topo.jsp"%>
                <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->


                <div id="mesario" >

                    <div id="voltar_sair"><a href="${basePath}menu.jsp" id="voltar">Menu</a><a href="${basePath}encerrarSessao.jsp" id="sair">Sair</a></div></br></br>
                    <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Consultar Mes�rios Volunt�rios</h1>

                    <c:if test="${nivel eq 1}">
                        <h2>${usuario.zona.numero}� Zona Eleitoral</h2>
                    </c:if>

                    <form id="consultarMesario" action="${basePath}ServletConsultarMesarioVoluntario.do" method="post" name="consultarMesario" onsubmit="return verificaDados();">
                        <input type="hidden" value="false" id="teste"></input>
                        <input type="hidden" value="${requestScope.datas}" id="data"></input>
                        <input type="hidden" value="${requestScope.preencher}" id="preencher"></input>
                        <input type="hidden" value="${requestScope.mostraTabela}" id="mostraTabela"></input>
                        <input type="hidden" value="${sessionScope.usuario.nivel}" id="nivel2"></input>


                        <c:if test="${nivel eq 2}">
                            <p>
                                <label for="unidade">Zona Eleitoral: </label>

                                <select name="zona"  id="zonaEleitoral"   style=" height : 20px;" >
                                    <option value="0" selected >Selecione</option>
                                    <c:forEach items="${listaZona}" var="zonas">
                                        <option value="${zonas.numZona}"  <c:if test="${param.zona eq zonas.numZona}">selected="selected"</c:if> >${zonas.numZona}� Zona</option>
                                    </c:forEach>
                                </select>
                            </p>
                        </c:if>

                        <p>
                            <label for="unidade">Ano de Inscri��o: </label>

                            <select name="ano"  id="unidade"   style=" height : 20px;  width : 375px;">
                                <option value="0"  >Selecione</option>
                                <c:forEach items="${listaData}" var="datas">
                                    <option value="${datas}" <c:if test="${param.ano eq datas}">selected="selected"</c:if> >${datas}</option>
                                </c:forEach>
                            </select>

                        </p>


                        <fieldset class="radio_menores">
                            <legend>Ordenar por:</legend>
                            <p class="radios_inline">
                                <input id="data_local" type="radio" name="ordenaTabela" value="1" <c:if test="${requestScope.ordenaTabela eq 1 || requestScope.ordenaTabela eq null}">checked</c:if> /><label for="data_local">Data de Inscri��o + Local de Vota��o</label>
                                <input id="data"       type="radio" name="ordenaTabela" value="2" <c:if test="${requestScope.ordenaTabela eq 2}">checked</c:if> /><label for="data">Data de Inscri��o </label>
                                    <br/>
                                    <input id="local"      type="radio" name="ordenaTabela" value="3" <c:if test="${requestScope.ordenaTabela eq 3}">checked</c:if> /><label for="local" class="alinha_radio" >Local de Vota��o</label>
                                <input id="alfabetica" type="radio" name="ordenaTabela" value="4"  <c:if test="${requestScope.ordenaTabela eq 4}">checked</c:if> /><label for="alfabetica"  >Ordem Alfab�tica</label>

                                </p>
                            </fieldset>

                            <p>
                                <input type="submit" value="Pesquisar" class="botoes_form" onclick="mostraTabela()" />
                                <input type="button" value="Limpar" class="botoes_form" onclick="limparConsulta()" />
                            </p>

                        </form>

                        <table border="0" cellpadding="0" cellspacing="0" id="tabela" name="tabela">
                            <caption>Mes�rios Inscritos <a href="javascript:gerarRelatorio();" class="imprimir" >Gerar Relat�rio</a>  <a href="javascript:gerarRelatorioCSV();" class="imprimir" >Exportar para CSV</a></caption>
                            <thead>
                                <tr>
                                    <th id="titulo_eleitoral">T�tulo Eleitoral</th>
                                    <th id="nome_mesario">Nome</th>
                                    <th id="inscricao">Data de Inscri��o</th>
                                    <th id="local_votacao">Local de Vota��o</th>
                                    <th id="secao">Se��o</th>
                                    <th id="ocupacao">Ocupa��o</th>
                                    <th id="nascimento">Data Nascimento</th>
                                    <th id="universitario">Universit�rio</th>
                                    <th></th>
                                </tr>

                            </thead>
                            <tbody>
                                <tr style="background: url('layout_aplicacoes/imagens/fundo_linhas_geral_B3.jpg') repeat scroll 0% 0% transparent;">
                                <c:forEach items="${requestScope.listaEleitores}" var="eleitor">

                                    <tr>
                                        <td headers="titulo_eleitoral">
                                            ${eleitor.titulo}
                                        </td>

                                        <td headers="nome_mesario">
                                            ${eleitor.nome}
                                        </td>

                                        <td headers="inscricao">
                                            ${eleitor.dataCadastro}
                                        </td>

                                        <td headers="local_votacao">
                                            ${eleitor.numLocalVotacao} - ${eleitor.localVotacao}
                                        </td>

                                        <td headers="secao">
                                            ${eleitor.secaoEleitoral}
                                        </td>

                                        <td headers="ocupacao">
                                            <c:if test="${eleitor.ocupacao == null}">
                                                N�o Informado.
                                            </c:if>
                                            ${eleitor.ocupacao}
                                        </td>

                                        <td>
                                            <fmt:formatDate pattern="dd/MM/yyyy" value="${eleitor.dataNascimento}"/>
                                        </td>

                                        <td headers="universitario">

                                            <c:if test="${eleitor.universitario == 1}">
                                                Sim
                                            </c:if>
                                            <c:if test="${eleitor.universitario != 1}">
                                                N�o
                                            </c:if>
                                        </td>

                                        <td>
                                            <img src="${basePath}images/icone_mais.gif" title="Visualizar contato" onclick="javascript:novaJanela('${eleitor.titulo}', '${eleitor.nome}', '${eleitor.email}', '${eleitor.dddFone}', '${eleitor.telefone}', '${eleitor.dddFoneOpcional}', '${eleitor.foneOpcional}', '${eleitor.endereco}', '${eleitor.bairro}', '${eleitor.cidade}', '${eleitor.cep}')"/>
                                        </td>

                                    </tr>
                                </c:forEach>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
                <%@include file="/WEB-INF/includes/rodape.jsp"%>
                <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
            </div>

        </body>

</html>
