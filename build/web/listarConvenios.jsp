<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp" %>
        <%@include file="/WEB-INF/includes/linkScript.jsp" %>

        <c:set var="listaUniversidades" value="${requestScope.listaUniversidades}" />

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp" %> - Analisar Conv�nio</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

    </head>

    <%if (mensagem != null) {%> <body onload="alert('<%=mensagem%>')"> <%} else {%> <body> <%}%>

            <c:if test="${sessionScope.usuario==null}">
                <c:redirect url="${basePath}usuarioNaoLogado.jsp"/>
            </c:if>

            <c:if test="${!sessionScope.grupoGt.contains(sessionScope.usuario.login)}">
                <c:redirect url="${basePath}usuarioSemPermissao.jsp"/>
            </c:if>

            <%@include file="/WEB-INF/includes/debug.jsp" %>

            <div id="tudo">
                <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
                <%@include file="/WEB-INF/includes/topo.jsp" %>
                <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

                <form id="form1" method="post" action="${basePath}ServletPreencherEditarConvenio.do">
                    <input type="hidden" name="universidadeSelecionada" value="" />
                    <input type="hidden" name="analisarConvenio" value="" />
                    <div id="mesario">

                        <div id="voltar_sair"><a href="${basePath}menu.jsp" id="voltar">Menu</a><a href="${basePath}encerrarSessao.jsp" id="sair">Sair</a></div></br></br>
                        <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp" %> - An�lise de Conv�nio</h1>

                        <table id="analisar_convenio" border="0" cellpadding="0" cellspacing="0">
                            <caption>Institui��es de Ensino Superior Conveniadas</caption>
                            <thead>
                                <tr>
                                    <th id="editar"></th>
                                    <th id="numero">N�mero</th>
                                    <th id="nome_convenio">Institui��o</th>
                                    <th id="cnpj">CNPJ</th>
                                    <th id="situacao">Situa��o</th>
                                    <th id="data_inicial">Data Inicial</th>
                                    <th id="data_final">Data Final</th>
                                </tr>
                            </thead>
                            <!-- foreach -->

                            <tbody>
                                <c:forEach var="universidade" items="${listaUniversidades}">
                                    <tr style="background: url('layout_aplicacoes/imagens/fundo_linhas_geral_B3.jpg') repeat scroll 0% 0% transparent;">
                                        <td headers="editar">
                                            <a href="${basePath}ServletPreencherEditarConvenio.do?universidadeSelecionada=${universidade.codObjeto}"><img src="${basePath}layout_aplicacoes/imagens/lapis.gif" title="Editar" border="0" /></a>
                                            <a href="${basePath}ServletGerarTermoConvenio.do?universidadeSelecionada=${universidade.codObjeto}"><img  title="Regerar Termo de Conv�nio" src="${basePath}layout_aplicacoes/imagens/icone_regerar.gif" border="0"/></a>
                                            <!--<c:if test="${universidade.convenioValido ne 'V�lido'}"><img  title="Conv�nio inv�lido, imposs�vel regerar Termo de Conv�nio" src="/layout_aplicacoes/imagens/icone_regerar2.gif" border="0"/></c:if>-->
                                            <a href="${basePath}ServletPreencherValidarConvenio.do?universidadeSelecionada=${universidade.codObjeto}" ><img src="${basePath}layout_aplicacoes/imagens/icone-mesario-valido.gif" title="Validar Conv�nio" border="0" /></a>
                                        </td>
                                        <td headers="numero">${universidade.numeroAnoConvenio}</td>
                                        <td headers="nome_convenio">${universidade.sigla} - ${universidade.nomeFantasia}</td>
                                        <td headers="cnpj">${universidade.cnpj}</td>
                                        <td headers="situacao">${universidade.convenioValido}</td>
                                        <td headers="data_inicial"><c:if test="${universidade.dataInicialConvenio.date < 10}">0</c:if>${universidade.dataInicialConvenio.date} / <c:if test="${universidade.dataInicialConvenio.month+1 < 10}">0</c:if>${universidade.dataInicialConvenio.month+1} / ${universidade.dataInicialConvenio.year+1900}</td>
                                        <td headers="data_final"><c:if test="${universidade.dataFinalConvenio.date < 10}">0</c:if>${universidade.dataFinalConvenio.date} / <c:if test="${universidade.dataFinalConvenio.month+1 < 10}">0</c:if>${universidade.dataFinalConvenio.month+1} / ${universidade.dataFinalConvenio.year+1900}</td>
                                        </tr>
                                </c:forEach>
                            </tbody>

                        </table>
                    </div>

                </form>

                <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
                <%@include file="/WEB-INF/includes/rodape.jsp" %>
                <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
            </div>
        </body>

</html>