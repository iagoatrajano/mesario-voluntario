<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>

        <c:set var="listaUniversidades" value="${requestScope.listaUniversidades}" />

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp" %> - Conv�nios V�lidos</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

    </head>

    <% if (mensagem != null) {%> <body onload="alert('<%=mensagem%>')" >  <% } else { %> <body> <% }%>

            <c:if test="${sessionScope.usuario==null}">
                <c:redirect url="${basePath}usuarioNaoLogado.jsp"/>
            </c:if>

            <c:if test="${!sessionScope.grupoGt.contains(sessionScope.usuario.login)}">
                <c:redirect url="${basePath}usuarioSemPermissao.jsp"/>
            </c:if>

            <%@include file="/WEB-INF/includes/debug.jsp" %>

            <div id="tudo">
                <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
                <%@include file="/WEB-INF/includes/topo.jsp" %>
                <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->


                <div id="mesario">

                    <div id="voltar_sair"><a href="${basePath}menu.jsp" id="voltar">Menu</a><a href="${basePath}encerrarSessao.jsp" id="sair">Sair</a></div></br></br>
                    <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp" %> - Conv�nios V�lidos</h1>

                    <table id="consultar_convenio" border="0" cellpadding="0" cellspacing="0">
                        <caption>Institui��es de Ensino Superior Conveniadas</caption>
                        <thead>
                            <tr>
                                <!--<th id="editar"></th>-->
                                <th id="numero">N�mero</th>
                                <th id="nome_convenio">Institui��o</th>
                                <th id="cnpj">CNPJ</th>
                                <!--<th id="situacao">Situa��o</th>-->
                                <th id="data_inicial">Data Inicial</th>
                                <th id="data_final">Data Final</th>
                            </tr>
                        </thead>
                        <!-- foreach -->

                        <tbody>
                            <c:forEach var="universidade" items="${listaUniversidades}">
                                <tr style="background: url('layout_aplicacoes/imagens/fundo_linhas_geral_B3.jpg') repeat scroll 0% 0% transparent;">

                                    <td headers="numero">${universidade.numeroAnoConvenio}</td>
                                    <td headers="nome_convenio">${universidade.sigla} - ${universidade.nomeFantasia}</td>
                                    <td headers="cnpj">${universidade.cnpj}</td>
                                    <!--<td headers="situacao">${universidade.convenioValido}</td>-->
                                    <td headers="data_inicial"><c:if test="${universidade.dataInicialConvenio.date < 10}">0</c:if>${universidade.dataInicialConvenio.date} / <c:if test="${universidade.dataInicialConvenio.month+1 < 10}">0</c:if>${universidade.dataInicialConvenio.month+1} / ${universidade.dataInicialConvenio.year+1900}</td>
                                    <td headers="data_final"><c:if test="${universidade.dataFinalConvenio.date < 10}">0</c:if>${universidade.dataFinalConvenio.date} / <c:if test="${universidade.dataFinalConvenio.month+1 < 10}">0</c:if>${universidade.dataFinalConvenio.month+1} / ${universidade.dataFinalConvenio.year+1900}</td>

                                    </tr>
                            </c:forEach>
                            <tr>
                                <td></td>
                                <td class="destaque_total"> Total </td>
                                <td></td>
                                <td></td>
                                <td class="destaque_total">${fn:length(listaUniversidades)} </td>
                            </tr>
                        </tbody>

                    </table>
                </div>
                <!--</form>-->

                <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
                <%@include file="/WEB-INF/includes/rodape.jsp" %>
                <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
            </div>

        </body>
</html>