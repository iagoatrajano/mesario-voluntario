<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@page session="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp" %>
        <%@include file="/WEB-INF/includes/linkScript.jsp" %>

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Cadastro de Mes�rio Volunt�rio</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


        <script type="text/javascript" language="javascript">
            <!--
          function teclaPressionada() {
                /*if(window.event.keyCode == 13){
                 consultarEleitor();
                 }*/
            }

            function consultarEleitor()
            {
                if (document.form1.titulo.value != null
                        && (document.form1.titulo.value == ""
                                || document.form1.titulo.value.trim == "")) {
                    document.form1.titulo.focus();
                    alert('Aten��o: O campo <N� T�tulo de Eleitor> n�o foi preenchido!');
                    location.href = "${basePath}informaTitulo.jsp";
                } else {
                    if (!ehNumero(document.form1.titulo, 'N� T�tulo de Eleitor')) {
                        document.form1.titulo.focus();
                        return;
                    }

                    var ehTituloValido = formatarTitulo(document.form1.titulo.value);

                    if (!ehTituloValido) {  //titulo invalido
                        //alert('O t�tulo digitado n�o � v�lido!');
                        alert('T�tulo inv�lido! Verifique a numera��o correta de seu t�tulo.');
                        document.form1.titulo.focus();
                        document.form1.titulo.select();
                        return;
                    } else {
                        document.form1.submit();
                        //location.href="cadastroMesario.jsp";
                    }
                }
            }

            function exibirMensagem(msg)
            {
                //alert(msg);
                //window.location.href = "./informaTitulo.jsp";
                alert(msg);
                //return;
            }
            //-->
        </script>

    </head>



    <% if (mensagem != null) {%> <body onload="javascript:exibirMensagem('<%=mensagem%>')" > <% } else { %> <body>  <% }%>

        <%@include file="/WEB-INF/includes/debug.jsp" %>

        <div id="tudo">

            <!-- T�TULO -->
            <div id="mesario">
                <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Cadastro de Mes�rio Volunt�rio</h1>

                <!--           <td align="left" valign="top"> &nbsp;&nbsp;-->
                <!--           <% if (mensagem == null) { %>-->
                <!--           <img alt="imprimir" src="images/aviso.gif" width="18" height="18" align="middle">-->
                <!--           <% } else {%>-->
                <!--           <img alt="imprimir" src="./../images/aviso.gif" width="18" height="18" align="middle">-->
                <!--           <% }%>-->
                <p>Informe o n�mero do seu t�tulo de eleitor e, em seguida, clique em Pesquisar.</p>
                <form name="form1" method="post" action="${basePath}ServletConsultarDadosEleitor.do">

                    <p><label for="endereco">N� T�tulo de Eleitor:</label><input type="text" name="titulo" size="30"  maxlength="13" onkeyup="teclaPressionada()"></p>
                    <p>
                        <input type="button" class="botoes_form" value="Pesquisar" onclick="consultarEleitor()">
                    </p>
                </form>
            </div>

        </div>
    </body>

</html>

