/**
 * Criado inicialmente para inserir imagem com title explicativo dentro da tag <a> que tenha href apontando para 
 * fora do TRE-PE [ (http://www.tre.pe.gov.br) e (http://intranet.tre-pe.gov.br) e
 * (http://peweb01.tre-pe.gov.br) e (http://boulevard.tre-pe.gov.br) ] 
 * <img src="/layout_aplicacoes/imagens/new_window_blue.gif" alt="Link será aberto em nova janela" title="Link será aberto em nova janela"/>  
 */
    $(
	 function()
	 {
		// Se link aponta para novo site
		$('a').not('a[href^=file], a[href^=#], a[href^=http://pebd03], a[href^=http://www.tre-pe.gov.br], a[href^=http://intranet], a[href^=http://peweb01], a[href^=http://boulevard]').each(function(){$(this).attr('title',$(this).attr('title')+' (Link para site externo)');$(this).html($(this).html()+'<img src="/layout_aplicacoes/imagens/new_window_blue.gif" alt="Link para site externo" title="Link para site externo"/>')});
		// Se link aponta para nova janela 
		$('a[target=_blank]').each(function(){$(this).attr('title', $(this).attr('title')+' (Link será aberto em nova janela)')});
     }
	);
