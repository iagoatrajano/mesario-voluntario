<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<?xml version="1.0" encoding="iso-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>

        <%            String erro = "";
            erro = (String) request.getAttribute("erro");
            String tela = (String) request.getAttribute("tela");
            if (tela == null) {
                tela = "";
            }
        %>

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

    </head>

    <body class="transparente" style="text:'#000000'; leftmargin:21; rightmargin:2;">

        <%@include file="/WEB-INF/includes/debug.jsp" %>

        <div id="tudo">
            <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
            <%@include file="/WEB-INF/includes/topo.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

            <div id="mesario">

                <div id="voltar_sair"><a href="${basePath}" id="sair">Login</a></div></br></br>
                <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp"%></h1>


                <h3> Usu�rio n�o logado.</h3>
            </div>

            <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
            <%@include file="/WEB-INF/includes/rodape.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
        </div> <!-- fim da div#tudo -->

    </body>
</html>
