<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<?xml version="1.0" encoding="iso-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>

        <%            String erro = "";
            erro = (String) request.getAttribute("erro");
            String tela = (String) request.getAttribute("tela");
            if (tela == null) {
                tela = "";
            }
        %>

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

    </head>

    <body class="transparente" style="text:'#000000'; leftmargin:21; rightmargin:2;">

        <%@include file="/WEB-INF/includes/debug.jsp" %>

        <div id="tudo">
            <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
            <%@include file="/WEB-INF/includes/topo.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

            <h1>Cadastro de Mes�rio Volunt�rio</h1>

            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2" align="left" valign="top">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td align="left" valign="top" class="subtitulo"></td>
                    <td align="right" valign="top" class="subtitulo">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="21">&nbsp;</td>
                    <td colspan="2" align="left" valign="top" class="fonteazulclara"><b class="subtitulo"><span class="fonteazulclara">&nbsp;</span></b></td>
                    <td width="21">&nbsp;</td>
                </tr>
                <tr>
                    <td width="21">&nbsp;</td>
                    <%if (tela.equals("internet")) { %>
                    <td align="right" valign="top" class="subtitulo"><!-- a href="../sigweb/telaInternet.jsp">Menu</a --></td>
                    <%} else if (tela.equals("internet2")) { %>
                    <td align="right" valign="top" class="subtitulo"><!-- a href="../sigweb/telaInternet2.jsp">Menu</a --></td>
                    <%} else { %>
                    <td align="right" valign="top" class="subtitulo"><!-- a href="../sigweb/telaPrincipal.jsp">Menu</a --></td>
                    <%}%>
                    <td width="21">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3" align="left" valign="top">
                        <table border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td align="left" class="fonte-cinza" > Ocorreu um erro na aplica��o. Informe ao Analista respons�vel a seguinte mensagem:</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="5"><b> <%=erro%></b></td>
                            </tr>
                        </table>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>

            <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
            <%@include file="/WEB-INF/includes/rodape.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
        </div> <!-- fim da div#tudo -->

    </body>
</html>
