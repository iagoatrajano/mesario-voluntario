<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>

        <%
            String login = null;
            login = (String) request.getAttribute("login");
            //
            if (login == null) {
                login = "";
            }
        %>

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Alterar a Senha</title>

        <!--[if lte IE 6]>
              <script src="/layout_aplicacoes/js/DD_belatedPNG_0.0.7a.js" type="text/javascript" charset="iso-8859-1"></script>
                  <script type="text/javascript">
                  DD_belatedPNG.fix('#topo, #caixa_assinatura, img');
                  </script>
        <![endif]-->

    </head>

    <%if (mensagem != null) {%> <body onload="javascript:exibirMensagem('<%=mensagem%>')">  <%} else {%>  <body>  <%}%>

            <div id="tudo">
                <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
                <%@include file="/WEB-INF/includes/topo.jsp"%>
                <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

                <div id="login" class="mesario_login">

                    <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Alterar Senha</h1>

                    <form class="altera_senha" name="form1" id="form1" method="post" action="${basePath}ServletAlterarSenha.do">

                        <input type="hidden" name="login" value="" />
                        <input type="hidden" name="passwordAntiga" value="" />
                        <input type="hidden" name="passwordNova" value="" />
                        <input type="hidden" name="senhaExpirada" value="0" />

                        <fieldset>
                            <legend>Controle de Acesso - <strong>Altera��o de Senha:</strong></legend>

                            <p><label for="usuario">Usu�rio:</label><input type="text" maxlength="15" title="Digite seu nome de usu�rio" id="usuario" name="usuario" value="<%=login%>"/></p>
                            <p><label for="senhaAntiga">Senha Atual:</label><input type="password" maxlength="8" title="Digite sua senha atual" id="senhaAntiga" name="senhaAntiga"/></p>

                            <p><label for="senhaNova">Senha Nova:</label><input type="password" maxlength="8" title="Digite sua nova senha" id="senhaNova" name="senhaNova"/></p>

                            <p><label for="confirmacaoSenha">Confirma��o Senha Nova:</label><input type="password" maxlength="8" title="Confirme sua nova senha" id="confirmacaoSenha" name="confirmacaoSenha"/></p>

                            <input type="button" id="alterar" title="Altere a senha" class="botoes_form" value="Alterar" onclick="javascript:validar()" />
                            <input type="reset" id="limpar" title="Limpe os dados do formul�rio" class="botoes_form" value="Limpar" onclick="javascript:limpar()" />

                        </fieldset>
                    </form>
                </div>


                <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
                <%@include file="/WEB-INF/includes/rodape.jsp"%>
                <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
            </div> <!-- fim da div#tudo -->
        </body>
</html>
