/* Fun��es auxiliares para valida��o de campos */
String.prototype.trim = function() {
	return this.replace(/^\s*/,'').replace(/\s*$/,'');
};

function campoPreenchido(campo) {
	return (campo.value.trim().length > 0) ? true : false;
}

function apresentaTextoFaltaPreenchimento(campo, textoCompl) {
	var msg = "Por favor, preencha o campo " + textoCompl + ".";
	alert(msg);
	campo.focus();
	campo.select();
}

function campoTextoObrigatorio(campo, textoCompl) {
	var ret = true;
	if (!campoPreenchido(campo)) {
		apresentaTextoFaltaPreenchimento(campo, textoCompl);
		ret = false;
	}
	return ret;
}


function exibirMensagem(msg) {
	alert(msg);
	var form = document.getElementById("form1");
	form.senhaExpirada.value = "1";
	form.senhaAntiga.focus();
}

/* Carregar fun��o do onload */
function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != "function") {
		window.onload = func;
	}
	else {
		window.onload = function() {
			oldonload();
			func();
		};
	}
}

function addEventLimpar() {
	var limpar = document.getElementById("limpar");
	if(limpar !== null) {
		limpar.onclick = limpar;
	}
}

function addEventValidar() {
	var limpar = document.getElementById("validar");
	if(limpar !== null) {
		limpar.onclick = validar;
	}
}

function validarLogin()
{

    if( (document.form_login.usuario.value == "") &&
        (document.form_login.senha.value == "")
       ){
         document.form_login.usuario.focus();
         alert('Aten��o: Nenhum campo preenchido!');
         return false;

    }else if ( (document.form_login.usuario.value != "") ||
               (document.form_login.senha.value != "")
              ){
              if(!campoTextoObrigatorio(document.form_login.usuario, "Usu�rio")){
                      return false;
              }else if(!campoTextoObrigatorio(document.form_login.senha, "Senha")){
                      return false;
              }
    }
    
    document.form_login.login.value = document.form_login.usuario.value;
    document.form_login.password.value = document.form_login.senha.value;

    document.form_login.submit();
}

function enterLogin()
{
	  if (window.event && window.event.keyCode == 13){
		  	validarLogin();
	  }
}


function limparLogin( )
{
    document.form_login.usuario.value = "";
    document.form_login.senha.value = "";
}


function validar() {
	var form = document.getElementById("form1");
	var usuario = form.usuario;
	var senhaAntiga = form.senhaAntiga;
	var senhaNova = form.senhaNova;
	var confirmacaoSenha = form.confirmacaoSenha;
	if((usuario.value === "") && (senhaAntiga.value === "") && (senhaNova.value === "") && (confirmacaoSenha.value === "")) {
		usuario.focus();
		alert('Aten��o: Nenhum campo preenchido!');
		return false;
	} else if(!campoTextoObrigatorio(usuario, "Usu�rio") && !campoTextoObrigatorio(senhaAntiga, "Senha Antiga") && !campoTextoObrigatorio(senhaNova, "Senha Nova") && !campoTextoObrigatorio(confirmacaoSenha, "Confirma��o Senha Nova")) {
		return false;		
	}
    
	if(senhaNova.value != confirmacaoSenha.value) {
		senhaNova.focus();
		alert('Aten��o: Dados inv�lidos! A nova senha e a confirma��o n�o conferem. Corrija e tente novamente!');
		return false;
	} else if(senhaNova.value.length < 5) {
		senhaNova.focus();
		alert('Aten��o: Dados inv�lidos! A nova senha e a confirma��o tem menos que 5 caracteres. Corrija e tente novamente!');
		return false;
	} else if(senhaNova.value == senhaAntiga.value) {
		senhaNova.focus();
		alert('Aten��o: Dados inv�lidos! A nova senha deve ser diferente da antiga. Corrija e tente novamente!');
		return false;
	}
    
	var login = form.login;
    var passwordAntiga = form.passwordAntiga;
	var passwordNova = form.passwordNova;
	login.value = usuario.value;
	passwordAntiga.value = senhaAntiga.value;
	passwordNova.value = senhaNova.value;
	
	form.submit();
}

function limpar() {
	var form = document.getElementById("form1");
	form.usuario.value = "";
	form.senhaAntiga.value = "";
	form.senhaNova.value = "";
	form.confirmacaoSenha.value = "";
}


addLoadEvent(addEventLimpar);
addLoadEvent(addEventValidar);
