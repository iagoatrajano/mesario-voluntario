<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>

        <c:set var="universidade" value="${requestScope.universidade}" />
        <c:set var="desabilitado" value="${requestScope.disabled}" />

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Analisar Conv�nio</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


        <script language="javascript" type="text/javascript">
            jQuery(function ($) {
                $("#cpf").mask("?999.999.999-99");
                $("#cnpj").mask("?99.999.999/9999-99");

            });

            function isEmpty(palavra) {
                if (palavra == "" || palavra.trim == "") {
                    return true;
                } else {
                    return false;
                }
            }
            function validarRadioBtn() {

                if (document.cadastroConvenio.tipoPessoa[0].checked) {
                    return true;
                } else if (document.cadastroConvenio.tipoPessoa[1].checked) {
                    return true;
                } else {
                    alert("Por favor,seleciona alguma das op��es do Tipo de Pessoa Juridica.");
                    return false;
                }

            }

            function validarCadastro() {

                if (isEmpty(document.cadastroConvenio.razaoSocial.value) || isEmpty(document.cadastroConvenio.nomeFantasia.value) ||
                        isEmpty(document.cadastroConvenio.sigla.value) || isEmpty(document.cadastroConvenio.cargo) ||
                        isEmpty(document.cadastroConvenio.nomeRep.value) || isEmpty(document.cadastroConvenio.endereco.value) ||
                        isEmpty(document.cadastroConvenio.bairro.value) || isEmpty(document.cadastroConvenio.cidade.value) ||
                        isEmpty(document.cadastroConvenio.cep.value) || isEmpty(document.cadastroConvenio.nacionalidade.value) ||
                        isEmpty(document.cadastroConvenio.rg.value) || isEmpty(document.cadastroConvenio.orgaoExp.value) ||
                        isEmpty(document.cadastroConvenio.cpf.value) || (!(validarRadioBtn())) ||
                        isEmpty(document.cadastroConvenio.cnpj.value)) {

                    alert("OBS.: Todas as informa��es abaixo devem ser preenchidas para que o cadastro seja concluido com sucesso.");
                    return false;
                } else {

                    if (!validarCNPJ(document.cadastroConvenio.cnpj.value)) {
                        document.cadastroConvenio.cnpj.focus();
                        alert("OBS.: O CNPJ informado n�o � v�lido.");
                        return false;
                    } else if (document.cadastroConvenio.cep.value.length != 9) {

                        if (document.cadastroConvenio.cep.value.length < 9) {
                            alert("OBS.: O campo CEP cont�m menos que oito d�gitos..");
                            document.cadastroConvenio.cep.focus;
                            return false;
                        } else {
                            alert("OBS.: O campo CEP cont�m mais que oito d�gitos..");
                            document.cadastroConvenio.cep.focus;
                            return false;
                        }
                        return false;

                    } else if (!validarCPF(document.cadastroConvenio.cpf.value)) {
                        document.cadastroConvenio.cpf.focus();
                        alert("OBS.: O cpf informado n�o � v�lido.");
                        return false;
                    } else if (isNaN(document.cadastroConvenio.rg.value)) {
                        document.cadastroConvenio.rg.focus();
                        alert("OBS.: O RG deve ser todo numerico.");
                        return false;
                    } else if (document.cadastroConvenio.emailConvenio.value != null && document.cadastroConvenio.emailConvenio.value != "") {
                        var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

                        if (!filtro.test(document.cadastroConvenio.emailConvenio.value)) {
                            alert("O endere�o de e-mail n�o � v�lido!");
                            document.cadastroConvenio.emailConvenio.focus();
                            return false;
                        }
                    }

                }



            }



            function exibirMensagem(msg) {
                alert(msg);

            }

        </script>


    </head>

    <%if (mensagem != null) {%>  <body onload="alert('<%=mensagem%>')">   <% } else { %> <body> <% }%>

            <c:if test="${sessionScope.usuario==null}">
                <c:redirect url="${basePath}usuarioNaoLogado.jsp"/>
            </c:if>

            <c:if test="${!sessionScope.grupoGt.contains(sessionScope.usuario.login)}">
                <c:redirect url="${basePath}usuarioSemPermissao.jsp"/>
            </c:if>

            <%@include file="/WEB-INF/includes/debug.jsp" %>

            <div id="tudo">
                <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
                <%@include file="/WEB-INF/includes/topo.jsp" %>
                <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

                <div id="mesario">
                    <div id="voltar_sair"><a href="${basePath}ServletPreencherListarConvenios.do" id="voltar">Voltar</a><a href="${basePath}menu.jsp" id="voltar">Menu</a><a href="${basePath}encerrarSessao.jsp" id="sair">Sair</a></div></br></br>
                    <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp" %> - An�lise de Conv�nio</h1>

                    <form id="cadastroConvenio" name="cadastroConvenio" method="post" action="${basePath}ServletEditarConvenio.do"  onSubmit="return validarCadastro()">
                        <input type="hidden" name="codObjeto" value="${universidade.codObjeto}"/>
                        <input type="hidden" name="flag" value="${requestScope.flag}"/>
                        <fieldset>
                            <legend>Dados da Entidade</legend>
                            <p><label for="cnpj">CNPJ: </label><input id="cnpj" type="text" name="cnpj" value="${universidade.cnpj}" ${desabilitado}></input></p>
                            <p><label for="razao_social">Raz�o Social: </label><input id="razao_social" ${desabilitado} name="razaoSocial" type="text" value="${universidade.razaoSocial}"></input></p>
                            <p><label for="nome_fantasia">Nome Fantasia: </label><input id="nome_fantasia" ${desabilitado} name="nomeFantasia" type="text" value="${universidade.nomeFantasia}"></input></p>
                            <p><label for="sigla">Sigla: </label><input id="sigla" ${desabilitado} name="sigla" type="text" value="${universidade.sigla}"></input></p>
                            <p><label for="phone">Telefone: </label><input ${desabilitado} id="phone" name="phone" type="text" style="width: 30%;" value="${universidade.telefone}"></input></p>
                            <p><label for="email">E-mail: </label><input ${desabilitado} id="emailConvenio" name="emailConvenio" type="text" style="width: 45%;"  value="${universidade.email}"></input></p>
                            <p class="radios_inline">
                                <span>Pessoa Jur�dica:</span>
                                <span class="campos_inline">
                                    <input id="publico" type="radio" ${desabilitado} name="tipoPessoa" value="2" <c:if test="${universidade.tipoPessoaJuridica eq 2}">checked="checked"</c:if> /><label for="sim">de Direito P�blico</label>
                                    <input id="privado" type="radio" ${desabilitado} name="tipoPessoa" value="1" <c:if test="${universidade.tipoPessoaJuridica eq 1}">checked="checked"</c:if> /><label for="nao">de Direito Privado</label>
                                    </span>
                                </p>
                            </fieldset>
                            <fieldset>
                                <legend>Dados do Representante</legend>
                                <p>
                                    <label for="titulo_representante">Cargo:</label>
                                    <input for="titulo_representante" ${desabilitado} name="cargo" type="text" value="${universidade.tituloRepresentante}"></input>

                            </p>

                            <p><label for="nome_rep">Nome Completo: </label><input ${desabilitado} id="nome_rep" name="nomeRep" type="text" value="${universidade.nomeRepresentante}"></input></p>
                            <p><label for="endereco">Endere�o: </label><input id="endereco" type="text" ${desabilitado} name="endereco" value="${universidade.enderecoRepresentante}" name="endereco"></input></p>
                            <p><label for="bairro">Bairro: </label><input id="bairro" type="text" ${desabilitado} name="bairro" value="${universidade.bairroRepresentante}" name="bairro"></input></p>
                            <p class="inputs_inline">
                                <span class="sem_rotulo">
                                    <label for="cidade">Cidade: </label>
                                    <input id="cidade" type="text" ${desabilitado} name="cidade" value="${universidade.cidadeRepresentante}" name="cidade"></input>
                                    <label class="cep" for="cep">CEP: </label>
                                    <input id="cep" class="mascara-cep cep2" type="text" ${desabilitado} name="cep" value="${universidade.cepRepresentante}" maxlength="10" name="cep"></input>
                                </span>
                            </p>
                            <p><label for="estado">Estado: </label>
                                <select id="estado" name="estado" ${desabilitado}>
                                    <c:forEach var="estado" items="${requestScope.estados}">
                                        <option  value="${estado}" <c:if test="${universidade.estadoRepresentante eq estado}" > selected="selected" </c:if> >${estado}</option>
                                    </c:forEach>
                                </select>
                            </p>

                            <p><label for="nacionalidade">Nacionalidade: </label><input id="nacionalidade" type="text" ${desabilitado} name="nacionalidade" value="${universidade.nacionalidadeRepresentante}"></input></p>
                            <p><label for="estado_civil">Estado Civil: </label>
                                <select id="estado_civil" name="estadoCivil" ${desabilitado}>
                                    <option value="SOLTEIRO" <c:if test="${universidade.estadoCivilRepresentante eq 'SOLTEIRO'}" > selected="selected" </c:if> >solteiro(a)</option>
                                    <option value="CASADO" <c:if test="${universidade.estadoCivilRepresentante eq 'CASADO'}" > selected="selected" </c:if> >casado(a)</option>
                                    <option value="VIUVO" <c:if test="${universidade.estadoCivilRepresentante eq 'VIUVO'}" > selected="selected" </c:if> >vi�vo(a)</option>
                                    <option value="SEPARADO JUDICIALMENTE" <c:if test="${universidade.estadoCivilRepresentante eq 'SEPARADO JUDICIALMENTE'}" > selected="selected" </c:if> >separado(a) judicialmente</option>
                                    <option value="DIVORCIADO" <c:if test="${universidade.estadoCivilRepresentante eq 'DIVORCIADO'}" > selected="selected" </c:if> >divorciado(a)</option>
                                    </select>
                                </p>
                                <p class="inputs_inline">
                                    <span class="sem_rotulo">
                                        <label for="rg">RG: </label><input id="rg" type="text" ${desabilitado} name="rg" value="${universidade.rgRepresentante}" />
                                    <label for="orgao_exp">�rg�o Expedidor:</label><input id="orgao_exp" type="text" ${desabilitado} name="orgaoExp" value="${universidade.orgaoExpRgRepresentante}" />

                                </span>
                            </p>

                            <p><label for="cpf">CPF: </label><input id="cpf" type="text" ${desabilitado} name="cpf" value="${universidade.cpfRepresentante}"></input></p>

                        </fieldset>


                        <p>
                            <input type="submit" ${desabilitado} value="Salvar" class="botoes_form" />

                        </p>

                    </form>
                </div>

                <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
                <%@include file="/WEB-INF/includes/rodape.jsp" %>
                <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
            </div>

        </body>
</html>