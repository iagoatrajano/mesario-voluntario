package tre.pi.sedesc.mesario.modelo;

import componente_acesso.controleusuario.usuario.Lotacao;
import componente_acesso.controleusuario.usuario.Zona;

public class Usuario {

    private componente_acesso.controleusuario.usuario.Usuario usuario;
    private String login;
    private String nome;
    private String sigla;
    private String cdLotacao;
    private String dsLotacao;
    private String validadeSenha = "90";
    private String nivel = "0";
    private String nomeNivel = "Usu�rio Comum";
    private Zona zona; //= new Zona(98, "Teresina", "98");
    private Lotacao lotacao;//  = new Lotacao(0, "SEDESC", "Se��o De Desenvolvimento");

    public componente_acesso.controleusuario.usuario.Usuario getUsuario() {
        lotacao = new Lotacao(Integer.parseInt(cdLotacao), sigla, dsLotacao);
        usuario = new componente_acesso.controleusuario.usuario.Usuario(login, "", validadeSenha, nome, lotacao, zona, nivel, nomeNivel);
        return usuario;
    }

    public void setUsuario(componente_acesso.controleusuario.usuario.Usuario usuario) {
        this.usuario = usuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getCdLotacao() {
        return cdLotacao;
    }

    public void setCdLotacao(String cdLotacao) {
        this.cdLotacao = cdLotacao;
    }

    public String getDsLotacao() {
        return dsLotacao;
    }

    public void setDsLotacao(String dsLotacao) {
        this.dsLotacao = dsLotacao;
    }

    public String getValidadeSenha() {
        return validadeSenha;
    }

    public void setValidadeSenha(String validadeSenha) {
        this.validadeSenha = validadeSenha;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNomeNivel() {
        return nomeNivel;
    }

    public void setNomeNivel(String nomeNivel) {
        this.nomeNivel = nomeNivel;
    }

    public Zona getZona() {
        return zona;
    }

    public void setZona(Zona zona) {
        this.zona = zona;
    }

    public Lotacao getLotacao() {
        return lotacao;
    }

    public void setLotacao(Lotacao lotacao) {
        this.lotacao = lotacao;
    }

}
