package tre.pi.sedesc.mesario.servlet;

import br.jus.trepi.sedesc.common.util.ResourceUtil;
import componente_acesso.controleusuario.usuario.Usuario;
import componente_acesso.controleusuario.usuario.Zona;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.ldap.LdapContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mesario.comunicacao.Fachada;
import tre.pi.sedesc.mesario.autenticacao.AutenticacaoLdap;

/**
 * Servlet implementation class ServletValidarUsuario
 */
public class ServletValidarUsuario extends HttpServlet {

  private static final long serialVersionUID = 1L;

  Fachada fachada = null;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ServletValidarUsuario() {
    super();
    // TODO Auto-generated constructor stub
  }

  public void init() throws ServletException {
    this.fachada = Fachada.getInstance();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   * response)
   */
  protected void doGet(HttpServletRequest request,
          HttpServletResponse response) throws ServletException, IOException {
    // TODO Auto-generated method stub
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   * response)
   */
  protected void doPost(HttpServletRequest request,
          HttpServletResponse response) throws ServletException, IOException {

    HttpSession session = request.getSession();
    Usuario usuario = getUsuarioAutenticadoNoAD(
            request.getParameter("usuario"), request.getParameter("senha"));

    if (usuario != null && usuario.getLogin() != null) {
      session.setAttribute("usuario", usuario);
      //
      List<String> grupoGt = ResourceUtil.getResourceAsStringList("grupo-gt.res", null);
      session.setAttribute("grupoGt", grupoGt);
      //
      response.sendRedirect(request.getContextPath()
              + getInitParameter("primeiraPagina"));
    } else {
      String mensagem = "O login n�o pode ser realizado, usu�rio n�o encontrado. ";

      request.setAttribute("mensagem", mensagem);

      // direciona para raiz do sistema
      getServletConfig()
              .getServletContext()
              .getRequestDispatcher("/componente_acesso/mensagemErro.jsp")
              .forward(request, response);
    }
  }

  private Usuario getUsuarioAutenticadoNoAD(String logon, String password) {

    Usuario usuario = null;
    AutenticacaoLdap autenticador = new AutenticacaoLdap("ldap://pidc01");
    LdapContext ldapContext = autenticador.login(logon, password);

    // Se ldapContext != null, ent�o o usu�rio autenticou com sucesso no AD.
    if (ldapContext != null) {
      try {
        tre.pi.sedesc.mesario.modelo.Usuario usuSedesc = fachada
                .consultarUsuarioLogin(logon);
        if (usuSedesc != null) {
          String numZona = usuSedesc.getSigla().substring(0, 2);
          try {
            Integer numZonaInteger = new Integer(numZona);
            mesario.controle.mesario.Zona zona = fachada
                    .consultarZonaPorNumero(numZonaInteger);
            usuSedesc.setZona(new Zona(Integer.parseInt(zona
                    .getNumZona()), zona.getMunicipio(), zona
                    .getNumZona()));
          } catch (NumberFormatException ex) {
            // ex.printStackTrace();
          } catch (SQLException e) {
            e.printStackTrace();
          }
        }
        usuario = usuSedesc.getUsuario();
      } catch (ClassNotFoundException ex) {
        ex.printStackTrace();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return usuario;
  }
}
