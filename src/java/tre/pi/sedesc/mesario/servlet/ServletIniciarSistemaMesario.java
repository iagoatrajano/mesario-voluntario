package tre.pi.sedesc.mesario.servlet;

import java.io.IOException;

import javax.naming.ldap.LdapContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tre.pi.sedesc.mesario.autenticacao.AutenticacaoLdap;
import componente_acesso.controleusuario.usuario.Usuario;

/**
 * Servlet implementation class ServletIniciarSistema
 */
public class ServletIniciarSistemaMesario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletIniciarSistemaMesario() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession();

		// String usada para armazenar as mensagens de erro
		String mensagem;

		// a session retorna o objeto usuario
		Usuario usuario = (Usuario) session.getAttribute("usuario");

		// o tipo pode ser 0 - usuario tem acesso ao sistema

		// o tipo pode ser 1 - usuario tem acesso ao sistema, mas tem que
		// verificar a validade do acesso( n�o estar lotado
		// alguma zona )

		// o tipo pode ser 2 - EJE

		if (usuario.getNivel().equals("1") && usuario.getZona() == null) {

			mensagem = "O login n�o pode ser realizado, pois o usu�rio n�o est� lotado em uma Zona Eleitoral. ";

			req.setAttribute("mensagem", mensagem);

			// direciona para raiz do sistema
			getServletConfig()
					.getServletContext()
					.getRequestDispatcher("/componente_acesso/mensagemErro.jsp")
					.forward(req, resp);

		} else {
			// acesso validado com sucesso o usuario ser� direcionado para a
			// pagina do menu.
			getServletConfig().getServletContext()
					.getRequestDispatcher("/menu.jsp").forward(req, resp);

		}
	}

}
