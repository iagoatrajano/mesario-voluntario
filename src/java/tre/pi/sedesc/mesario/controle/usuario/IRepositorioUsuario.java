package tre.pi.sedesc.mesario.controle.usuario;

import tre.pi.sedesc.mesario.modelo.Usuario;

public interface IRepositorioUsuario {
	public Usuario consultarLogin(String login) throws ClassNotFoundException ;
}
