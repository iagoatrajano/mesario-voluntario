package tre.pi.sedesc.mesario.controle.usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import mesario.util.Conexao;
import tre.pi.sedesc.mesario.modelo.Usuario;

public class RepositorioUsuarioBDR implements IRepositorioUsuario {

    public Usuario consultarLogin(String login) throws ClassNotFoundException {
        PreparedStatement stmt = null;
        Connection con = null;
        Conexao conexao = new Conexao();
        ResultSet res = null;
        Usuario retorno = null;

        StringBuilder sql = new StringBuilder(
                "SELECT usu.*, CASE WHEN us.nivel IS NULL THEN '2' ELSE us.nivel END AS nivel ")
                .append("from vw_usuario usu  ")
                .append("left join usuario us on us.matricula = usu.matricula ")
                .append("where usu.login = ?");
        try {
            con = (Connection) conexao.abrirConexao();
            stmt = con.prepareStatement(sql.toString());
            stmt.setString(1, login);

            res = stmt.executeQuery();

            if (res.next()) {
                retorno = new Usuario();
                retorno.setCdLotacao(res.getString("CD_LOTACAO"));
                retorno.setDsLotacao(res.getString("DESCRICAO_LOTACAO"));
                retorno.setLogin(res.getString("LOGIN"));
                retorno.setNivel(res.getString("NIVEL"));
                retorno.setNome(res.getString("NOME"));
                retorno.setSigla(res.getString("SIGLA"));
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            conexao.fecharResultSet(res);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }

        return retorno;
    }

}
