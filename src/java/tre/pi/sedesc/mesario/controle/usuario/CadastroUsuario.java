package tre.pi.sedesc.mesario.controle.usuario;

import tre.pi.sedesc.mesario.modelo.Usuario;

public class CadastroUsuario {
	private IRepositorioUsuario repositorioUsuario = new RepositorioUsuarioBDR();
	
	public Usuario consultarUsuarioLogin(String login) throws ClassNotFoundException{
		return repositorioUsuario.consultarLogin(login);
	}
}
