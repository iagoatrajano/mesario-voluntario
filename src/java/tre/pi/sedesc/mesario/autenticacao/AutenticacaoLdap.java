package tre.pi.sedesc.mesario.autenticacao;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class AutenticacaoLdap {

  private String providerUrl;
  private final String domainName = "CN=%s,CN=Users,DC=tre-pi,DC=gov,DC=br";

  public AutenticacaoLdap(String providerUrl) {
    this.providerUrl = providerUrl;
  }
  private Set<String> dnGruposVisitados = new HashSet<String>();

  public LdapContext login(String nomeUsuario, String senha) {
    dnGruposVisitados.clear();

    if (senha == null || senha.trim() == "") {

      return null;
    }

    String dn = String.format(domainName, nomeUsuario);
    Hashtable<String, String> configs = new Hashtable<String, String>();
    configs.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    configs.put(Context.PROVIDER_URL, providerUrl);
    configs.put(Context.SECURITY_AUTHENTICATION, "simple");
    configs.put(Context.SECURITY_PRINCIPAL, dn);
    configs.put(Context.SECURITY_CREDENTIALS, senha);
    try {
      return new InitialLdapContext(configs, null);
    } catch (AuthenticationException error) {
      return null;
    } catch (NamingException error) {
      new FalhaGeralLdapException(error);
    }
    return null;
  }

  private void fecharLdapContext(LdapContext ldapContext)
          throws FalhaGeralLdapException {
    if (ldapContext != null) {
      try {
        ldapContext.close();
      } catch (NamingException error) {
        throw new FalhaGeralLdapException(error);
      }
    }
  }

  public void loginLdap(String nomeUsuario, String senha)
          throws FalhaAutenticacaoException, FalhaGeralLdapException {
    LdapContext ldapContext = null;
    try {
      ldapContext = login(nomeUsuario, senha);
    } finally {
      fecharLdapContext(ldapContext);
    }
  }

}
