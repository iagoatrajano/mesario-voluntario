package tre.pi.sedesc.mesario.autenticacao;

public class FalhaGeralLdapException extends Exception {

    /**
     * Construtor da classe
     * @param causa exce��o a ser disparada
     */
    public FalhaGeralLdapException(Exception causa) {
        super(causa);
    }
}
