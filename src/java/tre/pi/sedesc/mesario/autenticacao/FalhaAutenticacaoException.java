package tre.pi.sedesc.mesario.autenticacao;

public class FalhaAutenticacaoException extends Exception {

   /**
    * propriedade mensagem
    */
   private String mensagem;

   /**
    * Construtor da classe
    * @param mensagem mensagem a ser mostrada na exce��o
    */
   public FalhaAutenticacaoException(String mensagem) {
       super(mensagem);
       this.setMensagem(mensagem);
   }

   /**
    * Construtor da classe
    * @param e exce��o a ser disparada
    */
   public FalhaAutenticacaoException(Exception e) {
       super(e);
   }

   /**
    * M�todo getter do atributo mensagem
    * @return string
    */
   public String getMensagem() {
       return mensagem;
   }

   /**
    * m�todo setter do atributo mensagem
    * @param mensagem mensagem a ser setada
    */
   public void setMensagem(String mensagem) {
       this.mensagem = mensagem;
   }
}
