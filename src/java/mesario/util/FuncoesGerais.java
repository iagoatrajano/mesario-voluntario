/*
 * Created on 10/05/2006
 *
 */
package mesario.util;

import java.util.*;
import java.math.BigDecimal;

import com.sun.org.apache.xalan.internal.xsltc.compiler.Parser;

public class FuncoesGerais {
	
	public static String formatarMes(int mes) {
			int indice = mes - 1;
			String[] meses = {"Jan", "Feb", "Mar", "Apr", "May",
							  "Jun", "Jul", "Aug", "Sep", "Oct",
							  "Nov", "Dec"};
			return meses[indice];
	  }
	
    public static String formarData(String data) {
	   String dia = data.substring(0,2);
	   String mes = data.substring(2,4);
	   String ano = data.substring(6);
	   String novaData = dia + "-" + mes + "-" + ano;
	   
	   return novaData;
	}
    
    
    public static String formatarTelefoneMascara(String telefone){	    	
        
		String telPar1 = telefone.substring(5, 9);					
		String telPar2 = telefone.substring(10,telefone.length());					
		telefone = telPar1 + telPar2;		
		
		return telefone;
    	
    }
    
    public static Integer dddTelefoneMascara(String telefone){    	
    	
    	String dddTel = telefone.substring(1,3);
    	
    	return Integer.parseInt(dddTel);
    	
    }
    
    public static Integer cepFormatarMascara(String cep){
    	
    	String cepPart1 = cep.substring(0,5);
    	
    	
    	String cepPart2 = cep.substring(6,cep.length());   
    	
    	
    	cep = cepPart1 + cepPart2;    	
    	
    	return Integer.parseInt(cep);
    	
    }
    

	public static boolean compararDataComDataAtual(String data, String dataAtual) {

		       boolean erro = false;
		       
		       int dia = Integer.parseInt(data.substring(0,2));
		       int mes = Integer.parseInt(data.substring(3,5));
		       int ano = Integer.parseInt(data.substring(6));

        	   int anoAtual = Integer.parseInt(dataAtual.substring(0,4));   
		       int mesAtual = Integer.parseInt(dataAtual.substring(5,7));    
		       int diaAtual = Integer.parseInt(dataAtual.substring(8,10));   

			   if(ano < anoAtual){
				erro = true;  //A data informada j� foi atingida (data menor que a data atual)
			   }
			   else if(ano == anoAtual){
				 if(mes < mesAtual){
				  erro = true;  //A data informada j� foi atingida (data menor que a data atual)
				 }
				 else if (mes == mesAtual){
					if(dia < diaAtual){
					 erro = true;  //A data informada j� foi atingida (data menor que a data atual)
					}
				 }	 
			   }

		  return erro;

	  }


/*    
   // Esta fun��o foi substitu�da por fachada.compararDataAtualComLimitesPrazoCadastramento(Bienio bienio).
   // Motivo: comparava a data atual da m�quina do usu�rio e n�o do servidor

   public static String compararDataAtualComLimitesPrazo(String dataPrazoInicial, String dataPrazoFinal) {
	  	
		   Date dtAtual = new Date();
		   boolean erro = false;
		   
		   Calendar cal = Calendar.getInstance();   
		   cal.setTime(new Date());   
               
		   int anoAtual = cal.get(Calendar.YEAR);   
		   int mesAtual = cal.get(Calendar.MONTH)+1;   
		   int diaAtual = cal.get(Calendar.DAY_OF_MONTH);
             
	       int diaInicial = Integer.parseInt(dataPrazoInicial.substring(0,2));
	       int mesInicial = Integer.parseInt(dataPrazoInicial.substring(3,5));
	       int anoInicial = Integer.parseInt(dataPrazoInicial.substring(6));

	       int diaFinal = Integer.parseInt(dataPrazoFinal.substring(0,2));
	       int mesFinal = Integer.parseInt(dataPrazoFinal.substring(3,5));
	       int anoFinal = Integer.parseInt(dataPrazoFinal.substring(6));    

           if(anoInicial > anoAtual){
           	erro = true;
           }
           else if(anoInicial == anoAtual){
           	 if(mesInicial > mesAtual){
           	  erro = true;
           	 }
           	 else if (mesInicial == mesAtual){
           	 	if(diaInicial > diaAtual){
           	 	 erro = true;
           	 	}
           	 }	 
           }
            

            
	       if(anoAtual > anoFinal){
            erro = true;
           }
           else if(anoAtual == anoFinal){
	         if(mesAtual > mesFinal){
              erro = true;
	         }
	         else if (mesAtual == mesFinal){
		       if(diaAtual > diaFinal){
		        erro = true;
		       } 
	         }
           }
           
	  if (erro) {

	  //A data atual n�o est� dentro do limite do prazo de cadastramento de a��es

	   return "sim";

	  }

	  return "n�o";

  }
*/


  public static String converteBigDecimalEmString(BigDecimal valor) {
  
   String parteInteira = "";
   String parteDecimal = "";
   String parteMilesimo = "";
   String valorFinal = "";
   char sinal = ' ';
   int j = 0;
   int x = 0;
   int indFinalParteInteira = 0;
   valorFinal = String.valueOf(valor);

   if (valorFinal.charAt(0)=='-'){
		 sinal = '-';
		 valorFinal = valorFinal.replace('-', ' ');
		 valorFinal = valorFinal.trim();
   }
   
   StringTokenizer st = new StringTokenizer(valorFinal,".");
   if(st.countTokens()>1){
	 parteInteira = st.nextToken();
	 parteDecimal = st.nextToken();
	 if(parteDecimal.length()==1){
	   parteDecimal = parteDecimal + "0";
	 }
	 indFinalParteInteira = parteInteira.length()-1;
	 for(j = indFinalParteInteira; j >= 0; j--){
	   x = x+1;
	   parteMilesimo = parteInteira.charAt(j) + parteMilesimo;
	   if((x==3)&&(j!=0)){
		 parteMilesimo = "."+parteMilesimo;
		 x = 0;
	   } // fim do if
	 }
	 
	 valorFinal = parteMilesimo + "," + parteDecimal;
	 }else{
	 if(valorFinal=="null"){
	    valorFinal = "";
	 }else{
		indFinalParteInteira = valorFinal.length()-1;
		for(j = indFinalParteInteira; j >= 0; j--){
		   x = x+1;
		   parteMilesimo = valorFinal.charAt(j) + parteMilesimo;
		   if((x==3)&&(j!=0)){
			 parteMilesimo = "."+parteMilesimo;
			 x = 0;
		   }
		}
		valorFinal = parteMilesimo + ",00";
	 }
   }
   valorFinal = sinal + valorFinal;
   valorFinal = valorFinal.trim();
   return valorFinal;
  }

  public static BigDecimal converterStringFormatoMoedaEmBigDecimal(String valor) {
	//retirando os pontos e substituindo a virgula por um ponto decimal, no Valor Previsto da A��o
	
	int i = 0;
	String parteMilesimo = "";
	String valorTemp = "";
	BigDecimal val = null;
	
		//retirando os pontos
		StringTokenizer st = new StringTokenizer(valor,".");
				
		if(st.countTokens()>1){
		  for (i=0; i <= (st.countTokens()+1); i++){
			parteMilesimo = st.nextToken();
			valorTemp = valorTemp + parteMilesimo;
		  }
		}else{
		  valorTemp = valor;
		}	
		//substituindo a v�rgula por ponto decimal
		valorTemp = valorTemp.replace(',', '.');

		val =  new BigDecimal(valorTemp);
        
        return val;
  }
}	
//	Testando Fun�oes gerais
//	public static void main(String[] argv){
//		String data  = FuncoesGerais.formarData("01112003");
		
//		System.out.println(data);
//	}




