/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mesario.util;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author bc548
 */
public class BaseServlet extends HttpServlet {

    protected final void forward(HttpServletRequest req, HttpServletResponse res, String path) throws ServletException, IOException {
        if (req == null) {
            throw new NullPointerException("A request must be specified.");
        }
        if (res == null) {
            throw new NullPointerException("A response must be specified.");
        }
        if (path == null) {
            throw new NullPointerException("An path must be specified.");
        }
        //
        this.getServletConfig().getServletContext().getRequestDispatcher(path).forward(req, res);
    }

}
