package mesario.util;

import java.util.ArrayList;

public class Util {
	
	public ArrayList <String> listarEstados(){
		ArrayList <String> lista = new ArrayList<String>();
		lista.add("AC");
		lista.add("AL");
		lista.add("AM");
		lista.add("AP");
		lista.add("BA");
		lista.add("CE");
		lista.add("DF");
		lista.add("ES");
		lista.add("GO");
		lista.add("MA");
		lista.add("MG");
		lista.add("MS");
		lista.add("MT");
		lista.add("PA");
		lista.add("PB");
		lista.add("PE");
		lista.add("PI");
		lista.add("PR");
		lista.add("RJ");
		lista.add("RN");
		lista.add("RO");
		lista.add("RR");
		lista.add("RS");
		lista.add("SC");
		lista.add("SE");
		lista.add("SP");
		lista.add("TO");
		return lista;
	}
	

	
public String editaNome( String nome){
		
		String novoNome = new String();
		//Separa o nome completo por espa�os
		String [] nomeQuebrado = nome.split(" ");
		
		for (int i = 0; i < nomeQuebrado.length; i++) {
			
			//Se n�o for a primeira palavra do nome e for um desses casos, fica tudo min�sculo
			
			if( i > 0 && (					
					nomeQuebrado[i].toString().toLowerCase().equals("da") ||
					nomeQuebrado[i].toString().toLowerCase().equals("das") ||
					nomeQuebrado[i].toString().toLowerCase().equals("de") ||
					nomeQuebrado[i].toString().toLowerCase().equals("do") ||
					nomeQuebrado[i].toString().toLowerCase().equals("dos") ||
					nomeQuebrado[i].toString().toLowerCase().equals("para") ||
					nomeQuebrado[i].toString().toLowerCase().equals("a") ||
					nomeQuebrado[i].toString().toLowerCase().equals("e") ||					
					nomeQuebrado[i].toString().toLowerCase().equals("o") ||
					nomeQuebrado[i].toString().toLowerCase().equals("um") ||
					nomeQuebrado[i].toString().toLowerCase().equals("�") ||
					nomeQuebrado[i].toString().toLowerCase().equals("o")) ) {				
				novoNome +=nomeQuebrado[i].toString().toLowerCase();			
			}
			else if (nomeQuebrado[i] != null && !nomeQuebrado[i].equals("")){
				//Primeira mai�scula

				novoNome += Character.toString(nomeQuebrado[i].charAt(0)).toUpperCase();
				for (int j = 1; j < nomeQuebrado[i].length(); j++) {
					novoNome += Character.toString(nomeQuebrado[i].charAt(j)).toLowerCase();
				}				
			}			
			//novoNome += " ";
		}
	
		return novoNome;
	}
	
}
