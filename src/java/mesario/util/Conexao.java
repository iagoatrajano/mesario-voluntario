/*
 * Created on 10/05/2006
 *
 */
package mesario.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class Conexao {

    private static final String JDBC_RESOURCE = "jdbc/mesario_voluntario_desenvolvimento";

    Constantes constantes = null;

    /**
     * Construtor para carregar driver jdbc.
     */
    public Conexao() throws ClassNotFoundException {
        constantes = new Constantes();
        Class.forName(constantes.get("driver"));
        //Class.forName(driver);
    }

    /**
     * Abre uma conexao com o BD.
     */
    public Connection abrirConexao() throws SQLException {
        Connection conn = this.getConnectionFromDataSource();
        return conn;
    }

    private static synchronized Connection getConnectionFromDataSource() throws SQLException {
        Connection conn = null;
        try {
            Context initCtx = new InitialContext();
            DataSource ds = (DataSource) initCtx.lookup(Conexao.JDBC_RESOURCE);
            conn = ds.getConnection();
            if (!conn.getAutoCommit()) {
                conn.setAutoCommit(true);
            }
        } catch (NamingException ex) {
            ex.printStackTrace();
        }
        return conn;
    }

    /**
     * Fecha um Statement.
     */
    public void fecharStatement(Statement stmt) {
        try {

            //s� fecha o statement se ele for diferente de null
            if (stmt != null) {
                stmt.close();
            }

        } catch (SQLException e) {
            //como o erro � um caso raro, apenas mosta a exce��o no console
            //outra op��o � gerar um log do sistema
            e.printStackTrace();
        }
    }

    /**
     * Fecha uma conexao com o banco.
     */
    public void fecharConexao(Connection conexao) {
        try {

            //s� fecha a conexao se ela for diferente de null
            if (conexao != null) {
                conexao.close();
            }

        } catch (SQLException e) {
            //como o erro � um caso raro, apenas mosta a exce��o no console
            //outra op��o � gerar um log do sistema
            e.printStackTrace();
        }
    }

    /**
     * Fecha uma conexao com o banco.
     */
    public void fecharResultSet(ResultSet rs) {
        try {

            //s� fecha a conexao se ela for diferente de null
            if (rs != null) {
                rs.close();
            }

        } catch (SQLException e) {
            //como o erro � um caso raro, apenas mosta a exce��o no console
            //outra op��o � gerar um log do sistema
            e.printStackTrace();
        }
    }

}
