package mesario.controle.mesario;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.excecoes.RepositorioException;

public class CadastroEleitor {

  private IRepositorioEleitor repositorioEleitor = new RepositorioEleitorBDR();

  public Eleitor consultarEleitor(String titulo) throws RepositorioException,
          SQLException, ClassNotFoundException, ParseException {
    return this.repositorioEleitor.consultarEleitor(titulo);
  }

  public List consultarOcupacao() throws RepositorioException, SQLException,
          ClassNotFoundException {
    return this.repositorioEleitor.consultarOcupacao();
  }

  public String consultarOcupacao(int numero) throws RepositorioException, SQLException,
          ClassNotFoundException {
    return this.repositorioEleitor.consultarOcupacao(numero);
  }

  public int cadastraEleitor(Eleitor eleitor) throws RepositorioException,
          SQLException, ClassNotFoundException {
    return this.repositorioEleitor.cadastraEleitor(eleitor);
  }

  public boolean consultarEleitorCadastrado(String titulo, String numZona) throws SQLException, ClassNotFoundException {
    return this.repositorioEleitor.consultarEleitorCadastrado(titulo, numZona);
  }

  public List consultarDatasInscricao(int zona) throws SQLException, ClassNotFoundException {
    return this.repositorioEleitor.consultarDatasInscricao(zona);
  }

  public List consultarEleitorAno(String ano, int zona, String tipoOrdenacao) throws SQLException, ClassNotFoundException, ParseException {
    return this.repositorioEleitor.consultarEleitorAno(ano, zona, tipoOrdenacao);
  }

  public ArrayList<UniversidadeConveniada> consultarEstatisticasPorEntidade() throws ClassNotFoundException {
    return this.repositorioEleitor.consultarEstatisticasPorEntidade();
  }

  public LinkedHashMap<String, Integer> consultarEstatisticasPorAno() throws ClassNotFoundException {
    return this.repositorioEleitor.consultarEstatisticasPorAno();
  }

  //public List consultarEstatisticasPorPeriodo(java.util.Date dataInicialPeriodo, java.util.Date dataFinalPeriodo) throws ClassNotFoundException{
  public int consultarEstatisticasPorPeriodo(String dataInicialPeriodo, String dataFinalPeriodo) throws ClassNotFoundException {
    return this.repositorioEleitor.consultarEstatisticasPorPeriodo(dataInicialPeriodo, dataFinalPeriodo);
  }

  public ArrayList<Zona> consultarZonasEleitorais() throws
          SQLException, ClassNotFoundException {
    return this.repositorioEleitor.consultarZonasEleitorais();
  }

  public Zona consultarZona(Integer numZona) throws
          SQLException, ClassNotFoundException {
    return this.repositorioEleitor.consultarZona(numZona);
  }
}
