package mesario.controle.mesario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.excecoes.RepositorioException;
import mesario.util.Conexao;

public class RepositorioEleitorBDR implements IRepositorioEleitor {

    public Eleitor consultarEleitor(String titulo) throws RepositorioException,
            SQLException, ClassNotFoundException, ParseException {
        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;
        Eleitor eleitor = null;

        Conexao conexao = new Conexao();

        sql = "SELECT NUM_TITULO, NOM_ELEITOR, DAT_NASC, NOM_MAE, NUM_ZONA, "
                + "  NUM_SECAO, NUM_MUNIC_ZONA, MUNICIPIO_ZONA, NUM_LOCAL_VOTACAO, "
                + "  LOCAL_VOTACAO " + "FROM vw_eleitor "
                + "WHERE num_titulo = '" + titulo + "'";

        try {

            con = (Connection) conexao.abrirConexao();

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                eleitor = new Eleitor();
                eleitor.setTitulo(rs.getString("NUM_TITULO"));
                eleitor.setNome(rs.getString("NOM_ELEITOR"));
                String dataNasc = rs.getString("DAT_NASC");
                dataNasc = dataNasc.substring(6, 8) + "/"
                        + dataNasc.substring(4, 6) + "/"
                        + dataNasc.substring(0, 4);
                eleitor.setDataNascimento((Date) new SimpleDateFormat(
                        "dd/MM/yyyy").parse((dataNasc)));
                eleitor.setNomeMae(rs.getString("NOM_MAE"));
                String numZona = rs.getString("NUM_ZONA");
                if (numZona.length() == 1) {
                    numZona = "00" + numZona;
                } else if (numZona.length() == 2) {
                    numZona = "0" + numZona;
                }
                eleitor.setNumZona(numZona);
                eleitor.setSecaoEleitoral(rs.getString("NUM_SECAO"));
                eleitor.setNumMunicZona(rs.getString("NUM_MUNIC_ZONA"));
                eleitor.setMunicipioZona(rs.getString("MUNICIPIO_ZONA"));
                eleitor.setNumLocalVotacao(rs.getString("NUM_LOCAL_VOTACAO"));
                eleitor.setLocalVotacao(rs.getString("LOCAL_VOTACAO"));
            }

        } catch (SQLException e) {
            throw e;
        } finally {
            conexao.fecharResultSet(rs);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }

        return eleitor;
    }

    public List consultarOcupacao() throws RepositorioException, SQLException,
            ClassNotFoundException {

        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;
        List<Vector<String>> listaOcupacao = null;
        Vector<String> ocupacao = null;

        Conexao conexao = new Conexao();

        sql = "SELECT num_ocupacao, des_ocupacao " + "FROM vw_ocupacao "
                + "ORDER BY des_ocupacao";

        try {

            con = (Connection) conexao.abrirConexao();

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            listaOcupacao = new ArrayList<Vector<String>>();
            while (rs.next()) {
                ocupacao = new Vector<String>();

                ocupacao.add(rs.getString("num_ocupacao"));
                ocupacao.add(rs.getString("des_ocupacao"));

                listaOcupacao.add(ocupacao);
            }

        } catch (SQLException e) {
            throw e;
        } finally {
            conexao.fecharResultSet(rs);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }

        return listaOcupacao;
    }

    public String consultarOcupacao(int numero) throws RepositorioException,
            SQLException, ClassNotFoundException {

        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;
        String ocupacao = null;

        Conexao conexao = new Conexao();

        sql = "SELECT des_ocupacao " + "FROM vw_ocupacao "
                + "WHERE num_ocupacao = " + numero + "ORDER BY des_ocupacao";

        try {

            con = (Connection) conexao.abrirConexao();

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                ocupacao = rs.getString("des_ocupacao");
            }

        } catch (SQLException e) {
            throw e;
        } finally {
            conexao.fecharResultSet(rs);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }

        return ocupacao;
    }

    public int cadastraEleitor(Eleitor eleitor) throws RepositorioException,
            SQLException, ClassNotFoundException {

        PreparedStatement stmt = null;
        String sql = "";
        String sqlInsert = "";
        String sqlValues = "";
        String sqlDataHoraAtual = "(SELECT SYSDATE FROM DUAL)";
        ResultSet rs = null;
        Connection con = null;

        Conexao conexao = new Conexao();
        try {

            con = (Connection) conexao.abrirConexao();

            if (!consultarEleitorCadastrado(eleitor.getTitulo().trim(), eleitor.getNumZona())) {

                try {
                    sqlInsert = "INSERT INTO ADMMESARIO.Eleitor ("
                            + "  TITULO, "
                            + "  NOME, "
                            + "  DATA_NASC, "
                            + "  NOM_MAE, "
                            + "  ZONA_ELEITORAL, "
                            + "  NUM_SECAO, "
                            + "  NUM_MUNIC_ZONA, "
                            + "  MUNICIPIO_ZONA, "
                            + "  NUM_LOCAL_VOTACAO, "
                            + "  LOCAL_VOTACAO, "
                            + "  DATA_CADASTRO, "
                            + "  ENDERECO, "
                            + "  BAIRRO, "
                            + "  CIDADE, "
                            + "  TELEFONE, "
                            + "  FONE_OPCIONAL, "
                            + "  SERVIDOR_PUBLICO, "
                            + "  FOI_MESARIO, "
                            + "  GRAU_INSTRUCAO,"
                            + "  COD_OBJETO_UNIV_CONVENIADA , "
                            + "  UNIVERSITARIO ";

                    sqlValues = "VALUES(?, ?, ?, ?, ?, "
                            + "       ?, ?, ?, ?, ?, "
                            + "       " + sqlDataHoraAtual + ", ?, ?, ?, ?,"
                            + "       ?, ?, ?, ?, "
                            + "       ?, ?";

                    // Para inclus�o desse campos abaixo no sql, n�o se utilizou as interroga��es
                    //  porque o if empregados abaixo iria se repitir duas vezes, uma para coloca��o do
                    //  sqlInsert e sqlValues com interroga��o e outro abaixo para implementa��o do
                    //  stmt.set..., desse modo � melhor colocar como esta sendo empregado abaixo.
                    if (eleitor.getOcupacao() != null) {
                        sqlInsert += ", NUM_OCUPACAO, OCUPACAO";
                        sqlValues += ", " + eleitor.getNumOcupacao() + ", '" + eleitor.getOcupacao() + "'";
                    }
                    if (eleitor.getCep() != null) {
                        sqlInsert += ", CEP";
                        sqlValues += ", " + eleitor.getCep();
                    }
                    /*if(eleitor.getDddFone()!=null){
				sqlInsert += ", DDD_FONE";
				sqlValues += ", "+ eleitor.getDddFone();
			}
			if(eleitor.getDddFoneOpcional() !=null){
				sqlInsert += ", DDD_FONE_OPCIONAL";
				sqlValues += ", "+ eleitor.getDddFoneOpcional();
			}*/
                    if (eleitor.getEmail() != null) {
                        sqlInsert += ", EMAIL";
                        sqlValues += ", " + "'" + eleitor.getEmail() + "'";
                    }

                    sqlInsert += ") ";
                    sqlValues += ") ";

                    sql = sqlInsert + sqlValues;
                    System.out.println("SQL: " + sql);

                    stmt = con.prepareStatement(sql);

                    stmt.setString(1, eleitor.getTitulo().trim());
                    stmt.setString(2, eleitor.getNome().trim());
                    stmt.setString(3, (new SimpleDateFormat("dd/MM/yyyy").format(eleitor.getDataNascimento())));
                    stmt.setString(4, eleitor.getNomeMae().trim());
                    stmt.setString(5, eleitor.getNumZona().trim());
                    stmt.setString(6, eleitor.getSecaoEleitoral().trim());
                    stmt.setString(7, eleitor.getNumMunicZona().trim());
                    stmt.setString(8, eleitor.getMunicipioZona().trim());
                    stmt.setString(9, eleitor.getNumLocalVotacao().trim());
                    stmt.setString(10, eleitor.getLocalVotacao().trim());

                    // a data_cadastro � passado no pr�prio sql
                    stmt.setString(11, eleitor.getEndereco().trim());
                    stmt.setString(12, eleitor.getBairro().trim());
                    stmt.setString(13, eleitor.getCidade().trim());
                    stmt.setString(14, eleitor.getTelefone().trim());

                    stmt.setString(15, eleitor.getFoneOpcional().trim());
                    stmt.setInt(16, eleitor.getServidorPublico());
                    stmt.setInt(17, eleitor.getFoiMesario());

                    stmt.setInt(18, eleitor.getGrauInstrucao());
                    stmt.setString(19, eleitor.getCodObjetoUniversidadeConveniada());
                    stmt.setString(20, eleitor.getUniversitario());

                    stmt.execute();
                    stmt.clearParameters();

                } catch (SQLException e) {
                    throw e;
                } finally {
                    conexao.fecharStatement(stmt);
                }

            }

            try {
                // Recebe a data do cadastro
                sql = " SELECT MAX(TO_CHAR(DATA_CADASTRO, 'DD/MM/YYYY  HH24:MI:SS')) as DATA_CADASTRO "
                        + " FROM Eleitor WHERE TITULO = ? ORDER BY DATA_CADASTRO DESC ";

                stmt = con.prepareStatement(sql);

                stmt.setString(1, eleitor.getTitulo());

                rs = stmt.executeQuery();

                while (rs.next()) {
                    eleitor.setDataCadastro(rs.getString("DATA_CADASTRO"));
                }
            } catch (SQLException e) {
                throw e;
            } finally {
                conexao.fecharResultSet(rs);
                conexao.fecharStatement(stmt);
            }

            try {
                //Retorna com a idade da pessoa
                sql = " SELECT MAX(TRUNC((to_number(to_char(DATA_CADASTRO,'YYYYMMDD')) - "
                        + " to_number(to_char(DATA_NASC,'YYYYMMDD')) ) / 10000)) as IDADE "
                        + " FROM ELEITOR "
                        + " WHERE TITULO = ? "
                        + " ORDER BY cod_objeto";

                stmt = con.prepareStatement(sql);
                stmt.setString(1, eleitor.getTitulo());
                rs = stmt.executeQuery();

                int idade = 0;
                while (rs.next()) {
                    System.out.println("IDADE " + idade);
                    idade = Integer.parseInt(rs.getString("IDADE"));
                }

                return idade;
            } catch (SQLException e) {
                throw e;
            } finally {
                conexao.fecharResultSet(rs);
                conexao.fecharStatement(stmt);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            conexao.fecharConexao(con);
        }

    }

    public boolean consultarEleitorCadastrado(String titulo, String numZona) throws SQLException, ClassNotFoundException {
        boolean retorno = false;

        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;
        int total = 0;

        Conexao conexao = new Conexao();

        sql = "SELECT count(*) as TOTAL "
                + "FROM ELEITOR WHERE TITULO = ? "
                + " AND zona_eleitoral = ? "
                + " AND TO_CHAR(DATA_CADASTRO, 'YYYY') = (SELECT MAX(TO_CHAR(DATA_CADASTRO, 'YYYY')) as ANO_CADASTRO "
                + "FROM Eleitor WHERE TITULO = ? "
                + " AND TO_CHAR(DATA_CADASTRO, 'YYYY') = (SELECT TO_CHAR(SYSDATE, 'YYYY') as ANO FROM DUAL))";

        try {

            con = (Connection) conexao.abrirConexao();

            stmt = con.prepareStatement(sql);
            stmt.setString(1, titulo);
            stmt.setString(2, numZona);
            stmt.setString(3, titulo);
            rs = stmt.executeQuery();

            while (rs.next()) {
                total = rs.getInt("TOTAL");
            }

            if (total > 0) {
                retorno = true;
            }

        } catch (SQLException e) {
            throw e;
        } finally {
            conexao.fecharResultSet(rs);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }

        return retorno;
    }

    public List consultarDatasInscricao(int zona) throws SQLException, ClassNotFoundException {

        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;
        List datas = null;

        Conexao conexao = new Conexao();

        sql = "SELECT distinct data_cadastro "
                + "FROM ELEITOR  "
                + "where zona_eleitoral = " + zona + " "
                + "order by data_cadastro ";

        try {

            con = (Connection) conexao.abrirConexao();

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            datas = new Vector<String>();
            while (rs.next()) {

                datas.add(rs.getString("data_cadastro"));
            }

        } catch (SQLException e) {
            throw e;
        } finally {
            conexao.fecharResultSet(rs);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }

        return datas;

    }

    public List consultarEleitorAno(String ano, int zona, String tipoOrdenacao) throws SQLException, ClassNotFoundException, ParseException {
        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;
        Eleitor eleitor = null;
        List listaEleitor = null;

        Conexao conexao = new Conexao();

        sql = " select distinct titulo, nome, zona_eleitoral, data_nasc, zona_eleitoral, "
                + " cod_objeto, local_votacao, num_local_votacao, endereco, "
                + " bairro, cep, cidade, telefone, ddd_fone, num_munic_zona,to_char(data_cadastro,'dd/mm/yyyy') as data, data_cadastro, "
                + " ddd_fone_opcional, fone_opcional, email, ocupacao, num_secao, universitario, cod_objeto_univ_conveniada "
                + " FROM eleitor "
                + " where to_char(data_cadastro,'yyyy') = " + ano + " and zona_eleitoral = " + zona;
        if (tipoOrdenacao.equals("2")) {
            sql += " order by data_cadastro";
        } else if (tipoOrdenacao.equals("3")) {
            sql += " order by num_local_votacao";
        } else if (tipoOrdenacao.equals("4")) {
            sql += "order by nome";
        } else {
            sql += "order by num_local_votacao ,data_cadastro ";
        }

        try {

            con = (Connection) conexao.abrirConexao();

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            listaEleitor = new Vector<Eleitor>();

            while (rs.next()) {
                eleitor = new Eleitor();
                eleitor.setTitulo(rs.getString("titulo"));
                eleitor.setNome(rs.getString("nome"));
                String dataNasc = (String) rs.getString("data_nasc");
                dataNasc = dataNasc.substring(8, 10) + "/" + dataNasc.substring(5, 7) + "/" + dataNasc.substring(0, 4);
                eleitor.setDataNascimento((Date) new SimpleDateFormat("dd/MM/yyyy").parse((dataNasc)));
                String numZona = rs.getString("zona_eleitoral");
                if (numZona.length() == 1) {
                    numZona = "00" + numZona;
                } else if (numZona.length() == 2) {
                    numZona = "0" + numZona;
                }
                eleitor.setNumZona(numZona);
                eleitor.setSecaoEleitoral(rs.getString("num_secao"));
                eleitor.setNumMunicZona(rs.getString("num_munic_zona"));
                eleitor.setNumLocalVotacao(rs.getString("num_local_votacao"));
                eleitor.setLocalVotacao(rs.getString("local_votacao"));
                eleitor.setEndereco(rs.getString("endereco"));
                eleitor.setBairro(rs.getString("bairro"));
                eleitor.setCidade(rs.getString("cidade"));
                eleitor.setCep(rs.getInt("cep"));
                eleitor.setFoneOpcional(rs.getString("fone_opcional"));
                eleitor.setDddFoneOpcional(rs.getInt("ddd_fone_opcional"));
                eleitor.setTelefone(rs.getString("telefone"));
                eleitor.setDddFone(rs.getInt("ddd_fone"));
                eleitor.setEmail(rs.getString("email"));
                eleitor.setOcupacao(rs.getString("ocupacao"));
                eleitor.setUniversitario(rs.getString("universitario"));
                eleitor.setCodObjetoUniversidadeConveniada(rs.getString("cod_objeto_univ_conveniada"));
                eleitor.setDataCadastro(rs.getString("data"));

                listaEleitor.add(eleitor);
            }

        } catch (SQLException e) {
            throw e;
        } finally {
            conexao.fecharResultSet(rs);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }

        return listaEleitor;

    }

    public ArrayList<UniversidadeConveniada> consultarEstatisticasPorEntidade() throws ClassNotFoundException {
        ArrayList<UniversidadeConveniada> lista = new ArrayList<UniversidadeConveniada>();
        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;
        Conexao conexao = new Conexao();
        UniversidadeConveniada universidadeConveniada = null;

        try {

            con = (Connection) conexao.abrirConexao();

            try {
                sql = " select uc.cod_objeto, sigla, NOME_FANTASIA, uc.DATA_FINAL_CONVENIO, "
                        + " uc.CONVENIO_VALIDO as valido, count(*) as total "
                        + " FROM universidade_conveniada uc join eleitor e on e.cod_objeto_univ_conveniada = uc.cod_objeto "
                        + " group by uc.cod_objeto, sigla,  nome_fantasia, data_final_convenio, convenio_valido "
                        + " union "
                        + " select cod_objeto, sigla, nome_fantasia, uc.DATA_FINAL_CONVENIO, uc.CONVENIO_VALIDO as valido, "
                        + " 0 as total "
                        + " FROM universidade_conveniada uc "
                        + " where uc.cod_objeto not in (select distinct e.cod_objeto_univ_conveniada from eleitor e join universidade_conveniada uc on e.cod_objeto_univ_conveniada = uc.cod_objeto) "
                        + " order by sigla, total desc, valido desc ";

                stmt = con.prepareStatement(sql);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    universidadeConveniada = new UniversidadeConveniada();
                    universidadeConveniada.setNomeFantasia(rs.getString("NOME_FANTASIA"));

                    if ((rs.getString("valido").equals("1")) && (rs.getDate("DATA_FINAL_CONVENIO").after(new Date()))) {
                        universidadeConveniada.setConvenioValido("V�lido");
                    } else if ((rs.getString("valido").equals("0")) && (rs.getDate("DATA_FINAL_CONVENIO").after(new Date()))) {
                        universidadeConveniada.setConvenioValido("Inv�lido");
                    } else {
                        universidadeConveniada.setConvenioValido("Encerrado");
                    }

                    universidadeConveniada.setQuantidadeEleitores(rs.getInt("total"));
                    universidadeConveniada.setSigla(rs.getString("sigla"));
                    lista.add(universidadeConveniada);
                }
            } catch (SQLException sqle) {
                throw sqle;
            } finally {
                conexao.fecharResultSet(rs);
                conexao.fecharStatement(stmt);
            }

            try {
                sql = "select null as cod_objeto, null as sigla, 'OUTRAS' as NOME_FANTASIA, ' ' as valido, count(*) as total "
                        + "from eleitor "
                        + "where "
                        + "cod_objeto_univ_conveniada is null "
                        + "and "
                        + "universitario = 1 ";
                stmt = con.prepareStatement(sql);
                rs = stmt.executeQuery();
                if (rs.next()) {
                    universidadeConveniada = new UniversidadeConveniada();
                    universidadeConveniada.setNomeFantasia(rs.getString("NOME_FANTASIA"));
                    universidadeConveniada.setConvenioValido(rs.getString("valido"));
                    universidadeConveniada.setQuantidadeEleitores(rs.getInt("total"));
                    universidadeConveniada.setSigla(rs.getString("sigla"));
                    lista.add(universidadeConveniada);
                }
            } catch (SQLException sqle) {
                throw sqle;
            } finally {
                conexao.fecharResultSet(rs);
                conexao.fecharStatement(stmt);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conexao.fecharConexao(con);
        }
        return lista;

    }

    public LinkedHashMap<String, Integer> consultarEstatisticasPorAno() throws ClassNotFoundException {
        LinkedHashMap<String, Integer> lista = new LinkedHashMap<String, Integer>();
        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;
        Conexao conexao = new Conexao();

        try {

            con = (Connection) conexao.abrirConexao();
            sql = "select to_char(data_cadastro, 'YYYY') as ANO, count(*) as TOTAL "
                    + "FROM eleitor "
                    + "group by to_char(data_cadastro, 'YYYY') "
                    + "order by to_char(data_cadastro, 'YYYY') desc ";

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                lista.put(rs.getString("ANO"), rs.getInt("TOTAL"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conexao.fecharResultSet(rs);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }

        return lista;

    }

    //public List consultarEstatisticasPorPeriodo(Date dataInicialPeriodo, Date dataFinalPeriodo)
    public int consultarEstatisticasPorPeriodo(String dataInicialPeriodo, String dataFinalPeriodo)
            throws ClassNotFoundException {

        int quantidade = 0;
        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;

        Conexao conexao = new Conexao();

        sql = "SELECT data_cadastro "
                + "FROM eleitor "
                + "WHERE trunc(data_cadastro) between to_date(?, 'dd/MM/yyyy') and  to_date(?, 'dd/MM/yyyy') ";

        try {

            con = (Connection) conexao.abrirConexao();

            stmt = con.prepareStatement(sql);
            stmt.setString(1, dataInicialPeriodo);
            stmt.setString(2, dataFinalPeriodo);
            rs = stmt.executeQuery();

            while (rs.next()) {
                quantidade++;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexao.fecharResultSet(rs);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }

        return quantidade;
    }

    public ArrayList<Zona> consultarZonasEleitorais() throws
            SQLException, ClassNotFoundException {

        ArrayList<Zona> zonasList = new ArrayList<Zona>();

        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;
        Zona zona = null;

        Conexao conexao = new Conexao();

        sql = "SELECT NUM_ZONA, NUM_POLO, NOM_MUNICIPIO "
                + "FROM VW_ZONA ORDER BY NUM_ZONA";

        try {

            con = (Connection) conexao.abrirConexao();
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                zona = new Zona();
                zona.setNumZona(rs.getString("NUM_ZONA"));
                zona.setPolo(rs.getString("NUM_POLO"));
                zona.setMunicipio(rs.getString("NOM_MUNICIPIO"));

                zonasList.add(zona);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexao.fecharResultSet(rs);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }

        return zonasList;
    }

    public Zona consultarZona(Integer numZona) throws SQLException,
            ClassNotFoundException {
        PreparedStatement stmt = null;
        String sql;
        ResultSet rs = null;
        Connection con = null;
        Zona zona = null;

        Conexao conexao = new Conexao();

        sql = "SELECT NUM_ZONA, NUM_POLO, NOM_MUNICIPIO "
                + "FROM VW_ZONA WHERE NUM_ZONA = " + numZona;

        try {

            con = (Connection) conexao.abrirConexao();
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                zona = new Zona();
                zona.setNumZona(rs.getString("NUM_ZONA"));
                zona.setPolo(rs.getString("NUM_POLO"));
                zona.setMunicipio(rs.getString("NOM_MUNICIPIO"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexao.fecharResultSet(rs);
            conexao.fecharStatement(stmt);
            conexao.fecharConexao(con);
        }
        return zona;
    }

}
