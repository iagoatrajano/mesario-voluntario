package mesario.controle.composicaoEje;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.List;

public interface IRepositorioComposicaoEje {
	
	public List consultarComposicaoEje()throws SQLException,ClassNotFoundException,ParseException;
	
	public void editarComposicaoEje(ComposicaoEje composicao) throws ClassNotFoundException;

	public boolean existeComposicaoEje(String id) throws ClassNotFoundException;
	
	public void inserirComposicaoEje(ComposicaoEje composicao) throws ClassNotFoundException;
	
	public String[] consultarDadosEje()throws SQLException,ClassNotFoundException, ParseException;
}
