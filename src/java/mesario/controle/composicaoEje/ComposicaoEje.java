package mesario.controle.composicaoEje;

public class ComposicaoEje {
	
	private String codObjeto;
	
	private String tipo;
	
	private String tratamento;
	
	private String nome;
	
	private String nacionalidade;
	
	private String estadoCivil;
	
	private String rg;
	
	private String orgaoExp;
	
	private String cpf;
	
	private String endereco;
	
	private String bairro;
	
	private int cep;
	
	private String cidade;
	
	private String estado;
	
	private String cargo;	
	
	
	
	public ComposicaoEje(String cod_objeto, String tipo, String tratamento,
			String nome, String nacionalidade, String estadoCivil, String rg,
			String orgaoExp, String cpf, String endereco, String bairro,
			int cep, String cidade, String estado, String cargo) {
		super();
		this.codObjeto = cod_objeto;
		this.tipo = tipo;
		this.tratamento = tratamento;
		this.nome = nome;
		this.nacionalidade = nacionalidade;
		this.estadoCivil = estadoCivil;
		this.rg = rg;
		this.orgaoExp = orgaoExp;
		this.cpf = cpf;
		this.endereco = endereco;
		this.bairro = bairro;
		this.cep = cep;
		this.cidade = cidade;
		this.estado = estado;
		this.cargo = cargo;
	}
	public ComposicaoEje() {
		super();
	}
	public String getCodObjeto() {
		return codObjeto;
	}
	public void setCodObjeto(String cod_objeto) {
		this.codObjeto = cod_objeto;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getTratamento() {
		return tratamento;
	}
	public void setTratamento(String tratamento) {
		this.tratamento = tratamento;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getOrgaoExp() {
		return orgaoExp;
	}
	public void setOrgaoExp(String orgaoExp) {
		this.orgaoExp = orgaoExp;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public int getCep() {
		return cep;
	}
	public void setCep(int cep) {
		this.cep = cep;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

}
