package mesario.controle;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import mesario.controle.composicaoEje.CadastroComposicaoEje;
import mesario.controle.composicaoEje.ComposicaoEje;
import mesario.excecoes.RepositorioException;

public class ControleCadastrarComposicaoEje {

	private CadastroComposicaoEje cadastroComposicaoEje = new CadastroComposicaoEje();

	public List consultarComposicaoEje() throws RepositorioException,
			SQLException, ClassNotFoundException, ParseException {
		return this.cadastroComposicaoEje.consultarComposicaoEje();
	}
	
	public void editarComposicaoEje(ComposicaoEje composicao) throws ClassNotFoundException{
		this.cadastroComposicaoEje.editarComposicaoEje(composicao);
	}
	
	public boolean existeComposicaoEje(String id) throws ClassNotFoundException{
		return this.cadastroComposicaoEje.existeComposicaoEje(id);
	}
	
	public void inserirComposicaoEje(ComposicaoEje composicao) throws ClassNotFoundException{
		this.cadastroComposicaoEje.inserirComposicaoEje(composicao);
	}

}
