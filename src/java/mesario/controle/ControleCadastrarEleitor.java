package mesario.controle;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import mesario.controle.mesario.CadastroEleitor;
import mesario.controle.mesario.Eleitor;
import mesario.controle.mesario.Zona;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.excecoes.RepositorioException;

public class ControleCadastrarEleitor {

  private CadastroEleitor cadastroEleitor = new CadastroEleitor();

  public Eleitor consultarEleitor(String titulo) throws RepositorioException,
          SQLException, ClassNotFoundException, ParseException {
    return this.cadastroEleitor.consultarEleitor(titulo);
  }

  public List consultarOcupacao() throws RepositorioException, SQLException,
          ClassNotFoundException {
    return this.cadastroEleitor.consultarOcupacao();
  }

  public String consultarOcupacao(int numero) throws RepositorioException,
          SQLException, ClassNotFoundException {
    return this.cadastroEleitor.consultarOcupacao(numero);
  }

  public int cadastraEleitor(Eleitor eleitor) throws RepositorioException,
          SQLException, ClassNotFoundException {
    return this.cadastroEleitor.cadastraEleitor(eleitor);
  }

  public boolean consultarEleitorCadastrado(String titulo, String numZona)
          throws SQLException, ClassNotFoundException {
    return this.cadastroEleitor.consultarEleitorCadastrado(titulo, numZona);
  }

  public List consultarDatasInscricao(int zona) throws SQLException, ClassNotFoundException {
    return this.cadastroEleitor.consultarDatasInscricao(zona);
  }

  public List consultarEleitorAno(String ano, int zona, String tipoOrdenacao) throws SQLException, ClassNotFoundException, ParseException {
    return this.cadastroEleitor.consultarEleitorAno(ano, zona, tipoOrdenacao);
  }

  public ArrayList<UniversidadeConveniada> consultarEstatisticasPorEntidade() throws ClassNotFoundException {
    return this.cadastroEleitor.consultarEstatisticasPorEntidade();
  }

  public LinkedHashMap<String, Integer> consultarEstatisticasPorAno() throws ClassNotFoundException {
    return this.cadastroEleitor.consultarEstatisticasPorAno();
  }

  //public List consultarEstatisticasPorPeriodo(Date dataInicialPeriodo, Date dataFinalPeriodo) throws ClassNotFoundException{
  public int consultarEstatisticasPorPeriodo(String dataInicialPeriodo, String dataFinalPeriodo) throws ClassNotFoundException {
    return this.cadastroEleitor.consultarEstatisticasPorPeriodo(dataInicialPeriodo, dataFinalPeriodo);
  }

  public ArrayList<Zona> consultarZonasEleitorais() throws
          SQLException, ClassNotFoundException {
    return this.cadastroEleitor.consultarZonasEleitorais();
  }

  public Zona consultarZona(Integer numZona) throws
          SQLException, ClassNotFoundException {
    return this.cadastroEleitor.consultarZona(numZona);
  }
}
