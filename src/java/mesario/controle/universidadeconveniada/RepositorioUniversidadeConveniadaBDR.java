package mesario.controle.universidadeconveniada;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import mesario.util.Conexao;
import mesario.util.Util;

public class RepositorioUniversidadeConveniadaBDR implements IRepositorioUniversidadeConveniada {

  public void editarDataFinalUniversidade(int codObjeto, Date dataFinal) throws ClassNotFoundException {
    PreparedStatement stmt = null;
    String sql;

    Connection con = null;

    Conexao conexao = new Conexao();
    sql = "UPDATE universidade_conveniada set "
            + "data_final_convenio = ? "
            + "WHERE cod_objeto = ? ";
    try {
      con = (Connection) conexao.abrirConexao();
      stmt = con.prepareStatement(sql);
      stmt.setDate(1, dataFinal);
      stmt.setInt(2, codObjeto);

      stmt.executeUpdate();

      stmt.close();
    } catch (Exception e) {
      e.printStackTrace();

    } finally {
      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);
    }
  }

  //Verifica se existe algum Numero de Convenio igual no mesmo ano.
  public boolean existeNumero(String numeroAnoConvenio, int codObjeto) throws SQLException, ClassNotFoundException {

    PreparedStatement stmt = null;
    Connection con = null;
    Conexao conexao = new Conexao();
    ResultSet rs = null;
    boolean retorno = false;

    String sql = "SELECT * FROM universidade_conveniada WHERE num_convenio || ano_convenio = ? ";
    try {
      con = (Connection) conexao.abrirConexao();
      stmt = con.prepareStatement(sql);
      stmt.setString(1, numeroAnoConvenio);
      rs = stmt.executeQuery();

      if (rs.next()) {
        if (!rs.getString("COD_OBJETO").equals(String.valueOf(codObjeto))) {
          retorno = true;
        }
      }

    } catch (Exception e) {
      e.printStackTrace();

    } finally {
      conexao.fecharResultSet(rs);
      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);
    }

    return retorno;
  }

  public void editarUniversidade(UniversidadeConveniada universidade) throws ClassNotFoundException {
    PreparedStatement stmt = null;
    String sql;

    Connection con = null;

    Conexao conexao = new Conexao();
    sql = "UPDATE universidade_conveniada set "
            + "razao_social = ?, "
            + "nome_fantasia = ?, "
            + "sigla = ?, "
            + "tipo_pessoa_juridica = ?, "
            + "titulo_representante = ?, "
            + "nome_representante = ?, "
            + "endereco_representante = ?, "
            + "cidade_representante = ?, "
            + "estado_representante = ?, "
            + "nacionalidade_representante = ?, "
            + "estado_civil_representante = ?, "
            + "rg_representante = ?, "
            + "orgao_exp_rg_representante = ?, "
            + "cpf_representante = ?, "
            + "data_inicial_convenio = ?, "
            + "data_final_convenio = ?, "
            + "convenio_valido = ?, "
            + "bairro_representante = ?, "
            + "cep_representante = ?, "
            + "cnpj = ?, "
            + "num_convenio = ?, "
            + "ano_convenio = ?, "
            + "email = ?, "
            + "telefone = ? "
            + "WHERE cod_objeto = ? ";

    try {
      con = (Connection) conexao.abrirConexao();

      stmt = con.prepareStatement(sql);
      stmt.setString(1, universidade.getRazaoSocial());
      stmt.setString(2, universidade.getNomeFantasia());
      stmt.setString(3, universidade.getSigla());
      stmt.setInt(4, universidade.getTipoPessoaJuridica());
      stmt.setString(5, universidade.getTituloRepresentante());
      stmt.setString(6, universidade.getNomeRepresentante());
      stmt.setString(7, universidade.getEnderecoRepresentante());
      stmt.setString(8, universidade.getCidadeRepresentante());
      stmt.setString(9, universidade.getEstadoRepresentante());
      stmt.setString(10, universidade.getNacionalidadeRepresentante());
      stmt.setString(11, universidade.getEstadoCivilRepresentante());
      stmt.setString(12, universidade.getRgRepresentante());
      stmt.setString(13, universidade.getOrgaoExpRgRepresentante());
      stmt.setString(14, universidade.getCpfRepresentante());
      stmt.setDate(15, universidade.getDataInicialConvenio());
      stmt.setDate(16, universidade.getDataFinalConvenio());

      if (universidade.getConvenioValido().equalsIgnoreCase("V�lido") || universidade.getConvenioValido() == "1") {
        stmt.setString(17, "1");
      } else {
        stmt.setString(17, "0");
      }
      stmt.setString(18, universidade.getBairroRepresentante());
      stmt.setInt(19, universidade.getCepRepresentante());
      stmt.setString(20, universidade.getCnpj());
      stmt.setInt(21, universidade.getNumeroDoConvenio());
      stmt.setInt(22, universidade.getAnoDoConvenio());
      stmt.setString(23, universidade.getEmail());
      stmt.setString(24, universidade.getTelefone());
      stmt.setString(25, universidade.getCodObjeto());

      stmt.executeUpdate();

      stmt.close();
    } catch (Exception e) {
      e.printStackTrace();

    } finally {

      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);
    }

  }

  public UniversidadeConveniada consultarUniversidade(int codObjeto) throws ClassNotFoundException {
    UniversidadeConveniada universidade = null;
    PreparedStatement stmt = null;
    PreparedStatement stmt2 = null;
    ResultSet rs = null;
    ResultSet rsData = null;
    Connection con = null;
    Util util = new Util();

    Conexao conexao = new Conexao();

    try {
      con = (Connection) conexao.abrirConexao();

      String sql = "select * from universidade_conveniada where cod_objeto = ?";
      stmt = con.prepareStatement(sql);
      stmt.setInt(1, codObjeto);

      rs = stmt.executeQuery();
      if (rs.next()) {
        universidade = new UniversidadeConveniada();
        universidade.setCodObjeto(rs.getString("cod_objeto"));
        universidade.setBairroRepresentante(rs.getString("bairro_representante"));
        universidade.setCepRepresentante(rs.getInt("cep_representante"));
        universidade.setCidadeRepresentante(rs.getString("cidade_representante"));
        universidade.setCnpj(rs.getString("CNPJ"));
        if (rs.getString("convenio_valido").equals("1")) {
          universidade.setConvenioValido("V�lido");
        } else {
          universidade.setConvenioValido("Inv�lido");
        }
        universidade.setCpfRepresentante(rs.getString("cpf_representante"));
        universidade.setDataFinalConvenio(rs.getDate("data_final_convenio"));
        universidade.setDataInicialConvenio(rs.getDate("data_inicial_convenio"));
        universidade.setEnderecoRepresentante(rs.getString("endereco_representante"));
        universidade.setEstadoCivilRepresentante(rs.getString("estado_civil_representante"));
        universidade.setEstadoRepresentante(rs.getString("estado_representante"));
        universidade.setNacionalidadeRepresentante(rs.getString("nacionalidade_representante"));
        universidade.setNomeFantasia(rs.getString("nome_fantasia"));
        universidade.setNomeRepresentante(rs.getString("nome_representante"));
        universidade.setOrgaoExpRgRepresentante(rs.getString("orgao_exp_rg_representante"));
        universidade.setRazaoSocial(rs.getString("razao_social"));
        universidade.setRgRepresentante(rs.getString("rg_representante"));
        universidade.setSigla(rs.getString("sigla"));
        universidade.setTipoPessoaJuridica(rs.getInt("tipo_pessoa_juridica"));
        universidade.setTituloRepresentante(rs.getString("titulo_representante"));
        universidade.setNumeroDoConvenio(rs.getInt("num_convenio"));
        universidade.setAnoDoConvenio(rs.getInt("ano_convenio"));
        universidade.setEmail(rs.getString("email"));
        universidade.setTelefone(rs.getString("telefone"));
        universidade.setEndereco(rs.getString("endereco"));
        universidade.setBairro(rs.getString("bairro"));
        universidade.setCidade(rs.getString("cidade"));
        universidade.setEstado(rs.getString("estado"));
        universidade.setCep(rs.getInt("cep"));

        try {
          String sqlDataHoraAtual = "(SELECT SYSDATE FROM DUAL)";
          stmt2 = con.prepareStatement(sqlDataHoraAtual);
          rsData = stmt2.executeQuery();
          if (rsData.next()) {
            if (universidade.getDataFinalConvenio().before(rsData.getDate("SYSDATE"))) {
              universidade.setConvenioValido("Encerrado");
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          conexao.fecharResultSet(rsData);
          conexao.fecharStatement(stmt2);
        }

      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      conexao.fecharResultSet(rs);
      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);
    }
    return universidade;
  }

  public ArrayList<UniversidadeConveniada> listarTodasUniversidades() throws ClassNotFoundException {
    ArrayList<UniversidadeConveniada> lista = new ArrayList<UniversidadeConveniada>();
    PreparedStatement stmt = null;
    PreparedStatement stmt2 = null;
    String sql;
    ResultSet rs = null;
    ResultSet rsData = null;
    Connection con = null;
    UniversidadeConveniada universidade = null;

    Conexao conexao = new Conexao();
    try {
      con = (Connection) conexao.abrirConexao();

      sql = "select * from universidade_conveniada order by sigla";
      stmt = con.prepareStatement(sql);

      rs = stmt.executeQuery();
      while (rs.next()) {
        universidade = new UniversidadeConveniada();
        universidade.setCodObjeto(rs.getString("cod_objeto"));
        universidade.setBairroRepresentante(rs.getString("bairro_representante"));
        universidade.setCepRepresentante(rs.getInt("cep_representante"));
        universidade.setCidadeRepresentante(rs.getString("cidade_representante"));
        universidade.setCnpj(rs.getString("CNPJ"));
        if (rs.getString("convenio_valido").equals("1")) {
          universidade.setConvenioValido("V�lido");
        } else {
          universidade.setConvenioValido("Inv�lido");
        }
        universidade.setCpfRepresentante(rs.getString("cpf_representante"));
        universidade.setDataFinalConvenio(rs.getDate("data_final_convenio"));
        universidade.setDataInicialConvenio(rs.getDate("data_inicial_convenio"));
        universidade.setEnderecoRepresentante(rs.getString("endereco_representante"));
        universidade.setEstadoCivilRepresentante(rs.getString("estado_civil_representante"));
        universidade.setEstadoRepresentante(rs.getString("estado_representante"));
        universidade.setNacionalidadeRepresentante(rs.getString("nacionalidade_representante"));
        universidade.setNomeFantasia(rs.getString("nome_fantasia"));
        universidade.setNomeRepresentante(rs.getString("nome_representante"));
        universidade.setOrgaoExpRgRepresentante(rs.getString("orgao_exp_rg_representante"));
        universidade.setRazaoSocial(rs.getString("razao_social"));
        universidade.setRgRepresentante(rs.getString("rg_representante"));
        universidade.setSigla(rs.getString("sigla"));
        universidade.setTipoPessoaJuridica(rs.getInt("tipo_pessoa_juridica"));
        universidade.setTituloRepresentante(rs.getString("titulo_representante"));
        universidade.setNumeroDoConvenio(rs.getInt("num_convenio"));
        universidade.setAnoDoConvenio(rs.getInt("ano_convenio"));

        try {
          String sqlDataHoraAtual = "(SELECT SYSDATE FROM DUAL)";
          stmt2 = con.prepareStatement(sqlDataHoraAtual);
          rsData = stmt2.executeQuery();
          if (rsData.next()) {
            if (universidade.getDataFinalConvenio().before(rsData.getDate("SYSDATE"))) {
              universidade.setConvenioValido("Encerrado");
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          conexao.fecharResultSet(rsData);
          conexao.fecharStatement(stmt2);
        }

        lista.add(universidade);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      conexao.fecharResultSet(rs);
      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);
    }
    return lista;
  }

  public List listasUniversidadesConveniadas() throws SQLException, ClassNotFoundException {
    PreparedStatement stmt = null;
    String sql;
    ResultSet rs = null;
    Connection con = null;
    UniversidadeConveniada universidade = null;
    List listaUniversidade = null;

    Conexao conexao = new Conexao();

    sql = "SELECT COD_OBJETO,RAZAO_SOCIAL,NOME_FANTASIA,SIGLA,  "
            + "TIPO_PESSOA_JURIDICA,TITULO_REPRESENTANTE,NOME_REPRESENTANTE,RG_REPRESENTANTE,ORGAO_EXP_RG_REPRESENTANTE, "
            + "CPF_REPRESENTANTE,DATA_INICIAL_CONVENIO,DATA_FINAL_CONVENIO, CNPJ, NUM_CONVENIO, ANO_CONVENIO "
            + "FROM UNIVERSIDADE_CONVENIADA  "
            + "WHERE CONVENIO_VALIDO = 1 "
            + "AND DATA_FINAL_CONVENIO >=  TO_CHAR(SYSDATE) "
            + "ORDER BY SIGLA";

    try {

      con = (Connection) conexao.abrirConexao();

      stmt = con.prepareStatement(sql);

      rs = stmt.executeQuery();

      listaUniversidade = new Vector<UniversidadeConveniada>();

      while (rs.next()) {

        universidade = new UniversidadeConveniada();

        universidade.setCodObjeto(rs.getString("COD_OBJETO"));
        universidade.setSigla(rs.getString("SIGLA"));
        universidade.setRazaoSocial(rs.getString("RAZAO_SOCIAL"));
        universidade.setNomeFantasia(rs.getString("NOME_FANTASIA"));
        universidade.setTipoPessoaJuridica(rs.getInt("TIPO_PESSOA_JURIDICA"));
        universidade.setTituloRepresentante(rs.getString("TITULO_REPRESENTANTE"));
        universidade.setNomeRepresentante(rs.getString("NOME_REPRESENTANTE"));
        universidade.setRgRepresentante(rs.getString("RG_REPRESENTANTE"));
        universidade.setOrgaoExpRgRepresentante(rs.getString("ORGAO_EXP_RG_REPRESENTANTE"));
        universidade.setCpfRepresentante(rs.getString("CPF_REPRESENTANTE"));
        universidade.setDataInicialConvenio(rs.getDate("DATA_INICIAL_CONVENIO"));
        universidade.setDataFinalConvenio(rs.getDate("DATA_FINAL_CONVENIO"));
        universidade.setCnpj(rs.getString("CNPJ"));
        universidade.setNumeroDoConvenio(rs.getInt("NUM_CONVENIO"));
        universidade.setAnoDoConvenio(rs.getInt("ANO_CONVENIO"));

        listaUniversidade.add(universidade);
      }

    } catch (SQLException e) {
      throw e;
    } finally {
      conexao.fecharResultSet(rs);
      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);
    }

    return listaUniversidade;
  }

  public void cadastrarUniversidadeConveniada(UniversidadeConveniada universidade) throws ClassNotFoundException, SQLException {

    PreparedStatement stmt = null;
    String sql = "";
    String sqlInsert = "";
    String sqlValues = "";
    String sqlDataHoraAtual = "(SELECT SYSDATE FROM DUAL)";
    ResultSet rs = null;
    Connection con = null;

    Conexao conexao = new Conexao();

    sqlInsert = "INSERT INTO UNIVERSIDADE_CONVENIADA ("
            + "  RAZAO_SOCIAL, NOME_FANTASIA, "
            + "  SIGLA, TIPO_PESSOA_JURIDICA, "
            + "  TITULO_REPRESENTANTE, NOME_REPRESENTANTE,ENDERECO_REPRESENTANTE, "
            + "  CIDADE_REPRESENTANTE, ESTADO_REPRESENTANTE, NACIONALIDADE_REPRESENTANTE, "
            + "  ESTADO_CIVIL_REPRESENTANTE, RG_REPRESENTANTE, ORGAO_EXP_RG_REPRESENTANTE, "
            + "  CPF_REPRESENTANTE, DATA_INICIAL_CONVENIO,DATA_FINAL_CONVENIO, "
            + "  CONVENIO_VALIDO , BAIRRO_REPRESENTANTE, CEP_REPRESENTANTE, CNPJ , EMAIL, TELEFONE, "
            + "  ENDERECO, BAIRRO, CEP, CIDADE, ESTADO";

    sqlValues = "VALUES( ?, ?, ?, ?, "
            + "       ?, ?, ?, ?, ?, "
            + "       ?, ?, ?, ?, ?, "
            + "       " + sqlDataHoraAtual + " , ?, ?, ?, ?, ?,? ,?, "
            + "       ?, ?, ?, ?, ?";

    sqlInsert += ") ";
    sqlValues += ") ";

    sql = sqlInsert + sqlValues;

    try {

      con = (Connection) conexao.abrirConexao();

      stmt = con.prepareStatement(sql);

      stmt.setString(1, universidade.getRazaoSocial());
      stmt.setString(2, universidade.getNomeFantasia());
      stmt.setString(3, universidade.getSigla());
      stmt.setInt(4, universidade.getTipoPessoaJuridica());

      stmt.setString(5, universidade.getTituloRepresentante());
      stmt.setString(6, universidade.getNomeRepresentante());
      stmt.setString(7, universidade.getEnderecoRepresentante());
      stmt.setString(8, universidade.getCidadeRepresentante());
      stmt.setString(9, universidade.getEstadoRepresentante());

      stmt.setString(10, universidade.getNacionalidadeRepresentante());
      stmt.setString(11, universidade.getEstadoCivilRepresentante());
      stmt.setString(12, universidade.getRgRepresentante());
      stmt.setString(13, universidade.getOrgaoExpRgRepresentante());
      stmt.setString(14, universidade.getCpfRepresentante());

      //Date utilDate = new Date("31/12/2015");  // import java.util.Date;
      //java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
      stmt.setDate(15, consultarDataFinalConvenio());
      stmt.setString(16, universidade.getConvenioValido());
      stmt.setString(17, universidade.getBairroRepresentante());
      stmt.setInt(18, universidade.getCepRepresentante());
      stmt.setString(19, universidade.getCnpj());
      stmt.setString(20, universidade.getEmail());
      stmt.setString(21, universidade.getTelefone());
      stmt.setString(22, universidade.getEndereco());
      stmt.setString(23, universidade.getBairro());
      stmt.setInt(24, universidade.getCep());
      stmt.setString(25, universidade.getCidade());
      stmt.setString(26, universidade.getEstado());

      stmt.execute();
      stmt.clearParameters();

    } catch (SQLException e) {
      throw e;
    } finally {

      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);

    }

  }

  public java.sql.Date consultarDataFinalConvenio() throws SQLException, ClassNotFoundException {
    PreparedStatement stmt = null;
    String sql;
    ResultSet rs = null;
    Connection con = null;
    UniversidadeConveniada universidade = null;

    Conexao conexao = new Conexao();
    java.sql.Date sqlDate = null;
    sql = "select data_final_convenio from configuracao ";

    try {

      con = (Connection) conexao.abrirConexao();

      stmt = con.prepareStatement(sql);

      rs = stmt.executeQuery();

      while (rs.next()) {
        sqlDate = rs.getDate("data_final_convenio");
      }

    } catch (SQLException e) {
      throw e;
    } finally {
      conexao.fecharResultSet(rs);
      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);
    }

    return sqlDate;
  }

  public boolean consultarCnpjExistente(String cnpj) throws ClassNotFoundException, SQLException {

    boolean existe = false;

    PreparedStatement stmt = null;
    String sql;
    ResultSet rs = null;
    Connection con = null;
    String sqlDataHoraAtual = "(SELECT SYSDATE FROM DUAL)";
    UniversidadeConveniada universidade = null;

    Conexao conexao = new Conexao();
    //SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    //java.sql.Date data = new java.sql.Date(format.parse(dataStr).getTime());

    sql = "select cnpj from universidade_conveniada "
            + "where cnpj = " + "'" + cnpj + "' "
            + "and " + sqlDataHoraAtual + "< data_final_convenio";

    try {

      con = (Connection) conexao.abrirConexao();

      stmt = con.prepareStatement(sql);

      rs = stmt.executeQuery();

      while (rs.next()) {
        existe = true;
      }

    } catch (SQLException e) {
      throw e;
    } finally {
      conexao.fecharResultSet(rs);
      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);
    }

    return existe;
  }

  public String consultarCodObjetoUniversidade(String cnpj) throws ClassNotFoundException, SQLException {

    PreparedStatement stmt = null;
    String sql;
    ResultSet rs = null;
    Connection con = null;

    String codObjeto = null;

    Conexao conexao = new Conexao();

    sql = "select cod_objeto from universidade_conveniada "
            + "where cnpj = " + "'" + cnpj + "' ";

    try {

      con = (Connection) conexao.abrirConexao();

      stmt = con.prepareStatement(sql);

      rs = stmt.executeQuery();

      while (rs.next()) {
        codObjeto = rs.getString("cod_objeto");
      }

    } catch (SQLException e) {
      throw e;
    } finally {
      conexao.fecharResultSet(rs);
      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);
    }

    return codObjeto;
  }

  public Date getDataFinal() throws ClassNotFoundException {
    PreparedStatement stmt = null;
    String sql;
    ResultSet rs = null;
    Connection con = null;

    Date dataFinal = null;

    Conexao conexao = new Conexao();

    sql = "select * from configuracao";

    try {

      con = (Connection) conexao.abrirConexao();

      stmt = con.prepareStatement(sql);

      rs = stmt.executeQuery();

      while (rs.next()) {
        dataFinal = rs.getDate("data_final_convenio");
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      conexao.fecharResultSet(rs);
      conexao.fecharStatement(stmt);
      conexao.fecharConexao(con);
    }

    return dataFinal;
  }
}
