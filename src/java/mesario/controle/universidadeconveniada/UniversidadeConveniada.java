package mesario.controle.universidadeconveniada;

import java.sql.Date;

public class UniversidadeConveniada {

  private String codObjeto;

  private String razaoSocial;

  private String nomeFantasia;

  private String sigla;

  private String enderecoRepresentante;

  private String cidadeRepresentante;

  private String estadoRepresentante;

  private String nacionalidadeRepresentante;

  private String estadoCivilRepresentante;

  private String bairroRepresentante;

  private int cepRepresentante;

  private int tipoPessoaJuridica;

  private String tituloRepresentante;

  private String nomeRepresentante;

  private String rgRepresentante;

  private String orgaoExpRgRepresentante;

  private String cpfRepresentante;

  private String convenioValido;

  private Date dataInicialConvenio;

  private Date dataFinalConvenio;

  private String cnpj;

  private int quantidadeEleitores;

  private int numeroDoConvenio;

  private int anoDoConvenio;

  private String telefone;

  private String email;

  private String endereco;
  private String bairro;
  private int cep;
  private String cidade;
  private String estado;

  public UniversidadeConveniada(String codObjeto, String razaoSocial,
          String nomeFantasia, String sigla, String enderecoRepresentante,
          String cidadeRepresentante, String estadoRepresentante,
          String nacionalidadeRepresentante, String estadoCivilRepresentante,
          String bairroRepresentante, int cepRepresentante,
          int tipoPessoaJuridica, String tituloRepresentante,
          String nomeRepresentante, String rgRepresentante,
          String orgaoExpRgRepresentante, String cpfRepresentante,
          Date dataInicialConvenio, Date dataFinalConvenio, int numeroDoConvenio,
          int anoDoConvenio, String endereco,
          String bairro, int cep, String cidade, String estado) {
    super();
    this.codObjeto = codObjeto;
    this.razaoSocial = razaoSocial;
    this.nomeFantasia = nomeFantasia;
    this.sigla = sigla;
    this.enderecoRepresentante = enderecoRepresentante;
    this.cidadeRepresentante = cidadeRepresentante;
    this.estadoRepresentante = estadoRepresentante;
    this.nacionalidadeRepresentante = nacionalidadeRepresentante;
    this.estadoCivilRepresentante = estadoCivilRepresentante;
    this.bairroRepresentante = bairroRepresentante;
    this.cepRepresentante = cepRepresentante;
    this.tipoPessoaJuridica = tipoPessoaJuridica;
    this.tituloRepresentante = tituloRepresentante;
    this.nomeRepresentante = nomeRepresentante;
    this.rgRepresentante = rgRepresentante;
    this.orgaoExpRgRepresentante = orgaoExpRgRepresentante;
    this.cpfRepresentante = cpfRepresentante;
    this.dataInicialConvenio = dataInicialConvenio;
    this.dataFinalConvenio = dataFinalConvenio;
    this.numeroDoConvenio = numeroDoConvenio;
    this.anoDoConvenio = anoDoConvenio;
    this.endereco = endereco;
    this.bairro = bairro;
    this.cep = cep;
    this.cidade = cidade;
    this.estado = estado;
  }

  public UniversidadeConveniada() {

  }

  public String getCodObjeto() {
    return codObjeto;
  }

  public void setCodObjeto(String codObjeto) {
    this.codObjeto = codObjeto;
  }

  public String getRazaoSocial() {
    return razaoSocial;
  }

  public void setRazaoSocial(String razaoSocial) {
    this.razaoSocial = razaoSocial;
  }

  public String getNomeFantasia() {
    return nomeFantasia;
  }

  public void setNomeFantasia(String nomeFantasia) {
    this.nomeFantasia = nomeFantasia;
  }

  public String getSigla() {
    return sigla;
  }

  public void setSigla(String sigla) {
    this.sigla = sigla;
  }

  public int getTipoPessoaJuridica() {
    return tipoPessoaJuridica;
  }

  public void setTipoPessoaJuridica(int tipoPessoaJuridica) {
    this.tipoPessoaJuridica = tipoPessoaJuridica;
  }

  public String getTituloRepresentante() {
    return tituloRepresentante;
  }

  public void setTituloRepresentante(String tituloRepresentante) {
    this.tituloRepresentante = tituloRepresentante;
  }

  public String getRgRepresentante() {
    return rgRepresentante;
  }

  public void setRgRepresentante(String rgRepresentante) {
    this.rgRepresentante = rgRepresentante;
  }

  public String getOrgaoExpRgRepresentante() {
    return orgaoExpRgRepresentante;
  }

  public void setOrgaoExpRgRepresentante(String orgaoExpRgRepresentante) {
    this.orgaoExpRgRepresentante = orgaoExpRgRepresentante;
  }

  public String getCpfRepresentante() {
    return cpfRepresentante;
  }

  public void setCpfRepresentante(String cpfRepresentante) {
    this.cpfRepresentante = cpfRepresentante;
  }

  public Date getDataInicialConvenio() {
    return dataInicialConvenio;
  }

  public void setDataInicialConvenio(Date dataInicialConvenio) {
    this.dataInicialConvenio = dataInicialConvenio;
  }

  public Date getDataFinalConvenio() {
    return dataFinalConvenio;
  }

  public void setDataFinalConvenio(Date dataFinalConvenio) {
    this.dataFinalConvenio = dataFinalConvenio;
  }

  public String getNomeRepresentante() {
    return nomeRepresentante;
  }

  public void setNomeRepresentante(String nomeRepresentante) {
    this.nomeRepresentante = nomeRepresentante;
  }

  public String getEnderecoRepresentante() {
    return enderecoRepresentante;
  }

  public void setEnderecoRepresentante(String enderecoRepresentante) {
    this.enderecoRepresentante = enderecoRepresentante;
  }

  public String getCidadeRepresentante() {
    return cidadeRepresentante;
  }

  public void setCidadeRepresentante(String cidadeRepresentante) {
    this.cidadeRepresentante = cidadeRepresentante;
  }

  public String getEstadoRepresentante() {
    return estadoRepresentante;
  }

  public void setEstadoRepresentante(String estadoRepresentante) {
    this.estadoRepresentante = estadoRepresentante;
  }

  public String getNacionalidadeRepresentante() {
    return nacionalidadeRepresentante;
  }

  public void setNacionalidadeRepresentante(String nacionalidadeRepresentante) {
    this.nacionalidadeRepresentante = nacionalidadeRepresentante;
  }

  public String getEstadoCivilRepresentante() {
    return estadoCivilRepresentante;
  }

  public void setEstadoCivilRepresentante(String estadoCivilRepresentante) {
    this.estadoCivilRepresentante = estadoCivilRepresentante;
  }

  public String getBairroRepresentante() {
    return bairroRepresentante;
  }

  public void setBairroRepresentante(String bairroRepresentante) {
    this.bairroRepresentante = bairroRepresentante;
  }

  public int getCepRepresentante() {
    return cepRepresentante;
  }

  public void setCepRepresentante(int cepRepresentante) {
    this.cepRepresentante = cepRepresentante;
  }

  public String getConvenioValido() {
    return convenioValido;
  }

  public void setConvenioValido(String convenioValido) {
    this.convenioValido = convenioValido;
  }

  public String getCnpj() {
    return cnpj;
  }

  public void setCnpj(String cnpj) {
    this.cnpj = cnpj;
  }

  public void setQuantidadeEleitores(int quantidadeEleitores) {
    this.quantidadeEleitores = quantidadeEleitores;
  }

  public int getQuantidadeEleitores() {
    return quantidadeEleitores;
  }

  public void setNumeroDoConvenio(int numeroDoConvenio) {
    this.numeroDoConvenio = numeroDoConvenio;
  }

  public int getNumeroDoConvenio() {
    return numeroDoConvenio;
  }

  public void setAnoDoConvenio(int anoDoConvenio) {
    this.anoDoConvenio = anoDoConvenio;
  }

  public int getAnoDoConvenio() {
    return anoDoConvenio;
  }

  public String getNumeroAnoConvenio() {
    String res = "";
    if (this.numeroDoConvenio != 0) {
      res = this.numeroDoConvenio + "/" + this.anoDoConvenio;
    }
    return res;
  }

  public String getNumero() {
    String numero = "";
    if (this.numeroDoConvenio != 0) {
      numero = "" + this.numeroDoConvenio;
    }
    return numero;
  }

  public String getAno() {
    String ano = "";
    if (this.anoDoConvenio != 0) {
      ano = "" + this.anoDoConvenio;
    }
    return ano;
  }

  public String getTelefone() {
    return telefone;
  }

  public void setTelefone(String telefone) {
    this.telefone = telefone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEndereco() {
    return endereco;
  }

  public void setEndereco(String endereco) {
    this.endereco = endereco;
  }

  public String getBairro() {
    return bairro;
  }

  public void setBairro(String bairro) {
    this.bairro = bairro;
  }

  public int getCep() {
    return cep;
  }

  public void setCep(int cep) {
    this.cep = cep;
  }

  public String getCidade() {
    return cidade;
  }

  public void setCidade(String cidade) {
    this.cidade = cidade;
  }

  public String getEstado() {
    return estado;
  }

  public void setEstado(String estado) {
    this.estado = estado;
  }

}
