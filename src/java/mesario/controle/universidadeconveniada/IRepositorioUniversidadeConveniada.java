package mesario.controle.universidadeconveniada;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public interface IRepositorioUniversidadeConveniada {
	
	public void cadastrarUniversidadeConveniada(UniversidadeConveniada universidade)  throws ClassNotFoundException,SQLException;
	
	public List listasUniversidadesConveniadas() throws SQLException,ClassNotFoundException;
	
	boolean consultarCnpjExistente(String cnpj) throws ClassNotFoundException, SQLException;

	public ArrayList <UniversidadeConveniada> listarTodasUniversidades() throws ClassNotFoundException;
	
	public UniversidadeConveniada consultarUniversidade(int codObjeto) throws ClassNotFoundException;
	
	public void editarUniversidade (UniversidadeConveniada universidade) throws ClassNotFoundException;
	
	public void editarDataFinalUniversidade(int codObjeto, Date dataFinal) throws ClassNotFoundException;
	
	public String consultarCodObjetoUniversidade(String cnpj) throws ClassNotFoundException, SQLException;
	
	public Date getDataFinal() throws ClassNotFoundException;
	
	public boolean existeNumero (String numeroAnoConvenio, int  codObjeto) throws SQLException, ClassNotFoundException;
}	
