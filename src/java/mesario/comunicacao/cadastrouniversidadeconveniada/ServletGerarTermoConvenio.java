package mesario.comunicacao.cadastrouniversidadeconveniada;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mesario.comunicacao.Fachada;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.util.Conexao;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 * Servlet implementation class ServletGerarTermoConvenio
 */
public class ServletGerarTermoConvenio extends HttpServlet {

  private static final long serialVersionUID = 1L;

  Fachada fachada = null;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ServletGerarTermoConvenio() {
    super();

  }

  public void init() throws ServletException {
    this.fachada = Fachada.getInstance();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   * response)
   */
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    doPost(req, resp);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   * response)
   */
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    System.out.println("ENTROU AKI!!!");

    Connection con = null;
    String nomeDoArquivo = new String("TermoConvenioCooperacao.pdf");
    String path = getServletContext().getRealPath("/");
    Conexao conexao = null;

    try {
      conexao = new Conexao();

      con = (Connection) conexao.abrirConexao();

      System.out.println("CONEXAO: " + con);

      UniversidadeConveniada universidade = (UniversidadeConveniada) req.getAttribute("universidade");

      if (universidade == null) {
        if (req.getParameter("universidadeSelecionada") == null) {
          universidade = (UniversidadeConveniada) req.getSession().getAttribute("universidadeTeste");
        } else {
          try {
            universidade = fachada.consultarUniversidade(Integer.parseInt(req.getParameter("universidadeSelecionada")));
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
      //
      Map<String, Object> param = new HashMap<String, Object>();

      param.put("nomeRep", universidade.getNomeRepresentante());
      param.put("fantasia", universidade.getNomeFantasia());

      param.put("nacionalidade", universidade.getNacionalidadeRepresentante());
      param.put("sigla", universidade.getSigla());

      param.put("endereco", universidade.getEndereco());

      param.put("estadoCivil", universidade.getEstadoCivilRepresentante());
      param.put("rg", universidade.getRgRepresentante());

      param.put("cnpj", universidade.getCnpj());
      param.put("cpf", universidade.getCpfRepresentante());
      param.put("bairro", universidade.getBairro());

      param.put("estado", universidade.getEstado());
      param.put("razaoSocial", universidade.getRazaoSocial());

      param.put("orgaoExp", universidade.getOrgaoExpRgRepresentante());
      param.put("cidade", universidade.getCidade());

      param.put("cod_objeto_universidade", universidade.getCodObjeto());
      param.put("cep", ((Integer) universidade.getCep()).toString());
      if (universidade.getDataFinalConvenio() == null) {
        try {
          param.put("data_final", fachada.getDataFinal());
        } catch (Exception e) {
          e.printStackTrace();
        }
      } else {
        param.put("data_final", universidade.getDataFinalConvenio());
      }

      String tipoPessoa = "";
      if (universidade.getTipoPessoaJuridica() == 2) {
        tipoPessoa = " pessoa jur�dica de direito p�blico";
      } else {
        tipoPessoa = " pessoa jur�dica de direito privado";
      }
      param.put("tipoPessoa", tipoPessoa);

      param.put("ocupacao", universidade.getTituloRepresentante());

      String pathJasper = getServletContext().getRealPath("/WEB-INF/relatorios");

      System.out.println("PATH JASPER: " + pathJasper);

      File file = new File(pathJasper);
      System.out.println("EXITE ARQUIVO: " + file.exists());

      param.put("SUBREPORT_DIR", pathJasper);

      try {
        JasperPrint impressao;

        impressao = JasperFillManager.fillReport(pathJasper + "/" + "Termo de Convenio de Cooperacao.jasper", param, con);

        resp.addHeader("Content-Type", "application/octet-stream");
        resp.addHeader("Content-Disposition", "attachment; filename=" + nomeDoArquivo);
        resp.addHeader("Content-Transfer-Encoding", "binary");

        JasperExportManager.exportReportToPdfFile(impressao, path + nomeDoArquivo);

        File arquivo = new File(path + nomeDoArquivo);

        OutputStream arqOut = resp.getOutputStream();
        InputStream fileIn = new FileInputStream(arquivo);
        byte[] buffer = new byte[10 * 1024];

        int nread = 0;
        while ((nread = fileIn.read(buffer)) != -1) {
          arqOut.write(buffer, 0, nread);
        }
        fileIn.close();
        arqOut.close();
        arquivo.delete();

      } catch (JRException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    } catch (ClassNotFoundException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally {
      conexao.fecharConexao(con);
    }
  }

}
