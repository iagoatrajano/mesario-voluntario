package mesario.comunicacao.cadastrouniversidadeconveniada;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mesario.comunicacao.Fachada;
import mesario.controle.composicaoEje.RepositorioComposicaoEjeBD;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.util.FuncoesGerais;

/**
 * Servlet implementation class ServletCadastrarUniversidadeConveniada
 */
public class ServletCadastrarUniversidadeConveniada extends HttpServlet {

  private static final long serialVersionUID = 1L;

  Fachada fachada = null;

  public ServletCadastrarUniversidadeConveniada() {
    super();

  }

  public void init() throws ServletException {
    this.fachada = Fachada.getInstance();
  }

  protected void doGet(HttpServletRequest request,
          HttpServletResponse response) throws ServletException, IOException {
    doPost(request, response);
  }

  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {

    UniversidadeConveniada universidade = new UniversidadeConveniada();

    String mensagem = null;

    String cnpj = req.getParameter("cnpj");

    try {
      universidade.setCnpj(cnpj);
      universidade.setRazaoSocial(req.getParameter("razaoSocial"));
      universidade.setNomeFantasia(req.getParameter("nomeFantasia"));
      universidade.setSigla(req.getParameter("sigla"));
      universidade.setTelefone(req.getParameter("phone"));
      universidade.setEmail(req.getParameter("emailConvenio"));
      universidade.setTipoPessoaJuridica(Integer.parseInt(req.getParameter("tipoPessoa")));
      universidade.setEndereco(req.getParameter("enderecoEntidade"));
      universidade.setBairro(req.getParameter("bairroEntidade"));
      universidade.setCep(FuncoesGerais.cepFormatarMascara(req.getParameter("cepEntidade")));
      universidade.setCidade(req.getParameter("cidadeEntidade"));
      universidade.setEstado(req.getParameter("estadoEntidade"));
      //
      //
      universidade.setTituloRepresentante(req.getParameter("cargo"));
      universidade.setNomeRepresentante(req.getParameter("nomeRep"));
      universidade.setEnderecoRepresentante(req.getParameter("endereco"));
      universidade.setBairroRepresentante(req.getParameter("bairro"));
      universidade.setCidadeRepresentante(req.getParameter("cidade"));
      universidade.setCepRepresentante(FuncoesGerais.cepFormatarMascara(req.getParameter("cep")));
      universidade.setEstadoRepresentante(req.getParameter("estado"));
      universidade.setNacionalidadeRepresentante(req.getParameter("nacionalidade"));
      universidade.setEstadoCivilRepresentante(req.getParameter("estadoCivil"));
      universidade.setRgRepresentante(req.getParameter("rg"));
      universidade.setOrgaoExpRgRepresentante(req.getParameter("orgaoExp"));
      universidade.setCpfRepresentante(req.getParameter("cpf"));
      //
      universidade.setConvenioValido("0");
      //

      if (!fachada.consultarCnpjExistente(cnpj)) {

        fachada.cadastrarUniversidadeConveniada(universidade);

        String codObjeto = fachada.consultarCodObjetoUniversidade(universidade.getCnpj());

        universidade.setCodObjeto(codObjeto);

        req.setAttribute("universidade", universidade);

        req.getSession().setAttribute("universidadeTeste", universidade);

        mensagem = "Cadastro efetuado com sucesso! Seu termo de conv�nio ser� gerado e deve ser impresso em 3 vias para assinatura, "
                + "bem como rubrica de todas as folhas. Ap�s a assinatura, para prosseguimento dos tr�mites legais para valida��o do "
                + "conv�nio, favor remeter o termo para a Presid�ncia do TRE-PI, acompanhado de: a) c�pia do documento de constitui��o "
                + "da institui��o de ensino e b) documento que habilita o gestor a responder por esta institui��o. O TRE-PI � situado "
                + "na Pra�a Desembargador Edgar Nogueira, s/n.Centro C�vico, Teresina - PI- 64000-920.";

        req.setAttribute("mensagem", mensagem);
        req.setAttribute("flag", "sim");
        getServletConfig().getServletContext().getRequestDispatcher(
                "/cadastroUniversidade.jsp").forward(req, resp);

      } else {
        try {

          RepositorioComposicaoEjeBD composicaoEjeBD = new RepositorioComposicaoEjeBD();
          String[] dadosEje = composicaoEjeBD.consultarDadosEje();
          String telefoneEje = dadosEje[0];
          String emailEje = dadosEje[1];
          mensagem = "A institui��o j� est� cadastrada. Caso necessite alterar os seus dados cadastrais, entre em contato com a "
                  + "Presid�ncia do TRE-PI, atrav�s do e-mail presi@tre-pi.jus.br ou dos telefones (86) 2107-9820 /9754/9821.";

          req.setAttribute("mensagem", mensagem);
          req.setAttribute("universidade", universidade);
          req.setAttribute("flag", "nao");
          getServletConfig().getServletContext().getRequestDispatcher(
                  "/cadastroUniversidade.jsp").forward(req, resp);

        } catch (Exception e) {
          e.printStackTrace();
        }
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}
