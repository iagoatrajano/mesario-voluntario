package mesario.comunicacao.consultaMesarioVoluntario;

import componente_acesso.controleusuario.usuario.Usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mesario.comunicacao.Fachada;
import mesario.util.Conexao;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 * Servlet implementation class ServletGerarTermoConvenio
 */
public class ServletGerarRelatorioConsulta extends HttpServlet {

  private static final long serialVersionUID = 1L;

  Fachada fachada = null;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ServletGerarRelatorioConsulta() {
    super();

  }

  public void init() throws ServletException {
    this.fachada = Fachada.getInstance();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   * response)
   */
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    doPost(req, resp);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   * response)
   */
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    System.out.println("ENTROU AKI!!!");

    Connection con = null;
    String nomeDoArquivo = new String("consulta mesario.pdf");
    String path = getServletContext().getRealPath("/");
    Conexao conexao = null;

    int zonaInt = 0;
    Usuario usuario = new Usuario();

    try {
      conexao = new Conexao();

      con = (Connection) conexao.abrirConexao();

      System.out.println("CONEXAO: " + con);

      HttpSession session = req.getSession();

      usuario = (Usuario) session.getAttribute("usuario");

      if (usuario.getNivel().equals("2")) {
        zonaInt = Integer.parseInt(req.getParameter("zona"));
      } else {
        zonaInt = usuario.getZona().getNumero();
      }

      Integer zonaInteger = new Integer(zonaInt);

      String zona = new String(zonaInteger.toString());

      String anoJasper = (String) req.getSession().getAttribute("anoJasper");
      String ordenaTabela = (String) req.getSession().getAttribute("ordenacaoJasper");

      if (ordenaTabela == null) {
        ordenaTabela = "num_local_votacao ,data_cadastro";
      } else if (ordenaTabela.equals("2")) {
        ordenaTabela = " data_cadastro";
      } else if (ordenaTabela.equals("3")) {
        ordenaTabela = "  num_local_votacao";
      } else if (ordenaTabela.equals("4")) {
        ordenaTabela = " nome";
      } else {
        ordenaTabela = " num_local_votacao ,data_cadastro ";
      }

      Map<String, Object> param = new HashMap<String, Object>();

      param.put("ano", anoJasper);
      param.put("zona", zona);

      param.put("ordenacao", ordenaTabela);

      System.out.println("Tipo de Ordena��o: " + ordenaTabela);

      String pathJasper = getServletContext().getRealPath("/WEB-INF/relatorios");

      System.out.println("PATH JASPER: " + pathJasper);

      File file = new File(pathJasper);
      System.out.println("EXITE ARQUIVO: " + file.exists());

      param.put("SUBREPORT_DIR", pathJasper);

      try {
        JasperPrint impressao;

        impressao = JasperFillManager.fillReport(pathJasper + "/" + "consulta mesario.jasper", param, con);

        resp.addHeader("Content-Type", "application/octet-stream");
        resp.addHeader("Content-Disposition", "attachment; filename=" + nomeDoArquivo);
        resp.addHeader("Content-Transfer-Encoding", "binary");

        JasperExportManager.exportReportToPdfFile(impressao, path + nomeDoArquivo);

        File arquivo = new File(path + nomeDoArquivo);

        OutputStream arqOut = resp.getOutputStream();
        InputStream fileIn = new FileInputStream(arquivo);
        byte[] buffer = new byte[10 * 1024];

        int nread = 0;
        while ((nread = fileIn.read(buffer)) != -1) {
          arqOut.write(buffer, 0, nread);
        }
        fileIn.close();
        arqOut.close();
        arquivo.delete();

      } catch (JRException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    } catch (ClassNotFoundException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally {
      conexao.fecharConexao(con);
    }

  }
}
