package mesario.comunicacao.consultaMesarioVoluntario;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import mesario.comunicacao.Fachada;
import mesario.controle.mesario.Eleitor;
import mesario.controle.mesario.Zona;

import componente_acesso.controleusuario.usuario.Usuario;

/**
 * Servlet implementation class ServletConsultarMesarioVoluntario
 */
public class ServletConsultarMesarioVoluntario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Fachada fachada = null;

	public ServletConsultarMesarioVoluntario() {
		super(); 
	}

	public void init() throws ServletException {
		this.fachada = Fachada.getInstance();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		boolean chamadaAjax  = Boolean.parseBoolean(request.getParameter("chamadaAjax"));	

		if(chamadaAjax){
			
			JSONObject jsonObject = new JSONObject();

			// Lista que ir� salvar a lista de datas	
			List listaDatas = null;
			ArrayList<String> datas = null;
			datas = new ArrayList<String>();
			String mensagem = null;	

			int zona = Integer.parseInt(request.getParameter("zona"));

			try{

				listaDatas = this.fachada.consultarDatasInscricao(zona);

				// colocando apenas os anos das datas em uma nova lista
				for (Object object : listaDatas) {
					datas.add((String) object.toString().subSequence(0, 4));

				}		


				// removendo os repeditos da lista datas
				List data = new ArrayList(new HashSet(datas));

				//ordenendo datas
				Collections.sort(data);


				// setando o atributos data com a listas de datas
				jsonObject.put("datas", data );

				response.getWriter().write(jsonObject.toString());


			} catch (SQLException e) {
				e.printStackTrace();
				request.setAttribute("erro", e.getMessage());
				getServletConfig().getServletContext().getRequestDispatcher(
						"/erro.jsp?2").forward(request, response);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				request.setAttribute("erro", e.getMessage());
				getServletConfig().getServletContext().getRequestDispatcher(
						"/erro.jsp?3").forward(request, response);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			doPost(request, response);
		}
	}

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// lista de datas
		List listaDatas;
		ArrayList<String> datas = new ArrayList<String>();
		ArrayList<Zona> zonasList = new ArrayList<Zona>();
		
		int zona;
		Usuario usuario = new Usuario();

		String acessoADM = (String) request.getParameter("acessoADM");
		String mensagem = null;

		// recebendo o parametro do ano escolhido
		String ano = (String) request.getParameter("ano");
		String ordenaTabela = (String) request.getParameter("ordenaTabela");
		
		if(ordenaTabela == null){
			ordenaTabela = "1";
		}
		
//		if (ano.equals("0")) {
//			mensagem = "Por favor selecione algum ano de inscri��o v�lido.";
//			
//			request.setAttribute("mensagem", mensagem);
//			
//			getServletConfig().getServletContext().getRequestDispatcher(
//			"/ServletPreencherConsultaMesarioVoluntario.do").forward(request,
//			response);
//		}
//
//		else {

			try {

				HttpSession session = request.getSession();

				usuario = (Usuario) session.getAttribute("usuario");

				if(usuario.getNivel().equals("2"))
				{
					zona = Integer.parseInt(request.getParameter("zona"));
					zonasList = this.fachada.consultarZonasEleitorais();
					request.setAttribute("zonasList", zonasList);
				}
				
				else
					zona = usuario.getZona().getNumero();

				// carregando os eleitores do ano respectivo
				List listaEleitores = this.fachada.consultarEleitorAno(ano, zona,ordenaTabela);

				// Verifica se a lista esta vazia e manda uma mensagem de erro
				if (listaEleitores.isEmpty()) {
					System.out.println("LISTA ELEITOR � NULA");
					mensagem = "N�o foi encontrado nenhum mes�rio volunt�rio "
							+ "no ano de " + ano + " na " + zona
							+ "� Zona Eleitoral";
					request.setAttribute("mensagem", mensagem);
				}

				// carregando novamente as datas disponiveis
				listaDatas = this.fachada.consultarDatasInscricao(zona);
							
				//formatando a data 
				for (Object object : listaDatas) {
					datas.add((String) object.toString().subSequence(0, 4));
				}

				// removendo as datas repetidas
				List data = new ArrayList(new HashSet(datas));

				// ordenando datas
				Collections.sort(data);
				
				ArrayList<Eleitor> arrayListEleitor = new ArrayList<Eleitor>(listaEleitores);
				
				
				
				for (Eleitor eleitor : arrayListEleitor) {	
					
					String ocupacao = eleitor.getOcupacao();
					
					
					if(ocupacao != null){
						char[] arrayChar = new char[ocupacao.length()];
						System.out.println("Ocupacao antiga: "+ ocupacao);
						String COMMA_INSIDE = ",";
						do{	
							int posicao = ocupacao.indexOf(",");
							if(posicao != -1){
								arrayChar = ocupacao.toCharArray();
								arrayChar[posicao] = ' ';
								ocupacao = String.valueOf(arrayChar);
								eleitor.setOcupacao(String.valueOf(arrayChar));
								System.out.println("Ocupacao Nova: "+ ocupacao);
								System.out.println("-------------------------------");
							}
						}while(ocupacao.indexOf(",") != -1);
					}
				}
				
				

				// setando a lista de Eleitores
				request.setAttribute("listaEleitores", listaEleitores);

				// setando a lista de datas(anos) disponiveis
				request.setAttribute("datas", data);
				
				
				// setando a opcao do radio button
				request.setAttribute("ordenaTabela", ordenaTabela);
				
				//setando a data de consulta para o jasper
				request.getSession().setAttribute("anoJasper", ano);
				request.getSession().setAttribute("ordenacaoJasper", ordenaTabela);
				

				request.setAttribute("mostraTabela", true);
				
				// forward = "/consultarMesarioVoluntario.jsp"
				getServletConfig().getServletContext().getRequestDispatcher(
						"/consultarMesarioVoluntario.jsp").forward(request,
						response);

			} catch (SQLException e) {
				e.printStackTrace();
				request.setAttribute("erro", e.getMessage());
				getServletConfig().getServletContext().getRequestDispatcher(
						"/erro.jsp?2").forward(request, response);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				request.setAttribute("erro", e.getMessage());
				getServletConfig().getServletContext().getRequestDispatcher(
						"/erro.jsp?3").forward(request, response);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
//}