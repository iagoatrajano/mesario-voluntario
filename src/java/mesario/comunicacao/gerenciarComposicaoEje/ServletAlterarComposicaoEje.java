package mesario.comunicacao.gerenciarComposicaoEje;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mesario.comunicacao.Fachada;
import mesario.controle.composicaoEje.ComposicaoEje;
import mesario.util.FuncoesGerais;

/**
 * Servlet implementation class ServletAlterarComposicaoEje
 */
public class ServletAlterarComposicaoEje extends HttpServlet {

  private static final long serialVersionUID = 1L;

  Fachada fachada = null;

  public void init() throws ServletException {
    this.fachada = Fachada.getInstance();
  }

  public ServletAlterarComposicaoEje() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   * response)
   */
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    doPost(req, resp);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   * response)
   */
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    ComposicaoEje diretor = null;
    ComposicaoEje testemunha1 = null;
    ComposicaoEje testemunha2 = null;

    String mensagem = null;

    //setando os atributos do diretor
    diretor = new ComposicaoEje();

    diretor.setTratamento(req.getParameter("tratamento_diretor"));
    diretor.setNome(req.getParameter("nome"));

    diretor.setNacionalidade(req.getParameter("nacionalidade"));
    diretor.setEstadoCivil(req.getParameter("estado_civil"));

    diretor.setRg(req.getParameter("rg"));
    diretor.setOrgaoExp(req.getParameter("orgao_exp"));

    diretor.setCpf(req.getParameter("cpf"));
    diretor.setEndereco(req.getParameter("endereco"));

    String cep = req.getParameter("cep");

    diretor.setCep((FuncoesGerais.cepFormatarMascara(cep)));

    diretor.setBairro(req.getParameter("bairro"));
    diretor.setCidade(req.getParameter("cidade"));

    diretor.setCidade(req.getParameter("cidade"));
    diretor.setEstado(req.getParameter("estado"));

    diretor.setCargo("Diretor da Eje");
    diretor.setTipo("1");
    diretor.setCodObjeto(req.getParameter("codDiretor"));

    try {
      if (fachada.existeComposicaoEje("1")) {
        fachada.editarComposicaoEje(diretor);
      } else {
        fachada.inserirComposicaoEje(diretor);
      }
    } catch (ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    //setando atributos da testemunha 1
    testemunha1 = new ComposicaoEje();
    testemunha1.setNome(req.getParameter("nome_test"));
    testemunha1.setCargo(req.getParameter("cargo_test"));
    testemunha1.setCpf(req.getParameter("cpf_test"));
    testemunha1.setRg(req.getParameter("rg_test"));
    testemunha1.setOrgaoExp("SSP-PI");
    testemunha1.setCodObjeto(req.getParameter("codTest1"));
    testemunha1.setTipo("2");

    try {
      if (fachada.existeComposicaoEje("2")) {
        fachada.editarComposicaoEje(testemunha1);
      } else {
        fachada.inserirComposicaoEje(testemunha1);
      }
    } catch (ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    //setando atributos da testemunha 2
    testemunha2 = new ComposicaoEje();
    testemunha2.setNome(req.getParameter("nome_test2"));
    testemunha2.setCargo(req.getParameter("cargo_test2"));
    testemunha2.setCpf(req.getParameter("cpf_test2"));
    testemunha2.setRg(req.getParameter("rg_test2"));
    testemunha2.setOrgaoExp("SSP-PI");
    testemunha2.setCodObjeto(req.getParameter("codTest2"));
    testemunha2.setTipo("2");

    try {
      if (fachada.existeComposicaoEje("2")) {
        fachada.editarComposicaoEje(testemunha2);
      } else {
        fachada.inserirComposicaoEje(testemunha2);
      }
    } catch (ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    mensagem = "Os dados foram alterados com sucesso.";

    req.setAttribute("mensagem", mensagem);

    getServletConfig().getServletContext().getRequestDispatcher("/ServletPreencherComposicaoEje.do").forward(req, resp);

  }

}
