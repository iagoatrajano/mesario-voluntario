package mesario.comunicacao.analisarConvenio;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mesario.comunicacao.Fachada;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;

/**
 * Servlet implementation class ServletValidarConvenio
 */
public class ServletValidarConvenio extends HttpServlet {

    private static final long serialVersionUID = 1L;

    Fachada fachada = null;

    public ServletValidarConvenio() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init() throws ServletException {
        this.fachada = Fachada.getInstance();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doPost(req, res);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String mensagem = "";
        String codObj = req.getParameter("codObjeto");

        //
        if ((req.getParameter("dataInicialConvenio") == null) || (req.getParameter("dataInicialConvenio").isEmpty())) {
            mensagem = "A data inicial do conv�nio deve ser informada.";
        }
        if ((req.getParameter("dataFinalConvenio") == null) || (req.getParameter("dataFinalConvenio").isEmpty())) {
            if (!mensagem.isEmpty()) {
                mensagem = "A data inicial e final do conv�nio devem ser informadas.";
            } else {
                mensagem = "A data final do conv�nio deve ser informada.";
            }
        }
        if (!mensagem.isEmpty()) {
            req.setAttribute("mensagem", mensagem);
            String endereco = "/ServletPreencherValidarConvenio.do?universidadeSelecionada=" + codObj;
            getServletConfig().getServletContext().getRequestDispatcher(endereco).forward(req, res);
            return;
        }

        UniversidadeConveniada universidade = new UniversidadeConveniada();
        String[] buffer = null;
        String numero = req.getParameter("numeroDoConvenio");
        String anoDoConvenio = req.getParameter("anoDoConvenio");

        int codObjeto = Integer.parseInt(codObj);

        try {

            //Verifica se o usuario digitou o Numero do Convenio.
            if (!numero.equals("")) {
                String numeroAnoConvenio = numero + anoDoConvenio;
                boolean existe = fachada.existeNumero((numeroAnoConvenio), codObjeto);
                UniversidadeConveniada univers = fachada.consultarUniversidade(codObjeto);

                //Verifica se o Numero de Convenio ja existe para esse ano
                if (!existe) {
                    univers.setNumeroDoConvenio(Integer.parseInt(numero));
                    univers.setAnoDoConvenio(Integer.parseInt(anoDoConvenio));
                    fachada.editarUniversidade(univers);
                } else {
                    req.setAttribute("mensagem", "O n�mero de conv�nio j� existe nesse ano.");
                    req.setAttribute("universidade", univers);

                    getServletConfig().getServletContext().getRequestDispatcher("/validarConvenio.jsp").forward(req, res);
                }

            }

            if (!"ativa".equals(req.getParameter("flag"))) {

                universidade = fachada.consultarUniversidade(codObjeto);

                if ("V�lido".equals(req.getParameter("convenioValido"))) {
                    universidade.setConvenioValido("1");
                } else {
                    universidade.setConvenioValido("0");
                }

                String dataInicial = req.getParameter("dataInicialConvenio");
                buffer = dataInicial.split("/");
                Date dataInicialDefinitivo = new Date(Integer.parseInt(buffer[2]) - 1900, Integer.parseInt(buffer[1]) - 1, Integer.parseInt(buffer[0]));
                universidade.setDataInicialConvenio(dataInicialDefinitivo);

                String dataFinal = req.getParameter("dataFinalConvenio");
                buffer = dataFinal.split("/");
                Date dataFinalDefinitivo = new Date(Integer.parseInt(buffer[2]) - 1900, Integer.parseInt(buffer[1]) - 1, Integer.parseInt(buffer[0]));
                universidade.setDataFinalConvenio(dataFinalDefinitivo);

                if (dataFinalDefinitivo.before(dataInicialDefinitivo)) {
                    req.setAttribute("mensagem", "A data final deve ser posterior � data inicial do conv�nio!");
                    req.setAttribute("universidade", universidade);

                    req.setAttribute("disabled", "");

                    getServletConfig().getServletContext().getRequestDispatcher("/validarConvenio.jsp").forward(req, res);
                } else {

                    fachada.editarUniversidade(universidade);
                    req.setAttribute("mensagem", "Conv�nio alterado com sucesso!");
                    getServletConfig().getServletContext().getRequestDispatcher("/ServletPreencherListarConvenios.do").forward(req, res);
                }

            } else {
                //atualiza apenas a data final
                UniversidadeConveniada uni = fachada.consultarUniversidade(Integer.parseInt(req.getParameter("codObjeto")));
                String dataFinal = req.getParameter("dataFinalConvenio");
                buffer = dataFinal.split("/");
                Date dataFinalDefinitivo = new Date(Integer.parseInt(buffer[2]) - 1900, Integer.parseInt(buffer[1]) - 1, Integer.parseInt(buffer[0]));

                if (dataFinalDefinitivo.before(uni.getDataInicialConvenio())) {
                    req.setAttribute("mensagem", "A data final deve ser posterior � data inicial do conv�nio!");
                    req.setAttribute("universidade", uni);

                    req.setAttribute("disabled", "disabled=\"disabled\"");
                    req.setAttribute("flag", "ativa");

                    getServletConfig().getServletContext().getRequestDispatcher("/validarConvenio.jsp").forward(req, res);
                } else {
                    uni.setDataFinalConvenio(dataFinalDefinitivo);
                    fachada.editarDataFinalUniversidade(Integer.parseInt(req.getParameter("codObjeto")), dataFinalDefinitivo);
                    req.setAttribute("mensagem", "Conv�nio alterado com sucesso!");
                    getServletConfig().getServletContext().getRequestDispatcher("/ServletPreencherListarConvenios.do").forward(req, res);
                }
            }

        } catch (Exception e) {
            req.setAttribute("mensagem", "");
            e.printStackTrace();
        }
    }

}
