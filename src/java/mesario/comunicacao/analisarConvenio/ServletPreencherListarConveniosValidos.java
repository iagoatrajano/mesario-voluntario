package mesario.comunicacao.analisarConvenio;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mesario.comunicacao.Fachada;


/**
 * Servlet implementation class ServletPreencherListarConveniosValidos
 */
public class ServletPreencherListarConveniosValidos extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	Fachada fachada = null;

	
    public ServletPreencherListarConveniosValidos() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init() throws ServletException {
		this.fachada = Fachada.getInstance();
	}
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List listaUniversidades;
		
		try{
			listaUniversidades = fachada.listasUniversidadesConveniadas();
			
			request.setAttribute("listaUniversidades", listaUniversidades);
			request.setAttribute("mensagem", request.getAttribute("mensagem"));
		}catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("erro", e.getMessage());
				getServletConfig().getServletContext().getRequestDispatcher(
						"/erro.jsp?2").forward(request, response);
		} 
		getServletConfig().getServletContext().getRequestDispatcher("/listarConveniosValidos.jsp").forward(request, response);
	}

}
