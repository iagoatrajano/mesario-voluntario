package mesario.comunicacao.analisarConvenio;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mesario.comunicacao.Fachada;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.util.FuncoesGerais;

/**
 * Servlet implementation class ServletEditarConvenio
 */
public class ServletEditarConvenio extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	Fachada fachada = null;
	
    public ServletEditarConvenio() {
        super();
        
    }
    public void init() throws ServletException {
		this.fachada = Fachada.getInstance();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		UniversidadeConveniada universidade = new UniversidadeConveniada();

		String mensagem = null;
		String [] buffer = null;
		
		try{
			if(!"ativa".equals(req.getParameter("flag"))){
					
				
				universidade.setCodObjeto(req.getParameter("codObjeto"));
				
				universidade.setRazaoSocial(req.getParameter("razaoSocial"));
				universidade.setNomeFantasia(req.getParameter("nomeFantasia"));
				universidade.setSigla(req.getParameter("sigla"));
				universidade.setCepRepresentante(FuncoesGerais
						.cepFormatarMascara(req.getParameter("cep")));
				universidade.setTipoPessoaJuridica(Integer.parseInt(req
						.getParameter("tipoPessoa")));
				universidade.setTituloRepresentante(req
						.getParameter("cargo"));
				universidade.setNomeRepresentante(req.getParameter("nomeRep"));
				universidade.setEnderecoRepresentante(req
						.getParameter("endereco"));
				universidade.setCidadeRepresentante(req.getParameter("cidade"));
				universidade.setEstadoRepresentante(req.getParameter("estado"));
				universidade.setNacionalidadeRepresentante(req
						.getParameter("nacionalidade"));
				universidade.setEstadoCivilRepresentante(req
						.getParameter("estadoCivil"));
				universidade.setRgRepresentante(req.getParameter("rg"));
				
				universidade.setOrgaoExpRgRepresentante(req
						.getParameter("orgaoExp"));
				universidade.setCpfRepresentante(req.getParameter("cpf"));
				universidade.setBairroRepresentante(req.getParameter("bairro"));
				universidade.setTituloRepresentante(req.getParameter("cargo"));
				universidade.setCnpj(req.getParameter("cnpj"));
				if("V�lido".equals(req.getParameter("convenioValido"))){
					universidade.setConvenioValido("1");
				}else{
					universidade.setConvenioValido("0");
				}
				universidade.setEmail(req.getParameter("emailConvenio"));
				universidade.setTelefone(req.getParameter("phone"));
				
				int codObjeto = Integer.parseInt(req.getParameter("codObjeto"));	
				UniversidadeConveniada uni = fachada.consultarUniversidade(codObjeto);
		//		String dataInicial = req.getParameter("dataInicialConvenio");			
			//	buffer = dataInicial.split("/");			
			//	Date dataInicialDefinitivo = new Date(Integer.parseInt(buffer[2]) - 1900, Integer.parseInt(buffer[1]) - 1, Integer.parseInt(buffer[0]));
				universidade.setDataInicialConvenio(uni.getDataInicialConvenio());
				
			//	String dataFinal = req.getParameter("dataFinalConvenio");			
			//	buffer = dataFinal.split("/");			
			//	Date dataFinalDefinitivo = new Date(Integer.parseInt(buffer[2]) - 1900, Integer.parseInt(buffer[1]) - 1, Integer.parseInt(buffer[0]));
				universidade.setDataFinalConvenio(uni.getDataFinalConvenio());
				
				if (uni.getDataFinalConvenio().before(uni.getDataInicialConvenio())){
					req.setAttribute("mensagem", "A data final deve ser posterior � data inicial do conv�nio!");
					req.setAttribute("universidade", universidade);

						req.setAttribute("disabled", "");
					
					getServletConfig().getServletContext().getRequestDispatcher("/editarConvenio.jsp").forward(req, res);
				}else{
					
					fachada.editarUniversidade(universidade);
					req.setAttribute("mensagem", "Conv�nio alterado com sucesso!");
					getServletConfig().getServletContext().getRequestDispatcher("/ServletPreencherListarConvenios.do").forward(req, res);
				}
				
			}else{
				//atualiza apenas a data final
				UniversidadeConveniada uni = fachada.consultarUniversidade(Integer.parseInt(req.getParameter("codObjeto")));
//				String dataFinal = req.getParameter("dataFinalConvenio");			
//				buffer = dataFinal.split("/");			
//				Date dataFinalDefinitivo = new Date(Integer.parseInt(buffer[2]) - 1900, Integer.parseInt(buffer[1]) - 1, Integer.parseInt(buffer[0]));
//				
//				if (dataFinalDefinitivo.before(uni.getDataInicialConvenio())){
//					req.setAttribute("mensagem", "A data final deve ser posterior � data inicial do conv�nio!");
//					req.setAttribute("universidade", uni);
//					
//						req.setAttribute("disabled", "disabled=\"disabled\"");
//						req.setAttribute("flag", "ativa");
//					
//
//					getServletConfig().getServletContext().getRequestDispatcher("/editarConvenio.jsp").forward(req, res);
//				}else{
//					uni.setDataFinalConvenio(dataFinalDefinitivo);
//					fachada.editarDataFinalUniversidade(Integer.parseInt(req.getParameter("codObjeto")), dataFinalDefinitivo);
//					req.setAttribute("mensagem", "Conv�nio alterado com sucesso!");
//					getServletConfig().getServletContext().getRequestDispatcher("/ServletPreencherListarConvenios.do").forward(req, res);
//				}
				req.setAttribute("mensagem", "Conv�nio alterado com sucesso!");
				getServletConfig().getServletContext().getRequestDispatcher("/ServletPreencherListarConvenios.do").forward(req, res);
			}
			
			
			
		} catch(Exception e){
			e.printStackTrace();
		}
		
		
	}

}
