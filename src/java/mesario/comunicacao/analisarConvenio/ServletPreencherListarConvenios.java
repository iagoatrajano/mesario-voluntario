package mesario.comunicacao.analisarConvenio;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mesario.comunicacao.Fachada;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;

public class ServletPreencherListarConvenios extends HttpServlet {

  private static final long serialVersionUID = 1L;

  Fachada fachada = null;

  public ServletPreencherListarConvenios() {
    super();

  }

  public void init() throws ServletException {
    this.fachada = Fachada.getInstance();
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doPost(request, response);
  }

  protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
    ArrayList<UniversidadeConveniada> listaUniversidades;
    //
    try {
      listaUniversidades = fachada.listarTodasUniversidades();
      //
      req.setAttribute("listaUniversidades", listaUniversidades);
      req.setAttribute("mensagem", req.getAttribute("mensagem"));
    } catch (Exception e) {
      e.printStackTrace();
      req.setAttribute("erro", e.getMessage());
      getServletConfig().getServletContext().getRequestDispatcher(
              "/erro.jsp?2").forward(req, res);
    }
    getServletConfig().getServletContext().getRequestDispatcher("/listarConvenios.jsp").forward(req, res);
  }

}
