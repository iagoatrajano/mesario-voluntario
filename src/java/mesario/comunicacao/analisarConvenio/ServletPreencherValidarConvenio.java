package mesario.comunicacao.analisarConvenio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mesario.comunicacao.Fachada;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.util.Util;

/**
 * Servlet implementation class PreencherValidarConvenio
 */
public class ServletPreencherValidarConvenio extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	Fachada fachada = null;
	
    public ServletPreencherValidarConvenio() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	}
	public void init() throws ServletException {
		this.fachada = Fachada.getInstance();
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		UniversidadeConveniada universidade = null;
		Util util = new Util();
		try{ 
			universidade = fachada.consultarUniversidade(Integer.parseInt(req.getParameter("universidadeSelecionada")));
			req.setAttribute("estados", util.listarEstados());
			
			if(universidade.getConvenioValido().equals("V�lido")){
				req.setAttribute("disabled", "disabled=\"disabled\"");
				req.setAttribute("flag", "ativa");
			}else{
				req.setAttribute("disabled", "");
			}
			
			
			req.setAttribute("universidade", universidade);
			
			
		}catch (Exception e) {
				e.printStackTrace();
				req.setAttribute("erro", e.getMessage());
				getServletConfig().getServletContext().getRequestDispatcher(
						"/erro.jsp?2").forward(req, res);
		} 
		getServletConfig().getServletContext().getRequestDispatcher("/validarConvenio.jsp").forward(req, res);
	}

}
