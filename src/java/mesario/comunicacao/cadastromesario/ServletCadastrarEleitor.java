package mesario.comunicacao.cadastromesario;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mesario.comunicacao.Fachada;
import mesario.controle.mesario.Eleitor;
import mesario.excecoes.RepositorioException;
import mesario.util.BaseServlet;
import mesario.util.EnvioEmail;

public class ServletCadastrarEleitor extends BaseServlet {

    private static final String cabecalhoEmailHTML
            = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"> "
            + "<HTML> "
            + "	<HEAD> "
            + "		<TITLE></TITLE> "
            + "		<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'> "
            + "		<style type='text/css'> "
            + "			body "
            + "			{ "
            + "				font: 9pt Verdana; "
            + "			} "
            + "			.titulo "
            + "			{ "
            + "				background-color: #E2DEDE; "
            + "			} "
            + "		</style> "
            + "	</HEAD> "
            + "	<BODY> "
            + "	<table width='100%'> "
            + "		<tr> "
            + "			<td> "
            + "				<b>Tribunal Regional Eleitoral do Piau�</b> "
            + "			</td> "
            + "		</tr> "
            + "		<tr> "
            + "			<td> "
            + "				<br> "
            + "				<br> "
            + "				<font size='1'>Este � um e-mail autom�tico. Por favor n�o o responda.</font> "
            + "			</td> "
            + "		</tr> "
            + "		<tr> "
            + "			<td> "
            + "				<hr> "
            + "			</td> "
            + "		</tr> "
            + "	</table> ";

    private static final String cabecalhoEmail
            = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"> "
            + "<HTML> "
            + "	<HEAD> "
            + "		<TITLE></TITLE> "
            + "		<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'> "
            + "		<style type='text/css'> "
            + "			body "
            + "			{ "
            + "				font: 7pt Verdana; "
            + "			} "
            + "			.titulo "
            + "			{ "
            + "				background-color: #E2DEDE; "
            + "			} "
            + "		</style> "
            + "	</HEAD> "
            + "	<BODY> "
            + "	<table width='100%'> "
            + "		<tr> "
            + "			<td> "
            + "				<b>Tribunal Regional Eleitoral do Piau�</b> "
            + "			</td> "
            + "		</tr> "
            + "		<tr> "
            + "			<td> "
            + "				<br> "
            + "				<br> "
            + "				<font size='1'>Este � um e-mail autom�tico. Por favor n�o o responda.</font> "
            + "			</td> "
            + "		</tr> "
            + "		<tr> "
            + "			<td> "
            + "				<hr> "
            + "			</td> "
            + "		</tr> "
            + "	</table> ";

    Fachada fachada = null;

    public void init() throws ServletException {
        this.fachada = Fachada.getInstance();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        doPost(req, res);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        String mensagem = null;
        // A vari�vel voluntarioCadastrado servir� para em caso de o eleitor
        //  ser cadastrado corretamente � mostrado a mensagem de confirma��o
        //  e depois retornado a p�gina inicial.
        String voluntarioCadastrado = null;
        String codObjetoUniversidade = req.getParameter("listaUniversidade");
        HttpSession session = req.getSession();

        Eleitor eleitor = (Eleitor) session.getAttribute("ELEITOR");

        try {
            if (eleitor != null
                    && !((String) req.getParameter("endereco")).trim().equals("")
                    && !((String) req.getParameter("bairro")).trim().equals("")
                    && !((String) req.getParameter("cidade")).trim().equals("")) {

                // Trocando aspas simples e aspas duplas para caracteres ascii
                String endereco = ((String) req.getParameter("endereco")).trim();

                String numero = ((String) req.getParameter("numero")).trim();

                String complemento = ((String) req.getParameter("complemento")).trim();

                if (numero != "") {
                    endereco = endereco + ", " + numero;
                }

                if (complemento != "") {
                    endereco = endereco + " - " + complemento;
                }

                eleitor.setEndereco(endereco);
                eleitor.setBairro(((String) req.getParameter("bairro")).trim());
                eleitor.setCidade(((String) req.getParameter("cidade")).trim());

                //tratamento da variavel telefone e do seu respectivo ddd
                String telefonePrincipal = req.getParameter("telefone_principal");
                if (!telefonePrincipal.equals("")) {
                    //inserindo valores
                    eleitor.setTelefone(telefonePrincipal);
                    //eleitor.setTelefone(FuncoesGerais.formatarTelefoneMascara(telefonePrincipal));
                    //eleitor.setDddFone(FuncoesGerais.dddTelefoneMascara(telefonePrincipal));
                }

                String valor;

                if ((valor = (String) req.getParameter("cep")) != null
                        && !valor.trim().equals("")) {
                    //Essa valida��o servir� para caso o cep seja digitado faltando algum caracter.
                    String cep = valor.trim().replace(".", "").replace("-", "");
                    if (cep.length() < 8) {
                        //Completa o cep com 0 (zero)
                        for (int i = cep.length(); i < 8; i++) {
                            cep += "0";
                        }
                    } else {
                        //Quebra o cep caso ele tenha mais de 8 digitos (00000000), sem o ponto e o tra�o.
                        // Ou caso ele esteja certo
                        cep = cep.substring(0, 8);
                    }
                    eleitor.setCep(Integer.parseInt(cep));
                }

                if ((valor = (String) req.getParameter("telefone_opcional")) != null
                        && !valor.trim().equals("")) {

                    //tratar a marcara do Telefone Opcional
                    String telefoneOpcional = (String) req.getParameter("telefone_opcional");

                    //inserir valores formatados
                    eleitor.setFoneOpcional(telefoneOpcional);
                    //eleitor.setFoneOpcional(FuncoesGerais.formatarTelefoneMascara(telefoneOpcional));
                    //eleitor.setDddFoneOpcional(FuncoesGerais.dddTelefoneMascara(telefoneOpcional));
                } else {
                    eleitor.setFoneOpcional("");
                }

                if ((valor = (String) req.getParameter("numOcupacao")) != null
                        && !valor.trim().equals("")
                        && !valor.trim().equals("000")) {
                    eleitor.setNumOcupacao(Integer.parseInt(valor.trim()));
                    eleitor.setOcupacao(fachada.consultarOcupacao(eleitor.getNumOcupacao()));
                }

                if ((valor = (String) req.getParameter("escolaridade")) != null
                        && !valor.trim().equals("")) {
                    eleitor.setGrauInstrucao(Integer.parseInt(valor.trim()));
                } else {
                    // 0 - N�o informado // Especificado na documenta��o
                    eleitor.setGrauInstrucao(0);
                }

                if ((valor = (String) req.getParameter("email")) != null && !valor.trim().equals("")) {
                    eleitor.setEmail(valor);
                }

                if ((valor = (String) req.getParameter("trabalhoMesario")) != null && !valor.trim().equals("")) {
                    eleitor.setFoiMesario(Integer.parseInt(valor.trim()));
                } else {
                    // 0 - N�o informado // Especificado na documenta��o
                    eleitor.setFoiMesario(0);
                }

                if ((valor = (String) req.getParameter("servidorPublico")) != null
                        && !valor.trim().equals("")) {
                    eleitor.setServidorPublico(Integer.parseInt(valor.trim()));
                } else {
                    // 0 - N�o informado // Especificado na documenta��o
                    eleitor.setServidorPublico(0);
                }

                //sem universidade
                if (codObjetoUniversidade == null) {
                    eleitor.setCodObjetoUniversidadeConveniada(codObjetoUniversidade);
                    eleitor.setUniversitario(null);
                } //OUTRA UNIVERSIDADE
                else if (codObjetoUniversidade.equals("0")) {
                    eleitor.setUniversitario("1");
                    eleitor.setCodObjetoUniversidadeConveniada(null);
                } //COM UNIVERSIDADE
                else {
                    eleitor.setUniversitario("1");
                    eleitor.setCodObjetoUniversidadeConveniada(codObjetoUniversidade);
                }

                eleitor.setDataCadastro(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

                //Recebe a idade atual da pessoa que foi pesquisado atrav�s de SQL
                int idade = this.fachada.cadastraEleitor(eleitor);

                if ((valor = (String) req.getParameter("email")) != null && !valor.trim().equals("")) {
                    EnviarEmailHTML(valor);
                }

                // CRIA��O E ENVIO DE EMAIL PARA ZONA ELEITORAL
                String texto = "";
                texto += " Senhor(a) Chefe da " + eleitor.getNumZona() + "� Zona Eleitoral, <br><br>";
                texto += " Nesta data, o eleitor abaixo discriminado efetuou sua inscri��o como mes�rio volunt�rio: <br>";

                if (idade < 18) {
                    texto += " ATEN��O: O eleitor � menor de 18 anos. <br><br>";

                    mensagem = "Infelizmente, no momento, como voc� ainda n�o completou 18 anos, "
                            + "n�o poder� participar como mes�rio. No entanto, seu cadastro foi "
                            + "efetuado para que na pr�xima elei��o voc� possa participar deste "
                            + "ato de cidadania.";
                }

                texto += "<br>";
                texto += " <b>DADOS EXTRA�DOS DO CADASTRO DE ELEITOR:</b> <br><br>";
                texto += " <b>T�tulo:</b> " + eleitor.getTitulo() + "<br>";
                texto += " <b>Nome:</b> " + eleitor.getNome() + "<br>";
                texto += " <b>Data de Nascimento:</b> " + new SimpleDateFormat("dd/MM/yyyy").format(eleitor.getDataNascimento()) + "<br>";
                texto += " <b>Nome da M�e:</b> " + eleitor.getNomeMae() + "<br>";
                texto += " <b>Zona Eleitoral:</b> " + Integer.parseInt(eleitor.getNumZona()) + "�<br>";
                texto += " <b>Se��o Eleitoral:</b> " + eleitor.getSecaoEleitoral() + "<br>";
                texto += " <b>Munic�pio Eleitoral:</b> " + eleitor.getMunicipioZona() + "<br>";
                texto += " <b>Local de Vota��o:</b> " + eleitor.getNumLocalVotacao() + " - " + eleitor.getLocalVotacao() + "<br>";

                texto += "<br>";
                texto += " <b>DADOS FORNECIDOS PELO ELEITOR:</b> <br><br>";
                texto += " <b>Data da inscri��o do mes�rio volunt�rio:</b> " + eleitor.getDataCadastro() + "<br>";
                texto += " <b>Endere�o:</b> " + eleitor.getEndereco() + "<br>";
                texto += " <b>Bairro:</b> " + eleitor.getBairro() + "<br>";
                texto += " <b>Cidade:</b> " + eleitor.getCidade() + "<br>";

                if (eleitor.getCep() != null) {
                    String cep = eleitor.getCep().toString();
                    texto += " <b>CEP:</b> "
                            + cep.substring(0, 2) + "." + cep.substring(2, 5) + "-"
                            + cep.substring(5, 8) + "<br>";
                } else {
                    texto += " <b>CEP:</b> - <br>";
                }

                if (eleitor.getDddFone() != null) {
                    texto += " <b>Telefone:</b> (" + eleitor.getDddFone() + ") " + eleitor.getTelefone() + "<br>";
                } else if (eleitor.getTelefone() != null) {
                    texto += " <b>Telefone:</b> " + eleitor.getTelefone() + "<br>";
                }

                if (eleitor.getDddFoneOpcional() != null) {
                    texto += " <b>Telefone Opcional:</b> (" + eleitor.getDddFoneOpcional() + ") " + eleitor.getFoneOpcional() + "<br>";
                } else if (!eleitor.getFoneOpcional().equals("")) {
                    texto += " <b>Telefone Opcional:</b> " + eleitor.getFoneOpcional() + "<br>";
                }

                if (eleitor.getEmail() != null) {
                    texto += " <b>Email:</b> " + eleitor.getEmail() + "<br>";
                } else {
                    texto += " <b>Email:</b> - <br>";
                }

                texto += " <b>Escolaridade:</b> ";
                if (eleitor.getGrauInstrucao() == 0) {
                    texto += "-";
                } else if (eleitor.getGrauInstrucao() == 1) {
                    texto += "Analfabeto";
                } else if (eleitor.getGrauInstrucao() == 2) {
                    texto += "L� e Escreve";
                } else if (eleitor.getGrauInstrucao() == 3) {
                    texto += "Primeiro grau Incompleto";
                } else if (eleitor.getGrauInstrucao() == 4) {
                    texto += "Primeiro Grau Completo";
                } else if (eleitor.getGrauInstrucao() == 5) {
                    texto += "Segundo Grau Incompleto";
                } else if (eleitor.getGrauInstrucao() == 6) {
                    texto += "Segundo Grau Completo";
                } else if (eleitor.getGrauInstrucao() == 7) {
                    texto += "Superior Incompleto";
                } else if (eleitor.getGrauInstrucao() == 8) {
                    texto += "Superior Completo";
                }
                texto += "<br>";

                if (eleitor.getOcupacao() != null) {
                    texto += " <b>Ocupa��o:</b> " + eleitor.getOcupacao();
                    texto += "<br>";
                }

                texto += " <b>� servidor p�blico:</b> ";
                if (eleitor.getServidorPublico() == 0) {
                    texto += " - ";
                } else if (eleitor.getServidorPublico() == 1) {
                    texto += "SIM";
                } else if (eleitor.getServidorPublico() == 2) {
                    texto += "N�O";
                }

                texto += "<br>";

                texto += " <b>J� foi mes�rio:</b> ";
                if (eleitor.getFoiMesario() == 0) {
                    texto += " - ";
                } else if (eleitor.getFoiMesario() == 1) {
                    texto += "SIM";
                } else if (eleitor.getFoiMesario() == 2) {
                    texto += "N�O";
                }

                texto += "<br>";

                texto += " <b>� universit�rio:</b> ";
                if ((eleitor.getUniversitario() != null) && (!eleitor.getUniversitario().equals(""))) {
                    texto += "SIM";
                } else {
                    texto += "N�O";
                }

                texto += "<br><br>";

                if (eleitor.getCodObjetoUniversidadeConveniada() != null && !eleitor.getCodObjetoUniversidadeConveniada().equals("")) {
                    texto += "Lembramos que o Tribunal Regional Eleitoral do Piau� assinou conv�nio com a universidade do mes�rio volunt�rio em tela, comprometendo-se, na Cl�usula Terceira - Das Obriga��es dos Convenentes, a:";
                    texto += "<br><br>";
                    texto += "- Nomear os mes�rios, convocando-os e capacitando-os para constituirem as Mesas Receptoras de Voto no dia, hora e locais de vota��o designados.<br>";
                    texto += "- Expedir certid�o ao universit�rio, atestando seu comparecimento e total de horas trabalhadas.";
                }

                EnviarEmail(eleitor.getNumZona(), texto);

                // Calcular a idade do candidato
                if (voluntarioCadastrado == null) {
                    voluntarioCadastrado = "Cadastro realizado com sucesso! "
                            + "A Justi�a Eleitoral agradece o seu interesse em ser Mes�rio, "
                            + "um trabalho de grande relev�ncia para o processo democr�tico.";
                }

                req.setAttribute("voluntarioCadastrado", voluntarioCadastrado);
                req.setAttribute("ELEITOR", eleitor);

                this.forward(req, res, "/cadastroMesario.jsp");

            } else {
                mensagem = "Sua sess�o Expirou  ";

                req.setAttribute("mensagem", mensagem);

                this.forward(req, res, "/informaTitulo.jsp");

            }

        } catch (RepositorioException e) {
            e.printStackTrace();
            req.setAttribute("erro", e.getMessage());
            this.forward(req, res, "/erro.jsp?1");
        } catch (SQLException e) {
            e.printStackTrace();
            req.setAttribute("erro", e.getMessage());
            this.forward(req, res, "/erro.jsp?2");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            req.setAttribute("erro", e.getMessage());
            this.forward(req, res, "/erro.jsp?3");
        }
    }

    public static void EnviarEmailHTML(String emailDestinatario) {

        String emailRemetente = "nao_responda@tre-pi.jus.br";

        String assunto = "Cadastro de Mes�rio Volunt�rio";

        String conteudoEmail = cabecalhoEmailHTML
                + "<table width='100%'> "
                + "	<tr> "
                + "		<td style='text-align: justify;'> "
                + "			<br/><br/> "
                + "			Prezado(a) Senhor(a), "
                + "			<br/><br/> "
                + "			Obrigado pelo seu cadastro! "
                + "           <br/><br/> "
                + "           Enviaremos seus dados para a Zona Eleitoral � qual o(a) senhor(a) est� vinculado(a) para an�lise. Se V. Sa. for convocado, entraremos em contato."
                + "           <br/><br/> "
                + "		</td> "
                + "	</tr> "
                + "</table> "
                + "</BODY></HTML>";

        EnvioEmail.enviarEmailHTML(emailDestinatario, emailRemetente, assunto, conteudoEmail);

    }

    public static void EnviarEmail(String zonaEleitoral, String conteudo) {

        String emailRemetente = "nao_responda@tre-pi.jus.br";

        String assunto = "Cadastro de Mes�rio Volunt�rio";

        String emailDestinatario = "zon" + zonaEleitoral + "@tre-pi.jus.br";

        String conteudoEmail = cabecalhoEmail
                + "<table width='100%'> "
                + "	<tr> "
                + "		<td style='text-align: justify; font-size: 10pt; font: arial;'> "
                + "			<br/><br/> "
                + conteudo
                + "		</td> "
                + "	</tr> "
                + "</table> "
                + "</BODY>"
                + "</HTML>";

        EnvioEmail.enviarEmail(emailDestinatario, emailRemetente, assunto, conteudoEmail);

    }

}
