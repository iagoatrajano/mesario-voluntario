package mesario.comunicacao.cadastromesario;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mesario.comunicacao.Fachada;
import mesario.controle.mesario.Eleitor;
import mesario.excecoes.RepositorioException;
import mesario.util.BaseServlet;

public class ServletConsultarDadosEleitor extends BaseServlet {

    Fachada fachada = null;

    public void init() throws ServletException {
        this.fachada = Fachada.getInstance();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        doPost(req, res);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        String titulo = req.getParameter("titulo");

        int tamanho = titulo.length();
        if (tamanho < 12) {
            while (tamanho != 12) {
                titulo = "0" + titulo;
                tamanho++;
            }
        }

        Eleitor eleitor = null;
        List listaUniversidade = null;
        List listaOcupacao = null;
        String mensagem = null;

        try {
            eleitor = this.fachada.consultarEleitor(titulo);
            listaUniversidade = this.fachada.listarUniversidadeConveniada();

            if (!listaUniversidade.isEmpty()) {

                req.setAttribute("listaUniversidade", listaUniversidade);
            }

            if (eleitor != null) {
                if (!fachada.consultarEleitorCadastrado(titulo, eleitor.getNumZona())) {
                    listaOcupacao = fachada.consultarOcupacao();

                    req.setAttribute("LISTAOCUPACAO", listaOcupacao);
                    req.setAttribute("ELEITOR", eleitor);

                    req.setAttribute("mensagem", mensagem);

                    this.forward(req, res, "/cadastroMesario.jsp");
                } else {
                    mensagem = "Prezado eleitor, constatamos que o seu cadastro j� foi efetivado este ano."
                            + "Caso deseje alterar os seus dados, entre em contato com o seu Cart�rio Eleitoral.";

                    req.setAttribute("mensagem", mensagem);

                    this.forward(req, res, "/informaTitulo.jsp");
                }
            } else {
                mensagem = "O t�tulo informado n�o existe no Cadastro Eleitoral do Piau�.  "
                        + "Por favor, entre em contato com o seu Cart�rio Eleitoral.";

                req.setAttribute("mensagem", mensagem);

                this.forward(req, res, "/informaTitulo.jsp");
            }

        } catch (RepositorioException e) {
            e.printStackTrace();
            req.setAttribute("erro", e.getMessage());
            this.forward(req, res, "/erro.jsp?1");
        } catch (SQLException e) {
            e.printStackTrace();
            req.setAttribute("erro", e.getMessage());
            this.forward(req, res, "/erro.jsp?2");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            req.setAttribute("erro", e.getMessage());
            this.forward(req, res, "/erro.jsp?3");
        } catch (ParseException e) {
            e.printStackTrace();
            req.setAttribute("erro", e.getMessage());
            this.forward(req, res, "/erro.jsp?4");
        }
    }

}
