package mesario.comunicacao;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import mesario.controle.ControleCadastrarComposicaoEje;
import mesario.controle.ControleCadastrarEleitor;
import mesario.controle.ControleCadastrarUniversidadeConveniada;
import mesario.controle.ControleCadastrarUsuario;
import mesario.controle.composicaoEje.ComposicaoEje;
import mesario.controle.mesario.Eleitor;
import mesario.controle.mesario.Zona;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.excecoes.RepositorioException;
import tre.pi.sedesc.mesario.modelo.Usuario;

public class Fachada {

  /**
   * Refer�ncia est�tica do Singleton. Guarda o endere�o do objeto que
   * representa a �nica inst�ncia desta classe em um programa JAVA.
   */
  private static Fachada instancia;

  private ControleCadastrarEleitor controleCadastrarEleitor;
  private ControleCadastrarUniversidadeConveniada controleCadastrarUniversidadeConveniada;
  private ControleCadastrarComposicaoEje controleCadastrarComposicaoEje;
  private ControleCadastrarUsuario controleCadastrarUsuario;

  /**
   * Construtor privado da classe. Ele � assim definido para que o padr�o de
   * implementa��o do Singleton possa garantir que uma �nica inst�ncia desta
   * classe exista em um programa JAVA. Para que isto ocorra, uma das premissas
   * � restringir a responsabilidade de criar objetos do tipo desta classe a ela
   * pr�pria. Isto se faz colocando o construtor com acesso privado. Este
   * construtor chama o m�todo que inicializa os controladores.
   *
   */
  private Fachada() {
    initControladores();
  }

  /**
   * Inicializa os controladores.
   */
  private void initControladores() {
    this.controleCadastrarEleitor = new ControleCadastrarEleitor();
    this.controleCadastrarUniversidadeConveniada = new ControleCadastrarUniversidadeConveniada();
    this.controleCadastrarComposicaoEje = new ControleCadastrarComposicaoEje();
    this.controleCadastrarUsuario = new ControleCadastrarUsuario();
  }

  /**
   * M�todo respons�vel por retornar a refer�ncia da �nica inst�ncia desta
   * classe no programa JAVA e por criar esta �nica inst�ncia, caso ela n�o
   * exista.
   *
   * @return Fachada refer�ncia para a �nica inst�ncia desta classe.
   */
  public synchronized static Fachada getInstance() {
    if (instancia == null) {
      instancia = new Fachada();
    }
    return instancia;
  }

  public Fachada(ControleCadastrarEleitor controleCadastrarEleitor) {
    this.controleCadastrarEleitor = controleCadastrarEleitor;
  }

  public Eleitor consultarEleitor(String titulo) throws RepositorioException,
          SQLException, ClassNotFoundException, ParseException {
    return this.controleCadastrarEleitor.consultarEleitor(titulo);
  }

  public List consultarOcupacao() throws RepositorioException, SQLException,
          ClassNotFoundException {
    return this.controleCadastrarEleitor.consultarOcupacao();
  }

  public String consultarOcupacao(int numero) throws RepositorioException,
          SQLException, ClassNotFoundException {
    return this.controleCadastrarEleitor.consultarOcupacao(numero);
  }

  public int cadastraEleitor(Eleitor eleitor) throws RepositorioException,
          SQLException, ClassNotFoundException {
    return this.controleCadastrarEleitor.cadastraEleitor(eleitor);
  }

  public boolean consultarEleitorCadastrado(String titulo, String numZona)
          throws SQLException, ClassNotFoundException {
    return this.controleCadastrarEleitor.consultarEleitorCadastrado(titulo,
            numZona);
  }

  public List consultarDatasInscricao(int zona) throws SQLException, ClassNotFoundException {
    return this.controleCadastrarEleitor.consultarDatasInscricao(zona);
  }

  public List consultarEleitorAno(String ano, int zona, String tipoOrdenacao) throws SQLException, ClassNotFoundException, ParseException {
    return this.controleCadastrarEleitor.consultarEleitorAno(ano, zona, tipoOrdenacao);
  }

  public ArrayList<UniversidadeConveniada> consultarEstatisticasPorEntidade() throws ClassNotFoundException {
    return this.controleCadastrarEleitor.consultarEstatisticasPorEntidade();
  }

  public LinkedHashMap<String, Integer> consultarEstatisticasPorAno() throws ClassNotFoundException {
    return this.controleCadastrarEleitor.consultarEstatisticasPorAno();
  }

  //public List consultarEstatisticasPorPeriodo(java.util.Date dataInicialPeriodo, java.util.Date dataFinalPeriodo) throws ClassNotFoundException{
  public int consultarEstatisticasPorPeriodo(String dataInicialPeriodo, String dataFinalPeriodo) throws ClassNotFoundException {
    return this.controleCadastrarEleitor.consultarEstatisticasPorPeriodo(dataInicialPeriodo, dataFinalPeriodo);
  }

  public ArrayList<Zona> consultarZonasEleitorais() throws
          SQLException, ClassNotFoundException {
    return this.controleCadastrarEleitor.consultarZonasEleitorais();
  }
  //----------Inicio do CONTROLE UNIVERSIDADE CONVENIADA----------

  public List listarUniversidadeConveniada() throws SQLException, ClassNotFoundException {
    return this.controleCadastrarUniversidadeConveniada.listarUniversidadeConveniada();
  }

  public void cadastrarUniversidadeConveniada(UniversidadeConveniada universidade) throws SQLException, ClassNotFoundException {
    this.controleCadastrarUniversidadeConveniada.cadastrarUniversidadeConveniada(universidade);

  }

  public boolean consultarCnpjExistente(String cnpj) throws ClassNotFoundException, SQLException {
    return this.controleCadastrarUniversidadeConveniada.consultarCnpjExistente(cnpj);

  }

  public ArrayList<UniversidadeConveniada> listarTodasUniversidades() throws ClassNotFoundException {
    return this.controleCadastrarUniversidadeConveniada.listarTodasUniversidades();
  }

  public UniversidadeConveniada consultarUniversidade(int codObjeto) throws ClassNotFoundException {
    return this.controleCadastrarUniversidadeConveniada.consultarUniversidade(codObjeto);
  }

  public void editarUniversidade(UniversidadeConveniada universidade) throws ClassNotFoundException {
    this.controleCadastrarUniversidadeConveniada.editarUniversidade(universidade);
  }

  public void editarDataFinalUniversidade(int codObjeto, Date dataFinal) throws ClassNotFoundException {
    this.controleCadastrarUniversidadeConveniada.editarDataFinalUniversidade(codObjeto, dataFinal);
  }

  public String consultarCodObjetoUniversidade(String cnpj) throws ClassNotFoundException, SQLException {
    return this.controleCadastrarUniversidadeConveniada.consultarCodObjetoUniversidade(cnpj);
  }

  public boolean existeNumero(String numeroAnoConvenio, int codObjeto) throws SQLException, ClassNotFoundException {
    return this.controleCadastrarUniversidadeConveniada.existeNumero(numeroAnoConvenio, codObjeto);
  }

  public List listasUniversidadesConveniadas() throws SQLException, ClassNotFoundException {
    return this.controleCadastrarUniversidadeConveniada.listasUniversidadesConveniadas();
  }
  //----------  FIM do CONTROLE UNIVERSIDADE CONVENIADA----------

  //----------Inicio do CONTROLE COMPOSICAO EJE ----------
  public List consultarComposicaoEje() throws RepositorioException, SQLException, ClassNotFoundException, ParseException {
    return this.controleCadastrarComposicaoEje.consultarComposicaoEje();
  }

  public void editarComposicaoEje(ComposicaoEje composicao) throws ClassNotFoundException {
    this.controleCadastrarComposicaoEje.editarComposicaoEje(composicao);
  }

  public boolean existeComposicaoEje(String id) throws ClassNotFoundException {
    return this.controleCadastrarComposicaoEje.existeComposicaoEje(id);
  }

  public void inserirComposicaoEje(ComposicaoEje composicao) throws ClassNotFoundException {
    this.controleCadastrarComposicaoEje.inserirComposicaoEje(composicao);
  }
  //----------  FIM do CONTROLE COMPOSICAO EJE ----------

  public Date getDataFinal() throws ClassNotFoundException {
    return this.controleCadastrarUniversidadeConveniada.getDataFinal();
  }

  public Usuario consultarUsuarioLogin(String login) throws ClassNotFoundException {
    return this.controleCadastrarUsuario.consultarUsuarioLogin(login);
  }

  public Zona consultarZonaPorNumero(Integer numZona) throws ClassNotFoundException, SQLException {
    return this.controleCadastrarEleitor.consultarZona(numZona);
  }

}
