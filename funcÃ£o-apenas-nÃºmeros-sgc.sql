create or replace FUNCTION FUNC_APENAS_NUMEROS(texto varchar) RETURN NUMBER AS
temp_fone varchar(20):=' ';
BEGIN

if texto is null 
then
  return 0;
else
  for indice in 1..length(texto) loop
    if substr(texto,indice,1) not in ('0','1','2','3','4','5','6','7','8','9') then
      return 0;
    end if;
  end loop;  
end if;
--
return 1;

END FUNC_APENAS_NUMEROS;