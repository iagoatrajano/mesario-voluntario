create or replace FUNCTION FUNC_EXTRAIR_MASCARA(texto varchar) RETURN VARCHAR AS
BEGIN
if texto is null
then
  return texto;
else
  return replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(texto,' ',''),'.',''),'-',''),'/',''),'\',''),'|',''),'(',''),')',''),'[',''),']',''),'{',''),'}','');
end if;

END FUNC_EXTRAIR_MASCARA;