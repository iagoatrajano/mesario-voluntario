create or replace FUNCTION FUNC_EXTRAIR_TELEFONE(texto varchar) RETURN VARCHAR AS
temp_texto varchar(20);
BEGIN
if texto is null
then
  return null;
else
  temp_texto:=FUNC_EXTRAIR_MASCARA(texto);
  if FUNC_APENAS_NUMEROS(temp_texto)<>0
  then
    return texto;
  else
    return null;
  end if;
end if;

END FUNC_EXTRAIR_TELEFONE;