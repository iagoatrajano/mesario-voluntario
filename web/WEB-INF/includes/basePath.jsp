<%
    String mensagem = (String) request.getAttribute("mensagem");
    //
    String path = request.getContextPath();
    String baseServer = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
    String basePath = baseServer + path + "/";

%>

<c:set value="<%=baseServer%>" var="baseServer"/>
<c:set value="<%=basePath%>" var="basePath"/>

<base href="${basePath}"/>