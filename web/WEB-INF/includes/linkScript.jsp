<link rel="icon" href="${basePath}layout_aplicacoes/imagens/logo-tre.gif" type="image/gif" />
<link rel="shortcut icon" href="${basePath}layout_aplicacoes/imagens/logo-tre.gif" type="image/gif"/>

<link href="${basePath}layout_aplicacoes/css/estilo_aplicacoes.css" rel="stylesheet" type="text/css"/>
<link href="${basePath}layout_aplicacoes/css/estilo_mesario.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="${basePath}js/jquery-1.7.2.min.js"></script>

<script type="text/javascript" src="${basePath}layout_aplicacoes/js/seginf_aplicacoes.js"></script>
<script type="text/javascript" language="javascript" src="${basePath}js/javascript.js"></script>
<script type="text/javascript" language="javascript" src="${basePath}componente_acesso/js/javascript.js"></script>
