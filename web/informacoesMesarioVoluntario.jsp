<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">



<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp" %>
        <%@include file="/WEB-INF/includes/linkScript.jsp" %>

        <%            String titulo = (String) request.getParameter("titulo"); //vem do JSP

            String nome = (String) request.getParameter("nome"); //vem do JSP

            String dddFonePrincipal = (String) request.getParameter("dddFone"); //vem do JSP
            String fonePrincipal = (String) request.getParameter("telefone"); //vem do JSP
            String dddFoneOpcional = (String) request.getParameter("dddFoneOpcional"); //vem do JSP
            String foneOpcional = (String) request.getParameter("foneOpcional"); //vem do JSP
            String telefone = "";
            if (!fonePrincipal.trim().equals("")) {
                if (!dddFonePrincipal.trim().equals("") & !dddFonePrincipal.trim().equals("0")) {
                    telefone = "(" + dddFonePrincipal + ") " + fonePrincipal;
                } else {
                    telefone = fonePrincipal;
                }
            }
            if (!foneOpcional.trim().equals("")) {
                if (!dddFoneOpcional.trim().equals("") & !dddFoneOpcional.trim().equals("0")) {
                    telefone = telefone + " / (" + dddFoneOpcional + ") " + foneOpcional;
                } else {
                    telefone = telefone + " / " + foneOpcional;
                }
            }

            String email = (String) request.getParameter("email"); //vem do JSP
            if (email.equals("")) {
                email = "N�o Informado";
            }

            String endereco = (String) request.getParameter("endereco"); //vem do JSP
            String bairro = (String) request.getParameter("bairro"); //vem do JSP
            String cidade = (String) request.getParameter("cidade"); //vem do JSP
            String cep = (String) request.getParameter("cep"); //vem do JSP
            String enderecoCompleto = "";
            if ((cep.equals("")) || (cep.trim().equals("0"))) {
                enderecoCompleto = endereco + ", " + bairro + ", " + cidade;
            } else {
                enderecoCompleto = endereco + ", " + bairro + ", " + cidade + ", " + cep;
            }

        %>

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp" %></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <script type="text/javascript">

            function fecharJanela() {
                window.self.close();
            }

        </script>

    </head>



    <body>

        <c:if test="${sessionScope.usuario==null}">
            <c:redirect url="${basePath}usuarioNaoLogado.jsp"/>
        </c:if>

        <%@include file="/WEB-INF/includes/debug.jsp" %>

        <div id="tudo">
            <div id="mesario">
                <ul id="popup">
                    <li><strong>T�tulo: </strong><span class="result titulo_popup"></span><%=titulo%></li>
                    <li><strong>Nome: </strong><span class="result nome_popup"></span><%=nome%></li>
                    <li><strong>Telefone(s): </strong><span class="result telefone_popup"></span><%=telefone%></li>
                    <li><strong>E-mail: </strong><span class="result email_popup"></span><%=email%></li>
                    <li><strong>Endere�o: </strong><span class="result endereco_popup"></span><%=enderecoCompleto%></li>
                </ul>
            </div>
        </div>

    </body>

</html>