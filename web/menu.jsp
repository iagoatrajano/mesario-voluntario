<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<?xml version="1.0" encoding="iso-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">




<html>

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>

        <c:set var="nivel" value="${sessionScope.usuario.nivel}"/>

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Menu </title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <!--[if lte IE 6]>
      <script src="/layout_aplicacoes/js/DD_belatedPNG_0.0.7a.js" type="text/javascript" charset="iso-8859-1"></script>
          <script type="text/javascript">
          DD_belatedPNG.fix('#topo, #caixa_assinatura, img');
          </script>
          <script src="/layout_aplicacoes/js/ie6_legis.js" type="text/javascript" charset="iso-8859-1"></script>
        <![endif]-->

    </head>

    <body>

        <c:if test="${sessionScope.usuario==null}">
            <c:redirect url="${basePath}usuarioNaoLogado.jsp"/>
        </c:if>

        <%@include file="/WEB-INF/includes/debug.jsp"%>

        <div id="tudo">

            <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
            <%@include file="/WEB-INF/includes/topo.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

            <div id="mesario">

                <div id="voltar_sair"><a href="${basePath}encerrarSessao.jsp" id="sair">Sair</a></div></br></br>
                <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp"%></h1>

                <c:if test="${nivel eq 1}">
                    <ul class="menu">
                        <li> <a href="${basePath}ServletPreencherConsultaMesarioVoluntario.do"> Consultar Mes�rios Volunt�rios</a> </li>
                    </ul>
                </c:if>

                <c:if test="${nivel eq 2}">

                    <ul class="menu">
                        <li> <a href="${basePath}ServletPreencherListarConvenios.do"> Analisar Conv�nio de Universidades</a> </li>

                        <li> <a href="${basePath}consultarEstatisticas.jsp"> Consultar Estat�sticas            </a> </li>

                        <li> <a href="${basePath}ServletPreencherComposicaoEje.do"> Gerenciar Dados da Presid�ncia            </a> </li>

                        <li> <a href="${basePath}ServletPreencherListarConveniosValidos.do"> Listar Conv�nios V�lidos       </a></li>

                        <li> <a href="${basePath}ServletPreencherConsultaMesarioVoluntario.do"> Consultar Mes�rios Volunt�rios</a> </li>

                    </ul>

                </c:if>

            </div>

            <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
            <%@include file="/WEB-INF/includes/rodape.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->

        </div> <!-- fim da div#tudo -->

    </body>

</html>