<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>


        <c:set var="opcaoEstatistica" value="${requestScope.opcaoEstatistica}" />
        <c:set var="exibir" value="${requestScope.exibir}" />


        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Estat�sticas</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

    </head>

    <body>

        <c:if test="${sessionScope.usuario==null}">
            <c:redirect url="${basePath}usuarioNaoLogado.jsp"/>
        </c:if>

        <c:if test="${!sessionScope.grupoGt.contains(sessionScope.usuario.login)}">
            <c:redirect url="${basePath}usuarioSemPermissao.jsp"/>
        </c:if>

        <%@include file="/WEB-INF/includes/debug.jsp"%>

        <div id="tudo">
            <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
            <%@include file="/WEB-INF/includes/topo.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->


            <c:choose>
                <c:when test="${exibir ne 1}">
                    <div id="mesario">

                        <div id="voltar_sair"><a href="${basePath}menu.jsp" id="voltar">Menu</a><a href="${basePath}encerrarSessao.jsp" id="sair">Sair</a></div></br></br>
                        <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Estat�sticas</h1>
                        <ul class="menu">
                            <li> <a href="${basePath}ServletConsultarEstatisticas.do?opcao=entidade">Consultar por Universidade</a> </li>

                            <li> <a href="${basePath}ServletConsultarEstatisticas.do?opcao=ano"> Consultar por Ano</a> </li>

                            <li> <a href="${basePath}periodoDeConvenio.jsp">Consultar por Per�odo</a> </li>
                        </ul>
                    </div>
                </c:when>

                <c:otherwise>
                    <div id="mesario">
                        <div id="voltar_sair"><a href="${basePath}consultarEstatisticas.jsp" id="voltar">Voltar</a><a href="${basePath}menu.jsp" id="voltar">Menu</a><a href="${basePath}encerrarSessao.jsp" id="sair">Sair</a></div></br></br>
                        <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Estat�sticas</h1>
                        <p class="destaque">Quantidade de mes�rios volunt�rios inscritos por <c:out value="${opcaoEstatistica}" />:</p>
                        <table id="tabela_estatisticas" cellspacing="0">
                            <thead>
                                <tr>
                                    <!-- Ano ou institui��o -->
                                    <c:choose>
                                        <c:when test="${opcaoEstatistica eq 'ano'}">
                                            <th id="ano">Ano</th>
                                            </c:when>

                                        <c:otherwise>
                                            <th id="entidade">Entidade</th>
                                            <th id="convenio">Conv�nio</th>
                                            </c:otherwise>
                                        </c:choose>

                                    <th id="quantidade">Quantidade</th>
                                </tr>
                            </thead>

                            <tbody>

                                <c:set var="total" value="0" />
                                <c:choose>
                                    <c:when test="${opcaoEstatistica eq 'ano'}">
                                        <c:forEach var="ano" items="${requestScope.listaAno}">
                                            <tr>
                                                <td headers="ano">${ano.key}</td>
                                                <td headers="quantidade">${ano.value}</td>
                                                <c:set var="total" value="${total + ano.value}" />
                                            </tr>
                                        </c:forEach>
                                    </c:when>

                                    <c:otherwise>
                                        <c:forEach var="entidade" items="${requestScope.listaUniversidades}">
                                            <tr>
                                                <td headers="entidade"><c:if test="${entidade.nomeFantasia ne 'OUTRAS'}" >${entidade.sigla} - </c:if>${entidade.nomeFantasia}</td>
                                                <td headers="convenio">${entidade.convenioValido}</td>
                                                <td headers="quantidade">${entidade.quantidadeEleitores}</td>
                                                <c:set var="total" value="${total + entidade.quantidadeEleitores}" />
                                            </tr>
                                        </c:forEach>

                                    </c:otherwise>
                                </c:choose>
                                <tr >
                                    <c:choose>
                                        <c:when test="${opcaoEstatistica eq 'ano'}">
                                            <td class="destaque_total" headers="ano">Total</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td class="destaque_total" headers="entidade">Total</td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:if test="${opcaoEstatistica eq 'entidade'}">
                                        <td headers="convenio"></td>
                                    </c:if>
                                    <td class="destaque_total" headers="ano">${total}</td>
                                </tr>

                            </tbody>

                        </table>
                    </div>
                </c:otherwise>
            </c:choose>

            <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
            <%@include file="/WEB-INF/includes/rodape.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
        </div>

    </body>

</html>