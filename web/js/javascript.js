function editarConvenio(uni){
	//document.form1.universidadeSelecionada.value = "asdasda";
	document.form1.action = "./ServletPreencherEditarConvenio.do?universidadeSelecionada="+uni;
	document.form1.submit();
}
function ehNumero(campo, texto) {	
    for (var i = 0; i < campo.value.length; i++) {
    	
	  caracter = campo.value.charAt( i );

	  if ((escape(caracter) < "0" ) || (escape(caracter) > "9" )) {
	    alert("O campo <" + texto + "> deve ser todo num�rico.");
	    campo.focus();
	    campo.select();
	    return false;
	  }
	  
    }
    
    return true;
}

function validarCPF(numeroCpf){
	   var cpf = numeroCpf;
	   var filtro = /^\d{3}.\d{3}.\d{3}-\d{2}$/i;
	   if(!filtro.test(cpf)){
	    // window.alert("CPF inv�lido. Tente novamente.");
		 return false;
	   }
	   
	   cpf = remove(cpf, ".");
	   cpf = remove(cpf, "-");
	    
	   if(cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" ||
		  cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" ||
		  cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" ||
		  cpf == "88888888888" || cpf == "99999999999"){
		//  window.alert("CPF inv�lido. Tente novamente.");
		  return false;
	   }

	   soma = 0;
	   for(i = 0; i < 9; i++)
	   	 soma += parseInt(cpf.charAt(i)) * (10 - i);
	   resto = 11 - (soma % 11);
	   if(resto == 10 || resto == 11)
		 resto = 0;
	   if(resto != parseInt(cpf.charAt(9))){		
		 return false;
	   }
	   soma = 0;
	   for(i = 0; i < 10; i ++)
		 soma += parseInt(cpf.charAt(i)) * (11 - i);
	   resto = 11 - (soma % 11);
	   if(resto == 10 || resto == 11)
		 resto = 0;
	   if(resto != parseInt(cpf.charAt(10))){	     
		 return false;
	   }
	   else	   return true;
}

function validarCNPJ(ObjCnpj){
    var cnpj = ObjCnpj;
    var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
    var dig1= new Number;
    var dig2= new Number;
    
    exp = /\.|\-|\//g
    cnpj = cnpj.toString().replace( exp, "" ); 
    var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));
            
    for(i = 0; i<valida.length; i++){
            dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);  
            dig2 += cnpj.charAt(i)*valida[i];       
    }
    dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
    dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));
    
    if(((dig1*10)+dig2) != digito) { 
         //   alert('CNPJ Invalido!');
          return false
    }
    else    return true;
            
}



// Fun��o para valida��o de CEP.
function validarCep(numeroCep)
{
	var pesquisa = /^\d{2}.\d{3}-\d{3}$/;
	  var CEP = numeroCep;

	  if(CEP.match(pesquisa)){	    
	     return true;	  
	  }else{
	    alert("O CEP informado n�o � v�lido");
	    return false;
	  }
}
	 
function remove(str, sub) {
	i = str.indexOf(sub);
	r = "";
	if (i == -1) return str;
	r += str.substring(0,i) + remove(str.substring(i + sub.length), sub);
	return r;
}

function formatarTitulo(tituloAjustado){
	
     var tamanho = tituloAjustado.length;

	 if(tamanho < 12){
		   while (tamanho != 12){
		   		tituloAjustado = "0" + tituloAjustado;
		   		tamanho ++;
		   }
	 }

	 return validarTitulo(tituloAjustado);
}

function validarTitulo(titulo){

           var somatorio = 0, resto, digVerificador1, digVerificador2;
	   var ehTituloValido = false;

	   var dig1 = parseInt(titulo.substring(0,1));
	   var dig2 = parseInt(titulo.substring(1,2));
	   var dig3 = parseInt(titulo.substring(2,3));
	   var dig4 = parseInt(titulo.substring(3,4));
	   var dig5 = parseInt(titulo.substring(4,5));
	   var dig6 = parseInt(titulo.substring(5,6));
	   var dig7 = parseInt(titulo.substring(6,7));
	   var dig8 = parseInt(titulo.substring(7,8));

	   var digUF1 = parseInt(titulo.substring(8,9));
	   var digUF2 = parseInt(titulo.substring(9,10));

	   var digVer1 = parseInt(titulo.substring(10,11));
	   var digVer2 = parseInt(titulo.substring(11,12));

	   //para calcular o primeiro d�gito verificador
	   somatorio = (dig1 * 9) + (dig2 * 8) + (dig3 * 7) + (dig4 * 6) +
                       (dig5 * 5) + (dig6 * 4) + (dig7 * 3) + (dig8 * 2);

	   resto = somatorio % 11;

	   if ((resto == 0) || (resto == 1)) {
	   		digVerificador1 = 0;
	   }else{
			digVerificador1 = 11 - resto;
	   }
	   ////////////////////////////////////////////////////

	   //para calcular o segundo d�gito verificador
	   somatorio = (digUF1 * 4) + (digUF2 * 3) + (digVerificador1 * 2);

	   resto = somatorio % 11;

	   if ((resto == 0) || (resto == 1)) {
			digVerificador2 = 0;
	   }else{
			digVerificador2 = 11 - resto;
	   }
	   ////////////////////////////////////////////////////

	   //verifica se os digitos verificadores s�o v�lidos
	   if((digVer1 == digVerificador1) && (digVer2 == digVerificador2)) {
			ehTituloValido = true;
	   }else{
			ehTituloValido = false;
	   }

	   return ehTituloValido;
}

function mensagemMailValido(campoMail) {
  if (ehEmailValido(campoMail.value)==false) {
      msg = "Por favor, digite um e-mail v�lido.";
      alert(msg);
      campoMail.focus();
      return false;
  }
  else {
      return true;
  }
}


function ehEmailValido(email) {

    posArroba = email.indexOf("@");
    primeiroPonto = email.indexOf(".",posArroba+1);
    tamanhoDominio = email.substring(posArroba+1,primeiroPonto).length;

    if (email != "") {

	if ((posArroba == -1 && email != "")
		|| email.indexOf("@",posArroba+1) != -1
		|| email.indexOf("@") == email.length-1
		|| email.indexOf(".") == email.length-1
		|| email.indexOf("@@") != -1
		|| email.indexOf("..") != -1
		|| email.indexOf(".@") != -1
		|| email.indexOf("@.") != -1
		|| email.substring(posArroba+1,email.length).indexOf(".") == -1
		|| email.charAt(0) == '@'
		|| email.charAt(0) == '.'
		|| email.charAt(email.length-1) == '.'
		|| posArroba < 2
		|| tamanhoDominio < 2
		|| tamanhoDominio > 26
		|| soNumeros(email)
		   ) {

	    return false;

	} else {
	    return true;
	}

    } else {
	return true;
    }

   return true;
}

//  Rotina revista: soNumeros()
//  Motivos: (a) elas n�o s�o usadas
//	     (b) h� erros sem�nticos:
//		 - soNumeros() retorna "true" se n�o tiver letras!
//		 - ehLetra() n�o considera caracteres acentuados!
//  Revis�o em: 16/ago/2002, por jftm.
//
function soNumeros(email) {

    for (i=0; i<email.length; i++) {
       if (ehLetra(email.charAt(i))) {	// estava errado! //
	    return false;
       }
    }
    return true;
}

//
//  Rotina comentada: ehLetra()
//  Motivo: (a) rotina n�o � usada em outro lugar (era
//		chamada por soNumeros(), por engano);
//	    (b) n�o considera caracteres acentuados!
//		(problema potencial);
//	    (c) pode ser otimizada. Ex: 'A' <= c && c <= 'Z'.
//  Revis�o em: 16/ago/2002, por jftm.
//
function ehLetra(caracter) {
    var alfabetoMaiusculo = "ABCDEFGHIJKLMNOPQRSTUVXYZ"
    var caracterMaiusculo = caracter.toUpperCase();
    if (caracter.length == 1) {

	if (alfabetoMaiusculo.indexOf(caracterMaiusculo) == -1) {
	    return false;
	} else {
	    return true;
	}
    } else {
        return false;
    }
}

//valida numero inteiro com mascara
function mascaraInteiro(e) {
var keycode;
if (window.event) keycode = window.event.keyCode;
else if (e) keycode = e.which;
 
 if (keycode < 48 || keycode > 57){
  event.returnValue = false;
  return false;
 }
  return true;
}

//adiciona mascara de cep
function MascaraCep(cep , e){
	var keycode;
	if (window.event) {
		keycode = window.event.keyCode;
	} else {
		if (e) {
			keycode = e.which;
		}
	}

	if(mascaraInteiro(cep)==false){
		event.returnValue = false;
	}
    
    return formataCampo(cep, '00.000-000', keycode);
    			
}

//adiciona mascara do ddd
function MascaraDDD(ddd){ 
	if(mascaraInteiro(ddd)==false){
	   event.returnValue = false;
	} 
	return formataCampo(ddd, '(000)', event);
}

//adiciona mascara ao telefone
function MascaraTelefone(tel){ 
	if(mascaraInteiro(tel)==false){
	   event.returnValue = false;
	} 
	return formataCampo(tel, '(00) 0000-0000', event);
}

function formataCampo(campo, Mascara, evento) {
	 var boleanoMascara; 
	 
	 var Digitato = evento;
	 //evento.keyCode;
	 exp = /\-|\.|\/|\(|\)| /g
	 campoSoNumeros = campo.value.toString().replace( exp, "" ); 
	 
	 var posicaoCampo = 0; 
	 var NovoValorCampo="";
	 var TamanhoMascara = campoSoNumeros.length; 
	 
	 if (Digitato != 8) { // backspace 
	 for(i=0; i<= TamanhoMascara; i++) { 
	   boleanoMascara = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
	   || (Mascara.charAt(i) == "/")) 
	   boleanoMascara = boleanoMascara || ((Mascara.charAt(i) == "(") 
	   || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " ")) 
	 if (boleanoMascara) { 
	   NovoValorCampo += Mascara.charAt(i); 
	   TamanhoMascara++;
	 }else { 
	  NovoValorCampo += campoSoNumeros.charAt(posicaoCampo); 
	  posicaoCampo++; 
	    } 
	}
	    campo.value = NovoValorCampo;
	 return true; 
	 }else { 
	 return true; 
	 }
	}
//-->