<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Estat�sticas</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <script language="javascript" type="text/javascript">

            //VALIDA��O DA DATA

            function VerificaData(digData)
            {
                var bissexto = 0;
                var data = digData.value;
                var tam = data.length;
                if (tam == 10)
                {
                    var dia = data.substr(0, 2);
                    var mes = data.substr(3, 2);
                    var ano = data.substr(6, 4);
                    if ((ano > 1900) || (ano < 2100))
                    {
                        switch (mes)
                        {
                            case '01':
                            case '03':
                            case '05':
                            case '07':
                            case '08':
                            case '10':
                            case '12':
                                if (dia <= 31)
                                {
                                    return true;
                                }
                                break;

                            case '04':
                            case '06':
                            case '09':
                            case '11':
                                if (dia <= 30)
                                {
                                    return true;
                                }
                                break;
                            case '02':
                                /* Validando ano Bissexto / fevereiro / dia */
                                if ((ano % 4 == 0) || (ano % 100 == 0) || (ano % 400 == 0))
                                {
                                    bissexto = 1;
                                }
                                if ((bissexto == 1) && (dia <= 29))
                                {
                                    return true;
                                }
                                if ((bissexto != 1) && (dia <= 28))
                                {
                                    return true;
                                }
                                break;
                        }
                    }
                }

                if (data != "__/__/____")
                {
                    alert("A Data " + data + " � inv�lida!");
                    digData.focus();
                    return false;
                } else
                    return true;
            }

            //verifica se tem algum campo em branco
            function verificaDados()
            {
                if (document.periodoConvenio.dataInicialPeriodo.value == "")

                {
                    alert("Preencha a Data Inicial.");
                    return false;
                }

                if (document.periodoConvenio.dataFinalPeriodo.value == "")

                {
                    alert("Preencha a Data Final.");
                    return false;
                } else
                    return true;
            }

        </script>




    </head>

    <body>

        <c:if test="${sessionScope.usuario==null}">
            <c:redirect url="${basePath}usuarioNaoLogado.jsp"/>
        </c:if>

        <c:if test="${!sessionScope.grupoGt.contains(sessionScope.usuario.login)}">
            <c:redirect url="${basePath}usuarioSemPermissao.jsp"/>
        </c:if>

        <%@include file="/WEB-INF/includes/debug.jsp"%>

        <div id="tudo">
            <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
            <%@include file="/WEB-INF/includes/topo.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

            <div id="mesario">

                <div id="voltar_sair"><a href="${basePath}consultarEstatisticas.jsp" id="voltar">Voltar</a><a href="${basePath}menu.jsp" id="voltar">Menu</a><a href="${basePath}encerrarSessao.jsp" id="sair">Sair</a></div></br></br>
                <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Estat�sticas</h1>

                <form id="periodoConvenio" name="periodoConvenio" method="post" action="${basePath}ServletConsultarEstatisticas.do?opcao=per�odo" onsubmit="return verificaDados();">

                    <fieldset>
                        <legend>Per�odo</legend>

                        <p class="inputs_inline">
                            <label for="data_inicial">Data Inicial:</label><input id="data_inicial" name="dataInicialPeriodo" class="mascara-data" type="text" onblur="VerificaData(this);" ></input>
                            <label for="data_final">Data Final:</label><input id="data_final" name="dataFinalPeriodo" class="mascara-data" type="text" onblur="VerificaData(this);" ></input> <br/>
                        </p>
                    </fieldset>

                    <p>
                        <input type="submit" value="Pesquisar" class="botoes_form" />
                    </p>

                </form>

                <c:choose>
                    <c:when test="${exibir eq 1}">

                        <p class="destaque">Quantidade de mes�rios volunt�rios inscritos por per�odo:</p>
                        <table id="tabela_estatisticas" cellspacing="0">
                            <thead>
                                <tr>
                                    <th id="periodo">Per�odo</th>
                                    <th id="quantidade">Quantidade</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td headers="datas">${requestScope.dataInicialPeriodo} <b>a</b> ${requestScope.dataFinalPeriodo}</td>
                                    <td headers="quantidade">${requestScope.quantidade}</td>
                                </tr>
                            </tbody>

                        </table>

                    </c:when>
                </c:choose>

            </div>

            <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
            <%@include file="/WEB-INF/includes/rodape.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
        </div>

    </body>

</html>