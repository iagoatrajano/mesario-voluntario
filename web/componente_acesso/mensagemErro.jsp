<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Erro ao alterar a senha.</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>

        <style media="print">
            .printHidden {visibility: hidden; display: none}
        </style>
        <style media="screen">
            .screenHidden {visibility: hidden; display: none}
        </style>


        <!--[if lte IE 6]>
              <script src="../js/js_layout/DD_belatedPNG_0.0.7a.js" type="text/javascript" charset="iso-8859-1"></script>
                  <script type="text/javascript">
                  DD_belatedPNG.fix('#topo, #caixa_assinatura, img');
                  </script>
        <![endif]-->

    </head>

    <body>

        <div id="tudo">
            <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
            <%@include file="/WEB-INF/includes/topo.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

            <div id="login" class="mesario_login">

                <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp"%></h1>

                <h2>Controle de Acesso</h2>
                <p class="msg_erro"><strong>Erro:</strong><%=mensagem%></p>

                <input type="button" id="volta_login" title="Volte � tela de altera��o de senha" class="botoes_form" value="Voltar" onclick="javascript:history.back()"/>

            </div>

            <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
            <%@include file="/WEB-INF/includes/rodape.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
        </div> <!-- fim da div#tudo -->

    </body>
</html>