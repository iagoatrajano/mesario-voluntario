<%@ page session="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">


    <head>

        <%@include file="/WEB-INF/includes/basePath.jsp"%>
        <%@include file="/WEB-INF/includes/linkScript.jsp"%>

        <title><%@include file="/WEB-INF/includes/nomeSistema.jsp"%> - Login</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


        <!--[if lte IE 6]>
      <script src="/layout_aplicacoes/js/DD_belatedPNG_0.0.7a.js" type="text/javascript" charset="iso-8859-1"></script>
          <script type="text/javascript">
          DD_belatedPNG.fix('#topo, #caixa_assinatura, img');
          </script>
        <![endif]-->


    </head>

    <body>
        <div id="tudo">

            <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
            <%@include file="/WEB-INF/includes/topo.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

            <div id="login" class="mesario_login">
                <h1><%@include file="/WEB-INF/includes/nomeSistema.jsp"%></h1>


                <form id="form_login" name="form_login" method="post" action="${basePath}ServletValidarUsuario.do">

                    <input type="hidden" name="login" value=""/>
                    <input type="hidden" name="password" value=""/>


                    <fieldset>
                        <legend>Controle de Acesso - <strong>LOGIN</strong></legend>

                        <p><label for="usuario">Usu�rio:</label><input type="text" title="Digite seu nome de usu�rio" id="usuario" name="usuario" onkeypress="return enterLogin()"/></p>
                        <p><label for="senha">Senha:</label><input type="password" title="Digite sua senha" id="senha" name="senha" onkeypress="return enterLogin()"/></p>

                        <input type="button" value="Entrar" onclick="validarLogin();" class="botoes_form" title="Clique para entrar no sistema"/>
                        <input type="button" value="Limpar" class="botoes_form" onclick="limparLogin();" title="Limpar os dados do formul�rio"/>

                    </fieldset>

                    <p id="mensagem"><c:out value="${requestScope.mensagem}"/> </p>

                    <p><a href="${basePath}componente_acesso/alterarSenha.jsp" class="botoes_form">Alterar Senha</a></p>

                </form>
            </div>
            <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
            <%@include file="/WEB-INF/includes/rodape.jsp"%>
            <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->
        </div>
    </body>
</html>