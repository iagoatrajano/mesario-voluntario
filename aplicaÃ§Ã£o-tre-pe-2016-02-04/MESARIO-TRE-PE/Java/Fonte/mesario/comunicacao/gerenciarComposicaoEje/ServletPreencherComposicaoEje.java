package mesario.comunicacao.gerenciarComposicaoEje;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mesario.comunicacao.Fachada;
import mesario.controle.composicaoEje.ComposicaoEje;
import mesario.excecoes.RepositorioException;
import mesario.util.Util;

/**
 * Servlet implementation class ServletPreencherComposicaoEje
 */
public class ServletPreencherComposicaoEje extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Fachada fachada = null;

	public void init() throws ServletException {
		this.fachada = Fachada.getInstance();
	}

	public ServletPreencherComposicaoEje() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	protected void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException {
		
		ComposicaoEje diretor     = null;
		ComposicaoEje testemunha1 = null;
		ComposicaoEje testemunha2 = null;
		String mensagem = null;
		
		mensagem = (String)req.getAttribute("mensagem");
		
		Util util = new Util();
		
		List<ComposicaoEje> listaComposicaoEje = null;
		List <String> listaEstados = util.listarEstados();
		try {
			listaComposicaoEje = fachada.consultarComposicaoEje();
			
			for (ComposicaoEje composicaoEje : listaComposicaoEje) {
				if(composicaoEje.getTipo().equals("1")){
					diretor = new ComposicaoEje();
					diretor.setTratamento(composicaoEje.getTratamento());
					diretor.setBairro(composicaoEje.getBairro());
					diretor.setNome(composicaoEje.getNome());
					diretor.setNacionalidade(composicaoEje.getNacionalidade());
					diretor.setEstadoCivil(composicaoEje.getEstadoCivil());
					diretor.setRg(composicaoEje.getRg());
					diretor.setOrgaoExp(composicaoEje.getRg());
					diretor.setCpf(composicaoEje.getCpf());
					diretor.setOrgaoExp(composicaoEje.getOrgaoExp());
					diretor.setEndereco(composicaoEje.getEndereco());
					diretor.setBairro(composicaoEje.getBairro());
					diretor.setCep(composicaoEje.getCep());
					diretor.setCidade(composicaoEje.getCidade());
					diretor.setEstado(composicaoEje.getEstado());
					diretor.setCodObjeto(composicaoEje.getCodObjeto());
					
					listaComposicaoEje.remove(diretor);
				}
				else{
					if(testemunha1 == null){
						testemunha1 = new ComposicaoEje();
						testemunha1.setNome(composicaoEje.getNome());
						testemunha1.setCargo(composicaoEje.getCargo());
						testemunha1.setCpf(composicaoEje.getCpf());
						testemunha1.setRg(composicaoEje.getRg());
						testemunha1.setCodObjeto(composicaoEje.getCodObjeto());
					}
					else{
						testemunha2 = new ComposicaoEje();
						testemunha2.setNome(composicaoEje.getNome());
						testemunha2.setCargo(composicaoEje.getCargo());
						testemunha2.setCpf(composicaoEje.getCpf());
						testemunha2.setRg(composicaoEje.getRg());	
						testemunha2.setCodObjeto(composicaoEje.getCodObjeto());
					}
				}
				
			}
			
			req.setAttribute("diretor", diretor);
			
			req.setAttribute("testemunha1", testemunha1);
			
			req.setAttribute("testemunha2", testemunha2);
			
			req.setAttribute("listaEstados", listaEstados);
			
			req.setAttribute("mensagem", mensagem);
			
			getServletConfig().getServletContext().getRequestDispatcher("/gerenciarComposicao.jsp").forward(req, resp);
			
			
		} catch (RepositorioException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		
	

	}

}
