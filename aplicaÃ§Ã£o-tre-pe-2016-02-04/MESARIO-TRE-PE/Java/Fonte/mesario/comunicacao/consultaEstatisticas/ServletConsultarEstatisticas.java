package mesario.comunicacao.consultaEstatisticas;

import java.io.IOException;
import java.util.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import componente_acesso.controleusuario.usuario.Usuario;

import mesario.comunicacao.Fachada;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;

/**
 * Servlet implementation class ServletConsultarEstatisticas
 */
public class ServletConsultarEstatisticas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	Fachada fachada = null;

    public ServletConsultarEstatisticas() {
        super();
 
    }

    public void init() throws ServletException {
		this.fachada = Fachada.getInstance();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String opcao = req.getParameter("opcao");
		//String [] buffer = null;
		
		ArrayList<UniversidadeConveniada> listaUniversidades;
		LinkedHashMap <String, Integer> listaAno;
		List listaPeriodo; 
		int total, quantidade;
		
		
		try{
			
			if ("entidade".equals(opcao)){
				//consulta universidades
				
				listaUniversidades = fachada.consultarEstatisticasPorEntidade();
				req.setAttribute("listaUniversidades", listaUniversidades);
				req.setAttribute("exibir", 1);
				//calcula total
				
			}else if ("ano".equals(opcao)){
				//consulta por ano
				listaAno = fachada.consultarEstatisticasPorAno();
				req.setAttribute("listaAno", listaAno);
				req.setAttribute("exibir", 1);
				
			}else if ("per�odo".equals(opcao)){
				//consulta por periodo
				String dataInicial = req.getParameter("dataInicialPeriodo");
				String dataFinal = req.getParameter("dataFinalPeriodo");
				
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				
				Date dataInicialPeriodo = (Date) format.parse(dataInicial);
				Date dataFinalPeriodo = (Date) format.parse(dataFinal);
				
				quantidade = fachada.consultarEstatisticasPorPeriodo(dataInicial, dataFinal);
				
				
				
				req.setAttribute("dataInicialPeriodo", dataInicial);
				req.setAttribute("dataFinalPeriodo", dataFinal);
				req.setAttribute("quantidade", quantidade);
				req.setAttribute("exibir", 1);
				
			}
			req.setAttribute("opcaoEstatistica", opcao);
		} catch (Exception e) {
			e.printStackTrace();
			req.setAttribute("erro", e.getMessage());
			getServletConfig().getServletContext().getRequestDispatcher(
					"/erro.jsp?2").forward(req, res);
		} 
		
		if ("per�odo".equals(opcao))
			getServletConfig().getServletContext().getRequestDispatcher("/periodoDeConvenio.jsp").forward(req, res);
		else
			getServletConfig().getServletContext().getRequestDispatcher("/consultarEstatisticas.jsp").forward(req, res);
	}

}
