package mesario.comunicacao.consultaMesarioVoluntario;


import java.awt.JobAttributes;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import com.sun.org.apache.xpath.internal.operations.Equals;

import componente_acesso.controleusuario.usuario.Usuario;

import mesario.comunicacao.*;
import mesario.controle.mesario.Eleitor;
import mesario.controle.mesario.Zona;
import mesario.excecoes.RepositorioException;

/**
 * Servlet implementation class ServletPreencherConsultaMesarioVoluntario
 */
public class ServletPreencherConsultaMesarioVoluntario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Fachada fachada = null;

	public ServletPreencherConsultaMesarioVoluntario() {
		super();
	}

	public void init() throws ServletException {
		this.fachada = Fachada.getInstance();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
}
	

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// Lista que ir� salvar a lista de datas	
		List listaDatas;
		ArrayList<String> datas = new ArrayList<String>();
		ArrayList<Zona> zonasList = new ArrayList<Zona>();
		Usuario usuario = new Usuario();
		
		String mensagem = null;		

		try {
			HttpSession session = request.getSession();
		
			
			usuario = (Usuario) session.getAttribute("usuario");
			
			if(usuario.getNivel().equals("2"))
			{
				zonasList = this.fachada.consultarZonasEleitorais();
			}
			
			else
			{
				int zona = usuario.getZona().getNumero();
			
				listaDatas = this.fachada.consultarDatasInscricao(zona);

				// colocando apenas os anos das datas em uma nova lista
				for (Object object : listaDatas) {
					datas.add((String) object.toString().subSequence(0, 4));

				}		
			
			
				// removendo os repeditos da lista datas
				List data = new ArrayList(new HashSet(datas));
			
				//ordenendo datas
				Collections.sort(data);
			
				int ultimoAno = data.size()-1;
			
				String anoConsulta = (String) data.get(ultimoAno);
			
				List listaEleitores = null;
				try {
					listaEleitores = this.fachada.consultarEleitorAno(anoConsulta, zona,"");
				
					// Verifica se a lista esta vazia e manda uma mensagem de erro
					if (listaEleitores.isEmpty()) {
						System.out.println("LISTA ELEITOR � NULA");
						mensagem = "N�o foi encontrado nenhum mes�rio volunt�rio "
								+ "no ano de " + anoConsulta + " na " + zona
								+ "� Zona Eleitoral";
						request.setAttribute("mensagem", mensagem);
					
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				ArrayList<Eleitor> arrayListEleitor = new ArrayList<Eleitor>(listaEleitores);
			
				for (Eleitor eleitor : arrayListEleitor) {	
				
					String ocupacao = eleitor.getOcupacao();
				
					if(ocupacao != null){
					
						char[] arrayChar = new char[ocupacao.length()];
						System.out.println("Ocupacao antiga: "+ ocupacao);
						String COMMA_INSIDE = ",";
					
						do{
							int posicao = ocupacao.indexOf(",");			
							if(posicao != -1){
								arrayChar = ocupacao.toCharArray();
								arrayChar[posicao] = ' ';
								ocupacao = String.valueOf(arrayChar);
								eleitor.setOcupacao(ocupacao);
								System.out.println("Ocupacao Nova: "+ ocupacao);
								System.out.println("-------------------------------");
							}
						}while(ocupacao.indexOf(",") != -1);
					}
				}
			
				
				request.setAttribute("mostraTabela", true);
			
				request.setAttribute("listaEleitores", listaEleitores);

				// setando o atributos data com a listas de datas
				request.setAttribute("datas", data);
			
				//variavel que incida que � a primeira vez que a tela � chamada
				request.setAttribute("preencher", "preencher");
			
			
				request.getSession().setAttribute("anoJasper", anoConsulta);
				request.getSession().setAttribute("ordenacaoJasper", "");
			}
			
			request.setAttribute("zonasList", zonasList);
			

			// forward = "/consultarMesarioVoluntario.jsp"
			getServletConfig().getServletContext().getRequestDispatcher(
					"/consultarMesarioVoluntario.jsp").forward(request,
					response);

		} catch (SQLException e) {
			e.printStackTrace();
			request.setAttribute("erro", e.getMessage());
			getServletConfig().getServletContext().getRequestDispatcher(
					"/erro.jsp?2").forward(request, response);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			request.setAttribute("erro", e.getMessage());
			getServletConfig().getServletContext().getRequestDispatcher(
					"/erro.jsp?3").forward(request, response);
		}
	}
}