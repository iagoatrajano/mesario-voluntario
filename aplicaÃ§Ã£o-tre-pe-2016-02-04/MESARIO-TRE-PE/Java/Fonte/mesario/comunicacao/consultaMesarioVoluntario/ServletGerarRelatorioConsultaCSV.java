package mesario.comunicacao.consultaMesarioVoluntario;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mesario.comunicacao.Fachada;
import mesario.controle.mesario.Eleitor;
import mesario.controle.mesario.Zona;

import componente_acesso.controleusuario.usuario.Usuario;

public class ServletGerarRelatorioConsultaCSV extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public static final String SEPARADOR_COLUNAS_CSV = ","; 
	
	private static final int BUFSIZE = 4096;
	
	Fachada fachada = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletGerarRelatorioConsultaCSV() {
        super();
        
    }
    public void init() throws ServletException {
		this.fachada = Fachada.getInstance();
	}
	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int length   = 0;
		Usuario usuario = new Usuario();

		
		String nomeDoArquivo = new String("consultaMesario.csv");
		String path = getServletContext().getRealPath("/");
		
		ArrayList<Zona> zonasList = new ArrayList<Zona>();
		int zona;
		String acessoADM = (String) req.getParameter("acessoADM");
		String mensagem = null;

		// recebendo o parametro do ano escolhido
		String ano = (String) req.getParameter("ano");
		String ordenaTabela = (String) req.getParameter("ordenaTabela");
		
		if(ordenaTabela == null){
			ordenaTabela = "1";
		}
		
		try{
			Locale locale = new Locale("pt", "BR");
			Locale.setDefault(locale);
		    PrintWriter writer = new PrintWriter(path + nomeDoArquivo,"ISO-8859-1");
		    		    
		    writer.append("T�tulo Eleitoral");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Nome");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Data de Inscri��o");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Local de Vota��o");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Se��o");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Ocupa��o");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Data Nascimento");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Universit�rio");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Telefone");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("E-mail");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Endere�o");
		   
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Bairro");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    writer.append("Cep");
		    writer.append(SEPARADOR_COLUNAS_CSV);
		    
		    writer.append('\n');			

		    HttpSession session = req.getSession();

			usuario = (Usuario) session.getAttribute("usuario");

			if(usuario.getNivel().equals("2"))
			{
				zona = Integer.parseInt(req.getParameter("zona"));
				zonasList = this.fachada.consultarZonasEleitorais();
				req.setAttribute("zonasList", zonasList);
			}
			
			else
				zona = usuario.getZona().getNumero();

			// carregando os eleitores do ano respectivo
			List listaEleitores = this.fachada.consultarEleitorAno(ano, zona, ordenaTabela);
			ArrayList<Eleitor> arrayListEleitor = new ArrayList<Eleitor>(listaEleitores);
			
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yy");
			
			for (Eleitor eleitor : arrayListEleitor) {
				
			    writer.append(eleitor.getTitulo());
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    writer.append(eleitor.getNome().replaceAll(",", " "));
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    writer.append(eleitor.getDataCadastro());
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    writer.append(eleitor.getLocalVotacao().replaceAll(",", " "));
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    writer.append(eleitor.getSecaoEleitoral());
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    
			    if(eleitor.getOcupacao() == null || eleitor.getOcupacao().isEmpty()){
			    	writer.append("N�o informado");
			    }else {
			    	writer.append(eleitor.getOcupacao().replaceAll(",", " "));
			    }
			    
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    writer.append(formato.format(eleitor.getDataNascimento()));
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    
			    if(eleitor.getUniversitario() == null || eleitor.getUniversitario().isEmpty()){
			    	writer.append("N�o");
			    }else {
			    	 writer.append("Sim");
			    }
			   
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    writer.append(eleitor.getTodosTelefones().replaceAll(",", " "));
			    writer.append(SEPARADOR_COLUNAS_CSV);

			    if(eleitor.getEmail() == null || eleitor.getEmail().isEmpty()){
			    	writer.append("N�o informado");
			    }else {
				    writer.append(eleitor.getEmail().replaceAll(",", " "));
			    }
			    
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    writer.append(eleitor.getEndereco().replaceAll(",", " "));
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    writer.append(eleitor.getBairro().replaceAll(",", " "));
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    
			  
			   if(eleitor.getCep() !=null){
			     writer.append(String.valueOf(eleitor.getCep()));
			   }else{
				   writer.append("N�o informado");  
			   }
			    writer.append(SEPARADOR_COLUNAS_CSV);
			    
			    writer.append('\n');			

			}
			
			writer.flush();
		    writer.close();

		 // sets HTTP header
		    resp.addHeader("Content-Type", "application/octet-stream");
	        resp.setHeader("Content-Disposition", "attachment; filename=\"" + nomeDoArquivo + "\"");
	        resp.addHeader("Content-Transfer-Encoding", "binary");
	        
	        File file = new File(path + nomeDoArquivo);
	        resp.setContentLength((int)file.length());
	        byte[] byteBuffer = new byte[BUFSIZE];
	       
	        DataInputStream in = new DataInputStream(new FileInputStream(file));
	        OutputStream outStream = resp.getOutputStream();

	        while ((in != null) && ((length = in.read(byteBuffer)) != -1))
	        {
	            outStream.write(byteBuffer,0,length);
	        }
	        
	        in.close();
	        outStream.close();
	    
		    
		}catch (SQLException e) {
			e.printStackTrace();
			req.setAttribute("erro", e.getMessage());
			getServletConfig().getServletContext().getRequestDispatcher("/erro.jsp?2").forward(req, resp);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			req.setAttribute("erro", e.getMessage());
			getServletConfig().getServletContext().getRequestDispatcher("/erro.jsp?3").forward(req, resp);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
