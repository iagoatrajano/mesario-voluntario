package mesario.comunicacao.cadastrouniversidadeconveniada;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.mail.handlers.message_rfc822;

import mesario.comunicacao.Fachada;
import mesario.controle.composicaoEje.RepositorioComposicaoEjeBD;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.util.Conexao;
import mesario.util.FuncoesGerais;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 * Servlet implementation class ServletCadastrarUniversidadeConveniada
 */
public class ServletCadastrarUniversidadeConveniada extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Fachada fachada = null;

	public ServletCadastrarUniversidadeConveniada() {
		super();

	}

	public void init() throws ServletException {
		this.fachada = Fachada.getInstance();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		UniversidadeConveniada universidade = new UniversidadeConveniada();

		String mensagem = null;

		String cnpj = req.getParameter("cnpj");

		try {
			universidade.setRazaoSocial(req.getParameter("razaoSocial"));
			universidade.setNomeFantasia(req.getParameter("nomeFantasia"));
			universidade.setSigla(req.getParameter("sigla"));
			universidade.setCepRepresentante(FuncoesGerais
					.cepFormatarMascara(req.getParameter("cep")));
			universidade.setTipoPessoaJuridica(Integer.parseInt(req
					.getParameter("tipoPessoa")));
			universidade.setTituloRepresentante(req
					.getParameter("cargo"));
			universidade.setNomeRepresentante(req.getParameter("nomeRep"));
			universidade.setEnderecoRepresentante(req
					.getParameter("endereco"));
			universidade.setCidadeRepresentante(req.getParameter("cidade"));
			universidade.setEstadoRepresentante(req.getParameter("estado"));
			universidade.setNacionalidadeRepresentante(req
					.getParameter("nacionalidade"));
			universidade.setEstadoCivilRepresentante(req
					.getParameter("estadoCivil"));
			universidade.setRgRepresentante(req.getParameter("rg"));
			universidade.setConvenioValido("0");
			universidade.setOrgaoExpRgRepresentante(req
					.getParameter("orgaoExp"));
			universidade.setCpfRepresentante(req.getParameter("cpf"));
			universidade.setBairroRepresentante(req.getParameter("bairro"));
			universidade.setTituloRepresentante(req.getParameter("cargo"));
			universidade.setCnpj(cnpj);
			universidade.setEmail(req.getParameter("emailConvenio"));
			universidade.setTelefone(req.getParameter("phone"));
			
			if (!fachada.consultarCnpjExistente(cnpj)) {
				
				
			    fachada.cadastrarUniversidadeConveniada(universidade);
			    
			    String codObjeto = fachada.consultarCodObjetoUniversidade(universidade.getCnpj());
			    
			    universidade.setCodObjeto(codObjeto);		   
			  
			    
			    req.setAttribute("universidade", universidade);			    
			 
			    req.getSession().setAttribute("universidadeTeste", universidade);
			    
			    mensagem = "Cadastro efetuado com sucesso!"
					+ "Seu termo de conv�nio ser� gerado e deve "
					+ "ser impresso para assinatura em 3 vias,"
					+ " bem como rubrica de todas as folhas."
					+ " Favor remeter esses documentos para a "
					+ "Av. Rui Barbosa, n� 320 - Gra�as - Recife - PE - CEP: 52.011-040, "
					+ "em aten��o � Escola Judici�ria Eleitoral, "
					+ "para prosseguimento dos tr�mites legais para valida��o do conv�nio.";
			    
			    req.setAttribute("mensagem", mensagem);
			    req.setAttribute("flag", "sim");
			    getServletConfig().getServletContext().getRequestDispatcher(
			    "/cadastroUniversidade.jsp").forward(req, resp);	

		
				
			} else {
				try {
					
					RepositorioComposicaoEjeBD composicaoEjeBD = new RepositorioComposicaoEjeBD();
					String[] dadosEje = composicaoEjeBD.consultarDadosEje();
					String telefoneEje = dadosEje[0];
					String emailEje = dadosEje[1];
					mensagem = "A institui��o j� est� cadastrada. "
						+ "Caso necessite alterar os seus dados cadastrais, "
						+ "entre em contato com a Escola Judici�ria Eleitoral do TRE-PE, "
						+ "atrav�s dos telefones " + telefoneEje + " "
						+ "ou do e-mail " + emailEje;
				
				req.setAttribute("mensagem", mensagem);
				req.setAttribute("universidade", universidade);
				req.setAttribute("flag", "nao");
				getServletConfig().getServletContext().getRequestDispatcher(
						"/cadastroUniversidade.jsp").forward(req, resp);
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();		
		}
	}

}
