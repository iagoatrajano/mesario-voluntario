package mesario.comunicacao.cadastromesario;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mesario.comunicacao.Fachada;
import mesario.controle.mesario.Eleitor;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.excecoes.RepositorioException;

public class ServletConsultarDadosEleitor extends HttpServlet {

	Fachada fachada = null;

	public void init() throws ServletException {
		this.fachada = Fachada.getInstance();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		
		String titulo = req.getParameter("titulo");
		
		int tamanho = titulo.length();
		if(tamanho < 12){
			   while (tamanho != 12){
			   		titulo = "0" + titulo;
			   		tamanho ++;
			   }
		 }
		
		Eleitor eleitor = null;
		List listaUniversidade = null;
		List listaOcupacao = null; 
		String mensagem = null;

		try {
			eleitor = this.fachada.consultarEleitor(titulo);			
			listaUniversidade = this.fachada.listarUniversidadeConveniada();
			
			
			if(!listaUniversidade.isEmpty()){
				
				req.setAttribute("listaUniversidade", listaUniversidade);
			}
			
			
			
			if(eleitor != null){
				if(!fachada.consultarEleitorCadastrado(titulo, eleitor.getNumZona())){
					listaOcupacao = fachada.consultarOcupacao();
					
					req.setAttribute("LISTAOCUPACAO", listaOcupacao);
					req.setAttribute("ELEITOR", eleitor);
					
					req.setAttribute("mensagem", mensagem);
					
					getServletConfig().getServletContext().getRequestDispatcher(
						"/cadastroMesario.jsp").forward(req, res);
				}else{
					mensagem = "Prezado eleitor, constatamos que o seu cadastro j� foi efetivado este ano." +
							"Caso deseje alterar os seus dados, entre em contato com o seu Cart�rio Eleitoral.";
					
					req.setAttribute("mensagem", mensagem);
					
					getServletConfig().getServletContext().getRequestDispatcher(
						"/informaTitulo.jsp").forward(req, res);
				}
			}else{
				mensagem = "O t�tulo informado n�o existe no Cadastro Eleitoral de Pernambuco.  " +
						   "Por favor, entre em contato com o seu Cart�rio Eleitoral.";
				
				req.setAttribute("mensagem", mensagem);
				
				getServletConfig().getServletContext().getRequestDispatcher(
					"/informaTitulo.jsp").forward(req, res);
			}
			
			
		} catch (RepositorioException e) {
			e.printStackTrace();
			req.setAttribute("erro", e.getMessage());
			getServletConfig().getServletContext().getRequestDispatcher(
					"/erro.jsp?1").forward(req, res);
		} catch (SQLException e) {
			e.printStackTrace();
			req.setAttribute("erro", e.getMessage());
			getServletConfig().getServletContext().getRequestDispatcher(
					"/erro.jsp?2").forward(req, res);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			req.setAttribute("erro", e.getMessage());
			getServletConfig().getServletContext().getRequestDispatcher(
					"/erro.jsp?3").forward(req, res);
		} catch (ParseException e) {
			e.printStackTrace();
			req.setAttribute("erro", e.getMessage());
			getServletConfig().getServletContext().getRequestDispatcher(
					"/erro.jsp?4").forward(req, res);
		}
	}

}
