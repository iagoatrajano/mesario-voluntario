package mesario.controle;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mesario.controle.universidadeconveniada.CadastroUniversidadeConveniada;
import mesario.controle.universidadeconveniada.UniversidadeConveniada;

public class ControleCadastrarUniversidadeConveniada {
	
	private CadastroUniversidadeConveniada cadastroUniversidadeConveniada = new CadastroUniversidadeConveniada();

	public List listarUniversidadeConveniada() throws SQLException, ClassNotFoundException{
		return this.cadastroUniversidadeConveniada.listarUniversidadeConveniada();
	}
	
	public void cadastrarUniversidadeConveniada(UniversidadeConveniada universidade) throws ClassNotFoundException,SQLException {
		this.cadastroUniversidadeConveniada.cadastrarUniversidadeConveniada(universidade);
	}
	public boolean consultarCnpjExistente(String cnpj) throws ClassNotFoundException, SQLException{
		return this.cadastroUniversidadeConveniada.consultarCnpjExistente(cnpj);
	}
	
	public ArrayList <UniversidadeConveniada> listarTodasUniversidades() throws ClassNotFoundException{
		return this.cadastroUniversidadeConveniada.listarTodasUniversidades();
	}
	
	public UniversidadeConveniada consultarUniversidade(int codObjeto) throws ClassNotFoundException{
		return this.cadastroUniversidadeConveniada.consultarUniversidade(codObjeto);
	}
	
	public void editarUniversidade (UniversidadeConveniada universidade) throws ClassNotFoundException{
		this.cadastroUniversidadeConveniada.editarUniversidade(universidade);
	}
	
	public void editarDataFinalUniversidade(int codObjeto, Date dataFinal) throws ClassNotFoundException{
		this.cadastroUniversidadeConveniada.editarDataFinalUniversidade(codObjeto, dataFinal);
	}
	public String consultarCodObjetoUniversidade(String cnpj) throws ClassNotFoundException, SQLException{
		return this.cadastroUniversidadeConveniada.consultarCodObjetoUniversidade(cnpj);
	}
	
	public Date getDataFinal() throws ClassNotFoundException{
		return this.cadastroUniversidadeConveniada.getDataFinal();
	}
	
	public boolean existeNumero (String numeroAnoConvenio, int  codObjeto) throws SQLException, ClassNotFoundException{
		return this.cadastroUniversidadeConveniada.existeNumero(numeroAnoConvenio,  codObjeto);
	}

	public List listasUniversidadesConveniadas() throws SQLException,ClassNotFoundException{
			return this.cadastroUniversidadeConveniada.listasUniversidadesConveniadas();
		}
}
	

