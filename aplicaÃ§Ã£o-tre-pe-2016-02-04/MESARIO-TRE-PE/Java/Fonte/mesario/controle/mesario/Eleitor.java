package mesario.controle.mesario;

import java.util.Date;

public class Eleitor {

	private String titulo;

	private String nome;

	private Date dataNascimento;

	private String nomeMae;

	private String numZona;

	private String numMunicZona;

	private String municipioZona;

	private String numLocalVotacao;

	private String localVotacao;

	private String secaoEleitoral;

	private String dataCadastro;

	private String endereco;

	private String bairro;

	private String cidade;

	private Integer cep;

	private Integer dddFone;

	private String telefone;

	private Integer dddFoneTrabalho;

	private String telefoneTrabalho;

	private Integer dddFoneOpcional;

	private String foneOpcional;

	private String email;

	private int numOcupacao;

	private String ocupacao;

	private int grauInstrucao;

	private int servidorPublico;

	private int foiMesario;
	
	private Integer ramal;
	
	private String universitario;
	
	private String codObjetoUniversidadeConveniada;

	public Eleitor() {

	}

	public Eleitor(String titulo, String nome, Date dataNascimento,
			String nomeMae, String numZona, String numMunicZona,
			String municipioZona, String numLocalVotacao, String localVotacao,
			String secaoEleitoral, String endereco, String bairro, String cidade,
			Integer cep, Integer dddFone, String telefone, Integer dddFoneTrabalho,
			String telefoneTrabalho, Integer dddFoneOpcional, String foneOpcional,
			String email, int numOcupacao, String ocupacao, int grauInstrucao,
			int servidorPublico, int foiMesario, Integer ramal,String codObjetoUniversidadeConveniada,String universitario) {
		super();
		this.titulo = titulo;
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.nomeMae = nomeMae;
		this.numZona = numZona;
		this.numMunicZona = numMunicZona;
		this.municipioZona = municipioZona;
		this.numLocalVotacao = numLocalVotacao;
		this.localVotacao = localVotacao;
		this.secaoEleitoral = secaoEleitoral;
		this.endereco = endereco;
		this.bairro = bairro;
		this.cidade = cidade;
		this.cep = cep;
		this.dddFone = dddFone;
		this.telefone = telefone;
		this.dddFoneTrabalho = dddFoneTrabalho;
		this.telefoneTrabalho = telefoneTrabalho;
		this.dddFoneOpcional = dddFoneOpcional;
		this.foneOpcional = foneOpcional;
		this.email = email;
		this.numOcupacao = numOcupacao;
		this.ocupacao = ocupacao;
		this.grauInstrucao = grauInstrucao;
		this.servidorPublico = servidorPublico;
		this.foiMesario = foiMesario;
		this.ramal = ramal;
		this.codObjetoUniversidadeConveniada = codObjetoUniversidadeConveniada;
		this.universitario = universitario;
	}

	public String getUniversitario() {
		return universitario;
	}

	public void setUniversitario(String universitario) {
		this.universitario = universitario;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Integer getDddFoneOpcional() {
		return dddFoneOpcional;
	}

	public void setDddFoneOpcional(Integer dddFoneOpcional) {
		this.dddFoneOpcional = dddFoneOpcional;
	}

	public String getFoneOpcional() {
		return foneOpcional;
	}

	public void setFoneOpcional(String foneOpcional) {
		this.foneOpcional = foneOpcional;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Integer getCep() {
		return cep;
	}

	public void setCep(Integer cep) {
		this.cep = cep;
	}

	public Integer getDddFone() {
		return dddFone;
	}

	public void setDddFone(Integer dddFone) {
		this.dddFone = dddFone;
	}

	public Integer getDddFoneTrabalho() {
		return dddFoneTrabalho;
	}

	public void setDddFoneTrabalho(Integer dddFoneTrabalho) {
		this.dddFoneTrabalho = dddFoneTrabalho;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public int getFoiMesario() {
		return foiMesario;
	}

	public void setFoiMesario(int foiMesario) {
		this.foiMesario = foiMesario;
	}

	public int getGrauInstrucao() {
		return grauInstrucao;
	}

	public void setGrauInstrucao(int grauInstrucao) {
		this.grauInstrucao = grauInstrucao;
	}

	public String getLocalVotacao() {
		return localVotacao;
	}

	public void setLocalVotacao(String localVotacao) {
		this.localVotacao = localVotacao;
	}

	public String getMunicipioZona() {
		return municipioZona;
	}

	public void setMunicipioZona(String municipioZona) {
		this.municipioZona = municipioZona;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getNumLocalVotacao() {
		return numLocalVotacao;
	}

	public void setNumLocalVotacao(String numLocalVotacao) {
		this.numLocalVotacao = numLocalVotacao;
	}

	public String getNumMunicZona() {
		return numMunicZona;
	}

	public void setNumMunicZona(String numMunicZona) {
		this.numMunicZona = numMunicZona;
	}

	public int getNumOcupacao() {
		return numOcupacao;
	}

	public void setNumOcupacao(int numOcupacao) {
		this.numOcupacao = numOcupacao;
	}

	public String getNumZona() {
		return numZona;
	}

	public void setNumZona(String numZona) {
		this.numZona = numZona;
	}

	public String getOcupacao() {
		return ocupacao;
	}

	public void setOcupacao(String ocupacao) {
		this.ocupacao = ocupacao;
	}

	public String getSecaoEleitoral() {
		return secaoEleitoral;
	}

	public void setSecaoEleitoral(String secaoEleitoral) {
		this.secaoEleitoral = secaoEleitoral;
	}

	public int getServidorPublico() {
		return servidorPublico;
	}

	public void setServidorPublico(int servidorPublico) {
		this.servidorPublico = servidorPublico;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getTelefoneTrabalho() {
		return telefoneTrabalho;
	}

	public void setTelefoneTrabalho(String telefoneTrabalho) {
		this.telefoneTrabalho = telefoneTrabalho;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Integer getRamal() {
		return ramal;
	}

	public void setRamal(Integer ramal) {
		this.ramal = ramal;
	}

	public String getCodObjetoUniversidadeConveniada() {
		return codObjetoUniversidadeConveniada;
	}

	public void setCodObjetoUniversidadeConveniada(
			String codObjetoUniversidadeConveniada) {
		this.codObjetoUniversidadeConveniada = codObjetoUniversidadeConveniada;
	}
	
	public String getTodosTelefones(){
		String resultado = getTelefone();
		if(getTelefoneTrabalho() != null && !getTelefoneTrabalho().isEmpty()){
			resultado += " / " + getTelefoneTrabalho();
		}
		if(getFoneOpcional() != null && !getFoneOpcional().isEmpty()){
			resultado += " / " + getFoneOpcional();
		}
		return resultado;
	}
}
