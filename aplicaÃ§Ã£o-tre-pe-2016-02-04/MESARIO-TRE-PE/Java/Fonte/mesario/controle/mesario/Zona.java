package mesario.controle.mesario;

public class Zona {
	
	private String numZona;
	
	private String polo;
	
	private String municipio;
	
	public Zona() {

	}
	
	

	public String getNumZona() {
		return numZona;
	}

	public void setNumZona(String numZona) {
		this.numZona = numZona;
	}

	public String getPolo() {
		return polo;
	}

	public void setPolo(String polo) {
		this.polo = polo;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	
	

}
