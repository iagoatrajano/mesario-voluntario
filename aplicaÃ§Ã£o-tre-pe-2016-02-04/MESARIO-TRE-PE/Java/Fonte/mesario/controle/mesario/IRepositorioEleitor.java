package mesario.controle.mesario;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import mesario.controle.universidadeconveniada.UniversidadeConveniada;
import mesario.excecoes.RepositorioException;

public interface IRepositorioEleitor {

	public Eleitor consultarEleitor(String titulo) throws RepositorioException, SQLException, ClassNotFoundException, ParseException;
	
	public List consultarOcupacao() throws RepositorioException, SQLException, ClassNotFoundException;
	
	public String consultarOcupacao(int numero) throws RepositorioException, SQLException, ClassNotFoundException;
	
	public int cadastraEleitor(Eleitor eleitor) throws RepositorioException, SQLException, ClassNotFoundException;
	
	public boolean consultarEleitorCadastrado(String titulo, String numZona) throws SQLException, ClassNotFoundException;
	
	public List consultarDatasInscricao(int zona)throws SQLException,ClassNotFoundException;
	
	public List consultarEleitorAno(String ano,int zona,String tipoOrdenacao)throws SQLException,ClassNotFoundException,ParseException;
	
	public ArrayList<UniversidadeConveniada> consultarEstatisticasPorEntidade () throws ClassNotFoundException;
	
	public LinkedHashMap <String, Integer> consultarEstatisticasPorAno () throws ClassNotFoundException;

	//public List consultarEstatisticasPorPeriodo(java.util.Date dataInicialPeriodo, java.util.Date dataFinalPeriodo) throws ClassNotFoundException;
	public int consultarEstatisticasPorPeriodo(String dataInicialPeriodo, String dataFinalPeriodo) throws ClassNotFoundException;

	public ArrayList<Zona> consultarZonasEleitorais() throws SQLException, ClassNotFoundException;
}
