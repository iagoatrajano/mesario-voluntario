package mesario.controle.composicaoEje;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import mesario.excecoes.RepositorioException;


public class CadastroComposicaoEje {
	
	private IRepositorioComposicaoEje repositorioEje =  new RepositorioComposicaoEjeBD();
	
	public List consultarComposicaoEje() throws RepositorioException, SQLException,
	ClassNotFoundException, ParseException {
	return this.repositorioEje.consultarComposicaoEje();
	}
	
	public void editarComposicaoEje(ComposicaoEje composicao) throws ClassNotFoundException{
		this.repositorioEje.editarComposicaoEje(composicao);
	}

	public boolean existeComposicaoEje(String id) throws ClassNotFoundException{
		return this.repositorioEje.existeComposicaoEje(id);
	}
	
	public void inserirComposicaoEje(ComposicaoEje composicao) throws ClassNotFoundException{
		this.repositorioEje.inserirComposicaoEje(composicao);
	}
	
}
