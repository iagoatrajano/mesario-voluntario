package mesario.controle.composicaoEje;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import mesario.controle.mesario.IRepositorioEleitor;
import mesario.util.Conexao;

public class RepositorioComposicaoEjeBD implements IRepositorioComposicaoEje{

	public List consultarComposicaoEje() throws SQLException,ClassNotFoundException, ParseException {
		PreparedStatement stmt = null;
		String sql;
		ResultSet rs = null;
		Connection con = null;
		List<ComposicaoEje> listaComposicaoEje = null;
		

		Conexao conexao = new Conexao();

		sql = "SELECT * from composicao_eje order by tratamento";

		try {

			con = (Connection) conexao.abrirConexao();

			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();

			listaComposicaoEje = new ArrayList<ComposicaoEje>();
			while (rs.next()) {
				ComposicaoEje composicao = new ComposicaoEje();

				composicao.setTratamento(rs.getString("tratamento"));
				composicao.setNome(rs.getString("nome"));
				composicao.setNacionalidade(rs.getString("nacionalidade"));
				composicao.setEstadoCivil(rs.getString("estado_civil"));
				composicao.setRg(rs.getString("rg"));
				composicao.setOrgaoExp(rs.getString("orgao_expedidor_rg"));
				composicao.setCpf(rs.getString("cpf"));
				composicao.setEndereco(rs.getString("endereco"));
				composicao.setBairro(rs.getString("bairro"));
				composicao.setCep(rs.getInt("cep"));
				composicao.setCidade(rs.getString("cidade"));
				composicao.setCargo(rs.getString("cargo"));
				composicao.setTipo(rs.getString("tipo"));
				composicao.setEstado(rs.getString("estado"));
				composicao.setCodObjeto(rs.getString("cod_objeto"));
				
				listaComposicaoEje.add(composicao);

				
			}

		} catch (SQLException e) {
			throw e;
		} finally {
			conexao.fecharResultSet(rs);
			conexao.fecharStatement(stmt);
			conexao.fecharConexao(con);
		}

		return listaComposicaoEje;
	}
	
	
	public String[] consultarDadosEje()throws SQLException,ClassNotFoundException, ParseException{
		PreparedStatement stmt = null;
		String sql;
		ResultSet rs = null;
		Connection con = null;
		
		Conexao conexao = new Conexao();
		String[] dadosEje = new String[2];

		
		sql = "SELECT telefone_eje, email_eje from configuracao order by telefone_eje";
		try {
			con = (Connection) conexao.abrirConexao();

			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();
			if (rs.next()) {
				dadosEje[0] = rs.getString("telefone_eje");
				dadosEje[1] = rs.getString("email_eje");
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			conexao.fecharResultSet(rs);
			conexao.fecharStatement(stmt);
			conexao.fecharConexao(con);
		}
		
		return dadosEje;
	}
	
	
	public void editarComposicaoEje(ComposicaoEje composicao) throws ClassNotFoundException{
		PreparedStatement stmt = null;
		String sql;
		
		Connection con = null;
		
		
		Conexao conexao = new Conexao();
		sql = "UPDATE composicao_eje set " + 
			"tratamento = ?, " +
			"nome = ?, " +
			"nacionalidade = ?, " + 
			"estado_civil = ?, " +
			"rg = ?, " + 
			"orgao_expedidor_rg = ?, " + 
			"cpf = ?, " + 
			"endereco = ?, " + 
			"bairro = ?, " + 
			"cep = ?, " + 
			"cidade = ?, " + 
			"estado = ? ," + 
			"cargo = ? " + 
			"WHERE cod_objeto = ? "; 
			
		try{
			con = (Connection) conexao.abrirConexao();
			
			stmt	= con.prepareStatement(sql);
			stmt.setString(1, composicao.getTratamento());
			stmt.setString(2, composicao.getNome());
			stmt.setString(3, composicao.getNacionalidade());
			stmt.setString(4, composicao.getEstadoCivil());
			stmt.setString(5, composicao.getRg());
			stmt.setString(6, composicao.getOrgaoExp());
			stmt.setString(7, composicao.getCpf());
			stmt.setString(8, composicao.getEndereco());
			stmt.setString(9, composicao.getBairro());
			stmt.setInt(10, composicao.getCep());
			stmt.setString(11, composicao.getCidade());
			stmt.setString(12, composicao.getEstado());
			stmt.setString(13, composicao.getCargo());
			stmt.setString(14, composicao.getCodObjeto());
			

			stmt.executeUpdate();
	
			stmt.close();
		}catch (Exception e){
			e.printStackTrace();
			
		}finally {
			
			conexao.fecharStatement(stmt);
			conexao.fecharConexao(con);
		}
		
	}	
		
	public boolean existeComposicaoEje(String id) throws ClassNotFoundException{
		PreparedStatement stmt = null;
		String sql;
		boolean existe = false;
		Connection con = null;
		ResultSet rs = null;
		
		Conexao conexao = new Conexao();
		try{
			con = (Connection) conexao.abrirConexao();
			sql= "select count(*) as total from composicao_eje "+
				  " where tipo = "+id;
			stmt	= con.prepareStatement(sql);
			rs = stmt.executeQuery();
			if (rs.next()){
				if(id.equals("1")){
					if (rs.getInt("total") == 1){
						existe = true;
					}				
				}
				else if(id.equals("2")){					
					if(rs.getInt("total") == 2){
						existe = true;
					}
				}
			}
			
			
		}catch (Exception e){
			e.printStackTrace();
			
		}finally {
			
			conexao.fecharStatement(stmt);
			conexao.fecharConexao(con);
		}
		return existe;
	}
	public void inserirComposicaoEje(ComposicaoEje composicao) throws ClassNotFoundException{
		PreparedStatement stmt = null;
		String sql;
		
		Connection con = null;
		
		
		Conexao conexao = new Conexao();
		sql = "insert into composicao_eje  " + 
			"(tratamento, " +
			"nome, " +
			"nacionalidade, " + 
			"estado_civil, " +
			"rg, " + 
			"orgao_expedidor_rg, " + 
			"cpf, " + 
			"endereco, " + 
			"bairro, " + 
			"cep, " + 
			"cidade, " + 
			"estado," + 
			"cargo, tipo ) " +
			"values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			
			
		try{
			con = (Connection) conexao.abrirConexao();
			
			stmt	= con.prepareStatement(sql);
			stmt.setString(1, composicao.getTratamento());
			stmt.setString(2, composicao.getNome());
			stmt.setString(3, composicao.getNacionalidade());
			stmt.setString(4, composicao.getEstadoCivil());
			stmt.setString(5, composicao.getRg());
			stmt.setString(6, composicao.getOrgaoExp());
			stmt.setString(7, composicao.getCpf());
			stmt.setString(8, composicao.getEndereco());
			stmt.setString(9, composicao.getBairro());
			stmt.setInt(10, composicao.getCep());
			stmt.setString(11, composicao.getCidade());
			stmt.setString(12, composicao.getEstado());
			stmt.setString(13, composicao.getCargo());
			stmt.setString(14, composicao.getTipo());
			
			

			stmt.execute();
	
			stmt.close();
		}catch (Exception e){
			e.printStackTrace();
			
		}finally {
			
			conexao.fecharStatement(stmt);
			conexao.fecharConexao(con);
		}
		
	}	
	
	

}
