package mesario.controle.universidadeconveniada;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mesario.excecoes.ExisteAnoNumeroException;

public class CadastroUniversidadeConveniada {
	
private IRepositorioUniversidadeConveniada repositorioUniversidadeConveniada = new RepositorioUniversidadeConveniadaBDR();
	
	public List listarUniversidadeConveniada() throws SQLException, ClassNotFoundException{
		return this.repositorioUniversidadeConveniada.listasUniversidadesConveniadas();
	}
	
	public void cadastrarUniversidadeConveniada(UniversidadeConveniada universidade) throws ClassNotFoundException,SQLException{
		this.repositorioUniversidadeConveniada.cadastrarUniversidadeConveniada(universidade);		
	}
	
	public boolean consultarCnpjExistente(String cnpj) throws ClassNotFoundException, SQLException{
		return this.repositorioUniversidadeConveniada.consultarCnpjExistente(cnpj);
	}
	
	public ArrayList <UniversidadeConveniada> listarTodasUniversidades() throws ClassNotFoundException{
		return this.repositorioUniversidadeConveniada.listarTodasUniversidades();
	}
	
	public UniversidadeConveniada consultarUniversidade(int codObjeto) throws ClassNotFoundException{
		return this.repositorioUniversidadeConveniada.consultarUniversidade(codObjeto);
	}
	
	public void editarNumeroAnoUniversidade(UniversidadeConveniada universidade, int codObjeto) 
		throws ClassNotFoundException, SQLException, ExisteAnoNumeroException{
		if(!this.repositorioUniversidadeConveniada.existeNumero(universidade.getNumero() + universidade.getAno(),  codObjeto)){
			this.repositorioUniversidadeConveniada.editarUniversidade(universidade);
		}else{
			throw new ExisteAnoNumeroException();
		}
	}
	
	public void editarUniversidade (UniversidadeConveniada universidade) throws ClassNotFoundException{
		this.repositorioUniversidadeConveniada.editarUniversidade(universidade);
	}
	
	public void editarDataFinalUniversidade(int codObjeto, Date dataFinal) throws ClassNotFoundException{
		this.repositorioUniversidadeConveniada.editarDataFinalUniversidade(codObjeto, dataFinal);
	}
	public String consultarCodObjetoUniversidade(String cnpj) throws ClassNotFoundException, SQLException{
		return this.repositorioUniversidadeConveniada.consultarCodObjetoUniversidade(cnpj);
	}

	public Date getDataFinal() throws ClassNotFoundException {
		
		return this.repositorioUniversidadeConveniada.getDataFinal();
	}

	public boolean existeNumero (String numeroAnoConvenio, int  codObjeto) throws SQLException, ClassNotFoundException{
		return this.repositorioUniversidadeConveniada.existeNumero(numeroAnoConvenio,  codObjeto);
	}

	public List listasUniversidadesConveniadas() throws SQLException, ClassNotFoundException {
		return this.repositorioUniversidadeConveniada.listasUniversidadesConveniadas();
	}
	
}
