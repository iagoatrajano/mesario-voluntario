/*
 * Created on 10/05/2006
 *
 */
package mesario.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexao {
	  
	Constantes constantes = null;
  
  /*
   * Driver do BD.
   */
  
  //private String driver = "oracle.jdbc.driver.OracleDriver";
  
  /*
   * URL para acessar o BD.
   */
  //private static String url = "jdbc:oracle:thin:@pebd02:1521:tre";

  /*
   * Usuario para acessar o BD.
   */
  //private static String user = "simpla";

  /*
   * Senha para acessar o BD.
   */
 // private static String password = "simpla$banco";

  /**
   * Construtor para carregar driver jdbc.
   */
  public Conexao() throws ClassNotFoundException {
	constantes = new Constantes();
	Class.forName(constantes.get("driver"));
	//Class.forName(driver);
  }

  /**
   * Abre uma conexao com o BD.
   */
  public Connection abrirConexao() throws SQLException {
	Connection conn = null;
	
	conn = DriverManager.getConnection(constantes.get("url"), 
									   constantes.get("user"), 
									   constantes.get("password"));
	//conn = DriverManager.getConnection(url,user,password);
	conn.setAutoCommit(false);
	return conn;
  }
  
 /**
 * Fecha um Statement.
 */
 public void fecharStatement(Statement stmt) {
	try {

		//s� fecha o statement se ele for diferente de null
		if (stmt != null) {
			stmt.close();
		}

	} catch (SQLException e) {
		//como o erro � um caso raro, apenas mosta a exce��o no console
		//outra op��o � gerar um log do sistema
		e.printStackTrace();
	}
 }

 /**
 * Fecha uma conexao com o banco.
 */
 public void fecharConexao(Connection conexao) {
	try {

		//s� fecha a conexao se ela for diferente de null
		if (conexao != null) {
			conexao.close();
		}

	} catch (SQLException e) {
		//como o erro � um caso raro, apenas mosta a exce��o no console
		//outra op��o � gerar um log do sistema
		e.printStackTrace();
	}
}

/**
* Fecha uma conexao com o banco.
*/
public void fecharResultSet(ResultSet rs) {
	try {

		//s� fecha a conexao se ela for diferente de null
		if (rs != null) {
			rs.close();
		}

	} catch (SQLException e) {
		//como o erro � um caso raro, apenas mosta a exce��o no console
		//outra op��o � gerar um log do sistema
		e.printStackTrace();
	}
}

}