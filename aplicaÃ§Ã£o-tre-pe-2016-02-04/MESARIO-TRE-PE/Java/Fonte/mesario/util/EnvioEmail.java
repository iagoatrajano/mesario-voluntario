package mesario.util;

import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EnvioEmail
{
  private static Properties properties;
  private static Session    session;

  // -------------------------------------------------------------------------------------------------------------------
  static
  {
    properties = new Properties();
    properties.put("mail.smtp.host", "dexter.tse.jus.br");
    session = Session.getDefaultInstance(properties, null);
  }

  // -------------------------------------------------------------------------------------------------------------------
  public static void enviarEmailHTML(String emailDestinatario, String emailRemetente, String assunto, String conteudo) {
      
	  try {
        
		MimeMessage msg = new MimeMessage(session);

        // Defini��o de quem est� enviando o email
        msg.setFrom(new InternetAddress(emailRemetente));

        // define o(s) destinat�rio(s) 
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailDestinatario, false));

        // defini��o do assunto do email
        msg.setSubject(assunto, "ISO-8859-1");
        msg.setHeader("X-Mailer", "TSE-SGRH");
        msg.setContent(conteudo, "text/html; charset=ISO-8859-1");
        msg.setDataHandler(new DataHandler(conteudo, "text/html; charset=ISO-8859-1"));
        msg.setSentDate(new Date());

        Transport transport = session.getTransport("smtp");
        Transport.send(msg);
        transport.close();
      
	  } catch (MessagingException e) {
        
      }
    
  }
  
  public static void enviarEmail(String emailDestinatario, String emailRemetente, String assunto, String conteudo) {
      
	  try {
        
		MimeMessage msg = new MimeMessage(session);

        // Defini��o de quem est� enviando o email
        msg.setFrom(new InternetAddress(emailRemetente));

        // define o(s) destinat�rio(s) 
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailDestinatario, false));

        // defini��o do assunto do email
        msg.setSubject(assunto, "ISO-8859-1");
        msg.setHeader("X-Mailer", "TSE-SGRH");
        msg.setContent(conteudo, "text/html; charset=ISO-8859-1");
        msg.setDataHandler(new DataHandler(conteudo, "text/html; charset=ISO-8859-1"));
        msg.setSentDate(new Date());

        Transport transport = session.getTransport("smtp");
        Transport.send(msg);
        transport.close();
      
	  } catch (MessagingException e) {
        
      }
    
  }

}
