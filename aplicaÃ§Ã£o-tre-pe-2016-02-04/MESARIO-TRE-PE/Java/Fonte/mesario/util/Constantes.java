package mesario.util;

import java.util.HashMap;
import java.util.Properties;

public class Constantes {
	
	
	   private HashMap hashConstantes = new HashMap();
	   private Properties properties = new Properties();

	   public Constantes(){
	   	
			try
			{
				this.properties.load( this.getClass( ).getResourceAsStream( "../conf/constantes.properties" ) );
			}
			catch ( Exception e )
			{
				e.getMessage();
			}

			this.hashConstantes.put("driver", properties.getProperty("driver"));
			this.hashConstantes.put("url", properties.getProperty("url"));
			this.hashConstantes.put("user", properties.getProperty("user"));
			this.hashConstantes.put("password", properties.getProperty("password"));
//			this.hashConstantes.put("driver", "oracle.jdbc.driver.OracleDriver");
//			this.hashConstantes.put("url", "jdbc:oracle:thin:@pebd02:1521:tre");
//			this.hashConstantes.put("user", "sigweb");
//			this.hashConstantes.put("password", "sigweb$banco");

	   }
	   
		public String get(String nome) {

			return (String) hashConstantes.get(nome);
		}
	
}
