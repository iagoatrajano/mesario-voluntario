package mesario.excecoes;

public class ExisteAnoNumeroException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExisteAnoNumeroException(){
		super("O n�mero de conv�nio j� existe nesse ano.");
	}
}
