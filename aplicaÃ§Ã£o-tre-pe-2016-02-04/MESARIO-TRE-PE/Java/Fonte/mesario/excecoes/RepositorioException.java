/*
 * Created on 10/05/2006
 *
*/
package mesario.excecoes;

/**
 * @author Marcela Bastos
 * @author Liliane Barros
 *  
*/

import java.io.PrintWriter;
import java.io.PrintStream;

public class RepositorioException extends Exception {

		//excecao encapsulada
		private Exception excecao;

		public RepositorioException(String msg, Exception e) {
			super(msg);
			this.excecao = e;
		}

		public RepositorioException(String msg) {
			super(msg);
		}

		public RepositorioException(Exception e) {
			this.excecao = e;
		}

		public Exception getException() {
			return excecao;
		}

		public void printStackTrace() {
			System.out.println("Imprimindo Exce��o Aninhada----");
			super.printStackTrace();
			if(excecao != null) {
				excecao.printStackTrace();
			}
		}

		public void printStackTrace(PrintWriter pw) {
			System.out.println("Imprimindo Exce��o Aninhada----");
			super.printStackTrace(pw);
			if(excecao != null) {
				excecao.printStackTrace(pw);
			}
		}

		public void printStackTrace(PrintStream ps) {
			System.out.println("Imprimindo Exce��o Aninhada----");
			super.printStackTrace(ps);
			if(excecao != null) {
				excecao.printStackTrace(ps);
			}
		}
}

