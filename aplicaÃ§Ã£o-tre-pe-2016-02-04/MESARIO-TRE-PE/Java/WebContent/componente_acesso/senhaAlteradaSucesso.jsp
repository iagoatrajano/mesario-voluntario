<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String baseServer = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/"; 
%>
<script type="text/javascript">
<!--
function voltarLogin(  )
{
	location.href="componente_acesso/login.jsp";
}
//-->
</script>
<c:set value="<%=baseServer%>" var="baseServer"/>
<title>Sistema de Mes�rio Volunt�rio do TRE-PE - Senha Alterada com sucesso</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="../../layout_aplicacoes/css/estilo_aplicacoes.css" rel="stylesheet" type="text/css" />
<script src="js/javascript.js" type="text/javascript" charset="iso-8859-1"></script>

<!--[if lte IE 6]> 
      <script src="/layout_aplicacoes/js/DD_belatedPNG_0.0.7a.js" type="text/javascript" charset="iso-8859-1"></script>
	  <script type="text/javascript">
	  DD_belatedPNG.fix('#topo, #caixa_assinatura, img');
	  </script>
	<![endif]-->

</head>

<body>
<div id="tudo">

<!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
	<c:import url="${baseServer}layout_aplicacoes/includes/include_topo.html"/>
<!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

   <div id="login" class="mesario_login">
	   <h1>Sistema de Mes�rio Volunt�rio do TRE-PE</h1>

      
	  <h2>Controle de Acesso</h2>
      <p class="msg_ok">A senha foi <strong>alterada com sucesso!</strong></p>
	  <p>Reinicie a aplica��o.</p>

	  <input type="button" id="volta_login" title="Volte a tela de Login" class="botoes_form" value="login" onclick="javascript:voltarLogin()"/>
   </div>	
	
<!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->	
	<c:import url="${baseServer}layout_aplicacoes/includes/include_rodape.html"/>
<!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->

</div> <!-- fim da div#tudo -->	
</body>
</html>
