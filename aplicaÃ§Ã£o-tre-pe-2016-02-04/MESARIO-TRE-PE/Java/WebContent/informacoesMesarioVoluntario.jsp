<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%
String path = request.getContextPath();
String baseServer = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<% 
	
	String titulo = (String) request.getParameter("titulo"); //vem do JSP
	
	String nome = (String) request.getParameter("nome"); //vem do JSP

	String dddFonePrincipal = (String) request.getParameter("dddFone"); //vem do JSP
	String fonePrincipal    = (String) request.getParameter("telefone"); //vem do JSP
	String dddFoneOpcional  = (String) request.getParameter("dddFoneOpcional"); //vem do JSP
	String foneOpcional     = (String) request.getParameter("foneOpcional"); //vem do JSP
	String telefone = "";
	if (!fonePrincipal.trim().equals("")) {
		if (!dddFonePrincipal.trim().equals("") & !dddFonePrincipal.trim().equals("0")){
			telefone = "("+dddFonePrincipal+") "+fonePrincipal;	
		}else{
		    telefone = fonePrincipal;	
		}
	}
	if (!foneOpcional.trim().equals("")) {
		if (!dddFoneOpcional.trim().equals("") & !dddFoneOpcional.trim().equals("0")){
		    telefone = telefone + " / ("+dddFoneOpcional+") "+foneOpcional;
		} else {
			telefone = telefone + " / "+foneOpcional;
		}
	}

	String email = (String) request.getParameter("email"); //vem do JSP
	if (email.equals("")) {
		email = "N�o Informado";
	}
	
	String endereco = (String) request.getParameter("endereco"); //vem do JSP
	String bairro   = (String) request.getParameter("bairro"); //vem do JSP
	String cidade   = (String) request.getParameter("cidade"); //vem do JSP
	String cep      = (String) request.getParameter("cep"); //vem do JSP
	String enderecoCompleto = "";
	if ( (cep.equals("")) || (cep.trim().equals("0")) ) {
		enderecoCompleto = endereco + ", " + bairro + ", " + cidade;
	} else {
		enderecoCompleto = endereco + ", " + bairro + ", " + cidade + ", " + cep;
	}
	
%>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tribunal Regional Eleitoral de Pernambuco</title>
	<script type="text/javascript" src="/layout_aplicacoes/js/jquery.min.js" charset="iso-8859-1"></script>
	<script type="text/javascript" src="/layout_aplicacoes/js/seginf_aplicacoes.js"></script>
	<link href="/layout_aplicacoes/css/estilo_aplicacoes.css" rel="stylesheet" type="text/css" />
	<link href="/layout_aplicacoes/css/estilo_mesario.css"     rel="stylesheet" type="text/css" />
</head>

<script language="javascript">

	function fecharJanela() {
		window.self.close();  
	}

</script>

<body>

<div id="tudo">
	<div id="mesario">
		<ul id="popup">
			<li><strong>T�tulo: </strong><span class="result titulo_popup"></span><%=titulo%></li>
			<li><strong>Nome: </strong><span class="result nome_popup"></span><%=nome%></li>
			<li><strong>Telefone(s): </strong><span class="result telefone_popup"></span><%=telefone%></li>
			<li><strong>E-mail: </strong><span class="result email_popup"></span><%=email%></li>
			<li><strong>Endere�o: </strong><span class="result endereco_popup"></span><%=enderecoCompleto%></li>
		</ul>
	</div>
</div>

</body>

</html>