<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%
String mensagem = (String) request.getAttribute("mensagem");
%>
<%
	String path = request.getContextPath();
	String baseServer = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort();  
			
%>

<c:set var="diretor" value="${requestScope.diretor}" ></c:set>
<c:set var="testemunha1" value="${requestScope.testemunha1}" ></c:set>
<c:set var="testemunha2" value="${requestScope.testemunha2}" ></c:set>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>TRE/PE - Gerenciar Composi��o</title>
<script type="text/javascript" src="<%=baseServer%>/layout_aplicacoes/js/jquery.min.js" charset="iso-8859-1"></script>
<script type="text/javascript" src="<%=baseServer%>/layout_aplicacoes/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<%=baseServer%>/layout_aplicacoes/js/seginf_aplicacoes.js"></script>
<script type="text/javascript" src="js/javascript.js"></script>
<link href="<%=baseServer%>/layout_aplicacoes/css/estilo_aplicacoes.css" rel="stylesheet" type="text/css" />
<link href="<%=baseServer%>/layout_aplicacoes/css/estilo_mesario.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">

jQuery(function($){			   
	   $("#cpf").mask("?999.999.999-99");	   
	   $("#cpf_test").mask("?999.999.999-99");
	   $("#cpf_test2").mask("?999.999.999-99");
});
function isEmpty(palavra){
	if(palavra == "" || palavra.trim == ""){
		return true;
	}
	else{
		return false;
	}
}

function validarCadastro(){

	if(isEmpty(document.composicaoEje.tratamento_diretor.value) || isEmpty(document.composicaoEje.nome.value)   ||
			   isEmpty(document.composicaoEje.endereco.value)  || isEmpty(document.composicaoEje.bairro.value)  ||
			   isEmpty(document.composicaoEje.bairro.value)|| isEmpty(document.composicaoEje.cidade.value)      ||
			   isEmpty(document.composicaoEje.cep.value) || isEmpty(document.composicaoEje.rg.value)            ||
			   isEmpty(document.composicaoEje.orgao_exp.value)    || isEmpty(document.composicaoEje.cpf.value)  ||	
			   isEmpty(document.composicaoEje.rg.value)	   || isEmpty(document.composicaoEje.orgao_exp.value) || 
			   isEmpty(document.composicaoEje.nacionalidade.value) || isEmpty(document.composicaoEje.nome_test.value) ||
			   isEmpty(document.composicaoEje.cargo_test.value) || isEmpty(document.composicaoEje.rg_test.value) ||
			   isEmpty(document.composicaoEje.cpf_test.value)|| isEmpty(document.composicaoEje.rg_test.value) ||
			   isEmpty(document.composicaoEje.nome_test2.value)|| isEmpty(document.composicaoEje.cargo_test2.value) ||
			   isEmpty(document.composicaoEje.rg_test2.value) || isEmpty(document.composicaoEje.cpf_test2.value) ){
		   
	   alert ("OBS.: Todas as informa��es abaixo devem ser preenchidas para que o cadastro seja concluido com sucesso.");
		return false;		   
	}
	else{			

		 if(document.composicaoEje.cep.value.length != 9){
			
			if(document.composicaoEje.cep.value < 9){
				alert("OBS.: O campo CEP cont�m menos que oito d�gitos..");
				document.composicaoEje.cep.focus;
				return false;
			}
			else{
				alert("OBS.: O campo CEP cont�m mais que oito d�gitos..");
				document.composicaoEje.cep.focus;
				return false;
			}
			return false;
		
		}else if(!validarCPF(document.composicaoEje.cpf.value)){
			document.composicaoEje.cpf.focus();
			alert("OBS.: O cpf informado do diretor da EJE n�o � v�lido.");
			return false;
		}else if(isNaN(document.composicaoEje.rg.value)){
			document.composicaoEje.rg.focus();
			alert("OBS.: O RG informado do diretor da EJE deve ser todo numerico.");
			return false;
		}else if(!validarCPF(document.composicaoEje.cpf_test.value)){
			document.composicaoEje.cpf_test.focus();
			alert("OBS.: O cpf informado da primeira testemunha n�o � v�lido.");
			return false;
		}else if(!validarCPF(document.composicaoEje.cpf_test2.value)){
			document.composicaoEje.cpf_test2.focus();
			alert("OBS.: O cpf informado da segunda testemunha n�o � v�lido.");
			return false;
		}
		
		else{
			//Valida��o realizada com sucesso.				
			return true;
		}
	}
}
	function exibirMensagem(msg){
		    alert(msg);	
			document.composicaoEje.action = "./ServletIniciarSistemaMesario.do";
			document.composicaoEje.submit();		
    }	
	
	function limpar(){
		document.composicaoEje.action = "./gerenciarComposicao.jsp";
		document.composicaoEje.submit();
	}
	

</script>

</head>

<%
	if (mensagem != null) {
%>
<body onload="javascript:exibirMensagem('<%=mensagem%>')" >
<%
	} else {
%>
<body>
<%
	}
%>

<div id="tudo">
<div id="topo">
		<h1 class="tre">
		<span class="destaque_sigla_tre">T</span>
		ribunal
		<span class="destaque_sigla_tre">R</span>
		egional
		<span class="destaque_sigla_tre">E</span>
		leitoral de Pernambuco
		</h1>
</div>
<div id="mesario">
	<div id="voltar_sair"><a href="./menu.jsp" id="sair">Voltar</a> <a href="./" id="sair">Sair</a></div>
	<h1>Gerenciar Composi��o</h1>
	
	<form id="composicaoEje" action="./ServletAlterarComposicaoEje.do" method="post" name="composicaoEje" onSubmit="return validarCadastro()">
	<input type="hidden" name="codDiretor" value="${diretor.codObjeto}"/>
	<input type="hidden" name="codTest1" value="${testemunha1.codObjeto}"/>
	<input type="hidden" name="codTest2" value="${testemunha2.codObjeto}"/>
	<input type="hidden" name="mensagem" value="${requestScope.mensagem}"/>
	<fieldset>
	<legend>Dados do Diretor</legend>

		<p><label for="tratamento_diretor">Tratamento do Diretor: </label><input id="tratamento_diretor" type="text" name="tratamento_diretor" value="${diretor.tratamento}"></input></p>
		<p><label for="nome">Nome Completo: </label><input id="nome" type="text" name="nome" value="${diretor.nome}"></input></p>
		<p><label for="endereco">Endere�o: </label><input id="endereco" type="text" value="${diretor.endereco}" name="endereco"></input></p>
		<p><label for="bairro">Bairro: </label><input id="bairro" type="text" value="${diretor.bairro}" name="bairro"></input></p>
						<p class="inputs_inline">
						<span class="sem_rotulo">
							<label for="cidade">Cidade: </label>

							<input id="cidade" type="text" value="${diretor.cidade}" name="cidade"></input>
							<label class="cep" for="cep">CEP: </label>
							<input id="cep" class="mascara-cep cep2" type="text" value="${diretor.cep}" maxlength="10" name="cep"></input>
							</span>
						</p>
						<p><label for="estado">Estado: </label>
							<select id="estado" name="estado">
								<option value="AC" <c:if test="${diretor.estado eq 'AC'}" > selected="selected" </c:if>>AC</option>
								<option value="AL"<c:if test="${diretor.estado eq 'AL'}" > selected="selected" </c:if>>AL</option>
								<option value="AM"<c:if test="${diretor.estado eq 'AM' }" > selected="selected" </c:if>>AM</option>
								<option value="AP"<c:if test="${diretor.estado eq 'AP' }" > selected="selected" </c:if>>AP</option>
								<option value="BA"<c:if test="${diretor.estado eq 'BA' }" > selected="selected" </c:if>>BA</option>
								<option value="CE"<c:if test="${diretor.estado eq 'CE' }" > selected="selected" </c:if>>CE</option>
								<option value="DF"<c:if test="${diretor.estado eq 'DF'}" > selected="selected" </c:if>>DF</option>
								<option value="ES"<c:if test="${diretor.estado eq 'ES'}" > selected="selected" </c:if>>ES</option>
								<option value="GO"<c:if test="${diretor.estado eq 'GO' }" > selected="selected" </c:if>>GO</option>
								<option value="MA"<c:if test="${diretor.estado eq 'MA'}" > selected="selected" </c:if>>MA</option>
								<option value="MG"<c:if test="${diretor.estado eq 'MG'}" > selected="selected" </c:if>>MG</option>
								<option value="MS"<c:if test="${diretor.estado eq 'MS'}" > selected="selected" </c:if>>MS</option>
								<option value="MT"<c:if test="${diretor.estado eq 'MT'}" > selected="selected" </c:if>>MT</option>
								<option value="PA"<c:if test="${diretor.estado eq 'PA'}" > selected="selected" </c:if>>PA</option>
								<option value="PB"<c:if test="${diretor.estado eq 'PB'}" > selected="selected" </c:if>>PB</option>
								<option value="PE"<c:if test="${diretor.estado eq 'PE' || diretor.estado eq null}" > selected="selected" </c:if>>PE</option>
								<option value="PI"<c:if test="${diretor.estado eq 'PI'}" > selected="selected" </c:if>>PI</option>
								<option value="PR"<c:if test="${diretor.estado eq 'PR'}" > selected="selected" </c:if>>PR</option>
								<option value="RJ"<c:if test="${diretor.estado eq 'RJ'}" > selected="selected" </c:if>>RJ</option>
								<option value="RN"<c:if test="${diretor.estado eq 'RN'}" > selected="selected" </c:if>>RN</option>
								<option value="RO"<c:if test="${diretor.estado eq 'RO'}" > selected="selected" </c:if>>RO</option>
								<option value="RR"<c:if test="${diretor.estado eq 'RR'}" > selected="selected" </c:if>>RR</option>
								<option value="RS"<c:if test="${diretor.estado eq 'RS'}" > selected="selected" </c:if>>RS</option>
								<option value="SC"<c:if test="${diretor.estado eq 'SC'}" > selected="selected" </c:if>>SC</option>
								<option value="SE"<c:if test="${diretor.estado eq 'SE'}" > selected="selected" </c:if>>SE</option>
								<option value="SP"<c:if test="${diretor.estado eq 'SP'}" > selected="selected" </c:if>>SP</option>
								<option value="TO"<c:if test="${diretor.estado eq 'TO'}" > selected="selected" </c:if>>TO</option>
							</select>
						</p>
		<p><label for="nacionalidade">Nacionalidade: </label><input id="nacionalidade" type="text" value="${diretor.nacionalidade}" name="nacionalidade"></input></p>
						<p><label for="estado_civil">Estado Civil: </label>
							<select id="estado_civil" name="estado_civil">
								<option value="solteiro" <c:if test="${diretor.estadoCivil eq 'solteiro'}" > selected="selected" </c:if> >solteiro(a)</option>
								<option value="casado" <c:if test="${diretor.estadoCivil eq 'casado'}" > selected="selected" </c:if> >casado(a)</option>
								<option value="viuvo" <c:if test="${diretor.estadoCivil eq 'viuvo'}" > selected="selected" </c:if> >vi�vo(a)</option>
								<option value="separado juridicamente" <c:if test="${diretor.estadoCivil eq 'separado juridicamente'}" > selected="selected" </c:if> >separado(a) judicialmente</option>
								<option value="divorciado" <c:if test="${diretor.estadoCivil eq 'divorciado'}" > selected="selected" </c:if> >divorciado(a)</option>
							</select>
						</p>
		<p class="inputs_inline">

							<span class="sem_rotulo">
								<label for="rg">RG: </label><input id="rg" type="text" value="${diretor.rg}" name="rg"/>
								<label for="orgao_exp">�rg�o Expedidor:</label><input id="orgao_exp" type="text" name="orgao_exp" value="${diretor.orgaoExp}" />
								
							</span>
						</p>
						
		<p><label for="cpf">CPF: </label><input id="cpf" type="text" value="${diretor.cpf}" name="cpf"></input></p>
		</fieldset>

		<fieldset>
		<legend>Dados da Primeira Testemunha</legend>
			<p><label for="nome_test">Nome Completo: </label><input id="nome_test" type="text" value="${testemunha1.nome}" name="nome_test"></input></p>
			<p><label for="cargo_test">Cargo: </label><input id="cargo_test" type="text" value="${testemunha1.cargo}" name="cargo_test"></input></p>
			<p><label for="cpf_test">CPF: </label><input id="cpf_test" type="text" value="${testemunha1.cpf}" name="cpf_test"></input></p>
			<p><label for="rg_test">RG: </label><input id="rg_test" type="text" value="${testemunha1.rg}" name="rg_test"/></p>

		</fieldset>
		
		<fieldset>
		<legend>Dados da Segunda Testemunha</legend>
			<p><label for="nome_test2">Nome Completo: </label><input id="nome_test2" type="text" value="${testemunha2.nome}" name="nome_test2"></input></p>
			<p><label for="cargo_test2">Cargo: </label><input id="cargo_test2" type="text" value="${testemunha2.cargo}" name="cargo_test2"></input></p>
			<p><label for="cpf_test2">CPF: </label><input id="cpf_test2" type="text" value="${testemunha2.cpf}" name="cpf_test2"></input></p>
			<p><label for="rg_test2">RG: </label><input id="rg_test2" type="text" value="${testemunha2.rg}" name="rg_test2"/></p>

		</fieldset>
		
		<p>
			<input type="submit" value="Salvar" class="botoes_form" />
			<input type="button" value="Limpar" onclick="javascript:limpar()" class="botoes_form" />
		</p>
	
	</form>
		<div id="caixa_assinatura">
		<div id="assinatura"> STI / Coordenadoria de Desenvolvimento de Sistemas </div>
	</div>
	</div>
</div>
</body>
</html>