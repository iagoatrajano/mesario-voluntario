<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="mesario.controle.universidadeconveniada.UniversidadeConveniada"%>
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<c:set var="universidade" value="${requestScope.universidade}" />
<%
String action ="./ServletCadastrarUniversidadeConveniada.do";
String mensagem = (String) request.getAttribute("mensagem");
UniversidadeConveniada universidade = (UniversidadeConveniada) request.getAttribute("universidade");
%>
<%
	String path = request.getContextPath();
	String baseServer = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort();  
			
%>



<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>TRE/PE - Cadastrar Conv�nio</title>
<script type="text/javascript" src="<%=baseServer%>/layout_aplicacoes/js/jquery.min.js" charset="iso-8859-1"></script>
<script type="text/javascript" src="<%=baseServer%>/layout_aplicacoes/js/seginf_aplicacoes.js"></script>
<script type="text/javascript" src="js/javascript.js"></script>
<script type="text/javascript" src="<%=baseServer%>/layout_aplicacoes/js/jquery.maskedinput.min.js"></script>
<link href="<%=baseServer%>/layout_aplicacoes/css/estilo_aplicacoes.css" rel="stylesheet" type="text/css" />
<link href="<%=baseServer%>/layout_aplicacoes/css/estilo_mesario.css" rel="stylesheet" type="text/css" />


<script language="javascript" type="text/javascript">

   jQuery(function($){			   
	   $("#cpf").mask("?999.999.999-99");
	   $("#cnpj").mask("?99.999.999/9999-99");

	});

	function isEmpty(palavra){
		if(palavra == "" || palavra.trim == ""){
			return true;
		}
		else{
			return false;
		}
	}
	function validarRadioBtn(){
		
		if(document.cadastroConvenio.tipoPessoa[0].checked ){			
			return true;
		}else if(document.cadastroConvenio.tipoPessoa[1].checked){			
			return true;
		}
		else{
			alert("Por favor,seleciona alguma das op��es do Tipo de Pessoa Juridica.");
			return false
		}		
		
	}

	function validarCadastro(){

		if(isEmpty(document.cadastroConvenio.razaoSocial.value) || isEmpty(document.cadastroConvenio.nomeFantasia.value)   ||
				   isEmpty(document.cadastroConvenio.sigla.value)  || isEmpty(document.cadastroConvenio.cargo) ||
				   isEmpty(document.cadastroConvenio.nomeRep.value)|| isEmpty(document.cadastroConvenio.endereco.value)      ||
				   isEmpty(document.cadastroConvenio.bairro.value) || isEmpty(document.cadastroConvenio.cidade.value)     ||
				   isEmpty(document.cadastroConvenio.cep.value)    || isEmpty(document.cadastroConvenio.nacionalidade.value) ||	
				   isEmpty(document.cadastroConvenio.rg.value)	   || isEmpty(document.cadastroConvenio.orgaoExp.value)      ||
				   isEmpty(document.cadastroConvenio.cpf.value)    || (!(validarRadioBtn())) ||  
				   isEmpty(document.cadastroConvenio.cnpj.value)){
			   
		   alert ("OBS.: Todas as informa��es devem ser preenchidas para que o cadastro seja conclu�do com sucesso.");
			return false;		   
		}
		else{			

			if(!validarCNPJ(document.cadastroConvenio.cnpj.value)){
				document.cadastroConvenio.cnpj.focus();
				alert("OBS.: O CNPJ informado n�o � v�lido.");
				return false;
			}else if(document.cadastroConvenio.cep.value.length != 9){
				
				if(document.cadastroConvenio.cep.value.length < 9){
					alert("OBS.: O campo CEP cont�m menos que oito d�gitos..");
					document.cadastroConvenio.cep.focus;
					return false;
				}
				else{
					alert("OBS.: O campo CEP cont�m mais que oito d�gitos..");
					document.cadastroConvenio.cep.focus;
					return false;
				}
				return false;
			
			}else if(!validarCPF(document.cadastroConvenio.cpf.value)){
				document.cadastroConvenio.cpf.focus();
				alert("OBS.: O cpf informado n�o � v�lido.");
				return false;
			}else if(isNaN(document.cadastroConvenio.rg.value)){
				document.cadastroConvenio.rg.focus();
				alert("OBS.: O RG deve ser todo numerico.");
				return false;
			}else if(document.cadastroConvenio.emailConvenio.value !=null && document.cadastroConvenio.emailConvenio.value !=""){
				var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

				if(!filtro.test(document.cadastroConvenio.emailConvenio.value)){
					alert("O endere�o de e-mail n�o � v�lido!");	
					document.cadastroConvenio.emailConvenio.focus();
			        return false;	
				}
			}
			else{
				//Valida��o realizada com sucesso.				
				return true;
			}
		}
		
				
		
	}	

	function exibirMensagem(msg){
			alert(msg);
			if(document.cadastroConvenio.flag.value == 'sim'){
				document.cadastroConvenio.flag.value = 'ok';
				document.cadastroConvenio.action = "./ServletGerarTermoConvenio.do";
				document.cadastroConvenio.submit();
				document.cadastroConvenio.action = "./ServletCadastrarUniversidadeConveniada.do";
				return true;
			}			
			
			return true;
	}

	function limpar(){
		document.cadastroConvenio.action = "./cadastroUniversidade.jsp?universidade=''";
		document.cadastroConvenio.submit();
	}

</script>


</head>

<%
	if (mensagem != null && universidade != null) {
%>
<body onload="javascript:exibirMensagem('<%=mensagem%>')">
<%
	} if(mensagem != null && universidade == null){	
	
%>
<body onload="javascript:exibirMensagem('<%=mensagem%>')">
<%
   }if(mensagem == null && universidade == null) {
%>
<body>
<%
	}
%>
<body>

<div id="tudo">
<div id="mesario">

	
	<h1>Cadastro de Conv�nio</h1>
	
	<form id="cadastroConvenio" name="cadastroConvenio" method="post" action="./ServletCadastrarUniversidadeConveniada.do"  onSubmit="return validarCadastro()">
	
	<input type="hidden" name="universidade" value="${requestScope.universidade}" />
	<input type="hidden" name="flag" value="${requestScope.flag}" />
	
	<fieldset>
	<legend>Dados da Entidade</legend>
	    <p><label for="cnpj">CNPJ: </label><input id="cnpj" type="text" <c:if test="${flag eq 'nao'}" > value="${universidade.cnpj}" </c:if>   name="cnpj"></input></p>
		<p><label for="razao_social">Raz�o Social: </label><input id="razao_social" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.razaoSocial}" </c:if>   name="razaoSocial"></input></p>
		<p><label for="nome_fantasia">Nome Fantasia: </label><input id="nome_fantasia" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.nomeFantasia}"</c:if> name="nomeFantasia"></input></p>
		<p><label for="sigla">Sigla: </label><input id="sigla" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.sigla}"</c:if> name="sigla"></input></p>
		<p><label for="phone">Telefone: </label><input ${desabilitado} id="phone" name="phone" type="text" style="width: 30%;" value="${universidade.telefone}"></input></p>
		<p><label for="email">E-mail: </label><input ${desabilitado} id="emailConvenio" name="emailConvenio" type="text" style="width: 45%;"  value="${universidade.email}"></input></p>
		<p class="radios_inline">

							<span>Pessoa Jur�dica: </span>
							<span class="campos_inline">
								<input  type="radio"  name="tipoPessoa" value="2" <c:if test="${universidade.tipoPessoaJuridica eq 2 && flag == 'nao'}">checked="checked"</c:if> /><label for="sim">de Direito P�blico</label>
								<input  type="radio"  name="tipoPessoa" value="1" <c:if test="${universidade.tipoPessoaJuridica eq 1 && flag == 'nao'}">checked="checked"</c:if> /><label for="nao">de Direito Privado</label>
							</span>
						</p>
						
		</fieldset>
		<fieldset>
		<legend>Dados do Representante</legend>
						<p>
							<label for="cargo">Cargo: </label>
							<input id="cargo" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.tituloRepresentante}"</c:if>  name="cargo"></input>
							
						</p>
		<p><label for="nome_rep">Nome Completo: </label><input id="nome_rep" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.nomeRepresentante}"</c:if> name="nomeRep"></input></p>
		<p><label for="endereco">Endere�o: </label><input id="endereco" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.enderecoRepresentante}"</c:if> name="endereco"></input></p>
		<p><label for="bairro">Bairro: </label><input id="bairro" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.bairroRepresentante}"</c:if> name="bairro"></input></p>
						<p class="inputs_inline">

						<span class="sem_rotulo">
							<label for="cidade">Cidade: </label>
							<input id="cidade" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.cidadeRepresentante}"</c:if> name="cidade"></input>
							<label class="cep" for="cep">CEP: </label>
							<input id="cep" class="mascara-cep cep2" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.cepRepresentante}" </c:if> maxlength="10" name="cep"></input>
							</span>
						</p>
						<p><label for="estado">Estado: </label>

							<select id="estado" name="estado">
								<option value="AC" <c:if test="${universidade.estadoRepresentante eq 'AC' && flag == 'nao'}" > selected="selected" </c:if>>AC</option>
								<option value="AL"<c:if test="${universidade.estadoRepresentante eq 'AL' && flag == 'nao'}" > selected="selected" </c:if>>AL</option>
								<option value="AM"<c:if test="${universidade.estadoRepresentante eq 'AM' && flag == 'nao'}" > selected="selected" </c:if>>AM</option>
								<option value="AP"<c:if test="${universidade.estadoRepresentante eq 'AP' && flag == 'nao'}" > selected="selected" </c:if>>AP</option>
								<option value="BA"<c:if test="${universidade.estadoRepresentante eq 'BA' && flag == 'nao'}" > selected="selected" </c:if>>BA</option>
								<option value="CE"<c:if test="${universidade.estadoRepresentante eq 'CE' && flag == 'nao'}" > selected="selected" </c:if>>CE</option>
								<option value="DF"<c:if test="${universidade.estadoRepresentante eq 'DF' && flag == 'nao'}" > selected="selected" </c:if>>DF</option>
								<option value="ES"<c:if test="${universidade.estadoRepresentante eq 'ES' && flag == 'nao'}" > selected="selected" </c:if>>ES</option>
								<option value="GO"<c:if test="${universidade.estadoRepresentante eq 'GO' && flag == 'nao'}" > selected="selected" </c:if>>GO</option>
								<option value="MA"<c:if test="${universidade.estadoRepresentante eq 'MA' && flag == 'nao'}" > selected="selected" </c:if>>MA</option>
								<option value="MG"<c:if test="${universidade.estadoRepresentante eq 'MG' && flag == 'nao'}" > selected="selected" </c:if>>MG</option>
								<option value="MS"<c:if test="${universidade.estadoRepresentante eq 'MS' && flag == 'nao'}" > selected="selected" </c:if>>MS</option>
								<option value="MT"<c:if test="${universidade.estadoRepresentante eq 'MT' && flag == 'nao'}" > selected="selected" </c:if>>MT</option>
								<option value="PA"<c:if test="${universidade.estadoRepresentante eq 'PA' && flag == 'nao'}" > selected="selected" </c:if>>PA</option>
								<option value="PB"<c:if test="${universidade.estadoRepresentante eq 'PB' && flag == 'nao'}" > selected="selected" </c:if>>PB</option>
								<option value="PE"<c:if test="${(universidade.estadoRepresentante eq 'PE' && flag == 'nao') || universidade.estadoRepresentante eq null }" > selected="selected" </c:if>SELECTED >PE</option>
								<option value="PI"<c:if test="${universidade.estadoRepresentante eq 'PI' && flag == 'nao'}" > selected="selected" </c:if>>PI</option>
								<option value="PR"<c:if test="${universidade.estadoRepresentante eq 'PR' && flag == 'nao'}" > selected="selected" </c:if>>PR</option>
								<option value="RJ"<c:if test="${universidade.estadoRepresentante eq 'RJ' && flag == 'nao'}" > selected="selected" </c:if>>RJ</option>
								<option value="RN"<c:if test="${universidade.estadoRepresentante eq 'RN' && flag == 'nao'}" > selected="selected" </c:if>>RN</option>
								<option value="RO"<c:if test="${universidade.estadoRepresentante eq 'RO' && flag == 'nao'}" > selected="selected" </c:if>>RO</option>
								<option value="RR"<c:if test="${universidade.estadoRepresentante eq 'RR' && flag == 'nao'}" > selected="selected" </c:if>>RR</option>
								<option value="RS"<c:if test="${universidade.estadoRepresentante eq 'RS' && flag == 'nao'}" > selected="selected" </c:if>>RS</option>
								<option value="SC"<c:if test="${universidade.estadoRepresentante eq 'SC' && flag == 'nao'}" > selected="selected" </c:if>>SC</option>
								<option value="SE"<c:if test="${universidade.estadoRepresentante eq 'SE' && flag == 'nao'}" > selected="selected" </c:if>>SE</option>
								<option value="SP"<c:if test="${universidade.estadoRepresentante eq 'SP' && flag == 'nao'}" > selected="selected" </c:if>>SP</option>
								<option value="TO"<c:if test="${universidade.estadoRepresentante eq 'TO' && flag == 'nao'}" > selected="selected" </c:if>>TO</option>
							</select>
						</p>
						 
		<p><label for="nacionalidade">Nacionalidade: </label><input id="nacionalidade" type="text" <c:if test="${flag eq 'nao'}" > value="${universidade.nacionalidadeRepresentante}" </c:if> name="nacionalidade"></input></p>
						<p><label for="estado_civil">Estado Civil: </label>

							<select id="estado_civil" name="estadoCivil">
								<option value="solteiro" <c:if test="${universidade.estadoCivilRepresentante eq 'solteiro' && flag == 'nao'}" > selected="selected" </c:if> >solteiro(a)</option>
								<option value="casado" <c:if test="${universidade.estadoCivilRepresentante eq 'casado' && flag == 'nao'}" > selected="selected" </c:if> >casado(a)</option>
								<option value="viuvo" <c:if test="${universidade.estadoCivilRepresentante eq 'viuvo' && flag == 'nao'}" > selected="selected" </c:if> >vi�vo(a)</option>
								<option value="seperado Judicialmente" <c:if test="${universidade.estadoCivilRepresentante eq 'seperado Judicialmente' && flag == 'nao'}" > selected="selected" </c:if> >separado(a) judicialmente</option>
								<option value="divorciado" <c:if test="${universidade.estadoCivilRepresentante eq 'divorciado' && flag == 'nao'}" > selected="selected" </c:if> >divorciado(a)</option>
							</select>
						</p>
		<p class="inputs_inline">
							<span class="sem_rotulo">
								<label for="rg">RG: </label><input id="rg" type="text" <c:if test="${flag eq 'nao'}" > value="${universidade.rgRepresentante}"</c:if> name="rg"/>

								<label for="orgao_exp">�rg�o Expedidor:</label><input id="orgao_exp" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.orgaoExpRgRepresentante}"</c:if> name="orgaoExp" maxlength="6"/>
								
							</span>
						</p>
						
		<p><label for="cpf">CPF: </label><input id="cpf" type="text" <c:if test="${flag eq 'nao'}" >value="${universidade.cpfRepresentante}"</c:if> name="cpf"></input></p>
		
		</fieldset>
		
		<p>
			<input type="submit" value="Salvar" class="botoes_form" />
			<input type="button" value="Limpar" onclick="javascript:limpar()" class="botoes_form" />
		</p>
	</form>

		
	</div>
	</div>

</body>
</html>