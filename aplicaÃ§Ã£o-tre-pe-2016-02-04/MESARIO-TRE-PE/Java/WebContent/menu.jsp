<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<?xml version="1.0" encoding="iso-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<c:set var="nivel" value="${sessionScope.usuario.nivel}"/>




<%
String path = request.getContextPath();
String baseServer = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<html>

  <head>
    <title>TRE/PE - Menu </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

    <script type="text/javascript" src="/layout_aplicacoes/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/layout_aplicacoes/js/seginf_aplicacoes.js"></script>


    <link rel="stylesheet" href="/layout_aplicacoes/css/estilo_aplicacoes.css"  type="text/css" />
    <link rel="stylesheet" href="/layout_aplicacoes/css/estilo_mesario.css" type="text/css" />

    <!--[if lte IE 6]>
  <script src="/layout_aplicacoes/js/DD_belatedPNG_0.0.7a.js" type="text/javascript" charset="iso-8859-1"></script>
      <script type="text/javascript">
      DD_belatedPNG.fix('#topo, #caixa_assinatura, img');
      </script>
      <script src="/layout_aplicacoes/js/ie6_legis.js" type="text/javascript" charset="iso-8859-1"></script>
    <![endif]-->
  </head>

  <body>
    <div id="tudo">

      <!-- INICIO DO C�DIGO DO INCLUDE do TOPO -->
      <!-- <c:import url="/layout_aplicacoes/includes/include_topo.html"/> -->
      <div id="topo">
        <h1 class="tre"><span class="destaque_sigla_tre">T</span>ribunal <span class="destaque_sigla_tre">R</span>egional <span class="destaque_sigla_tre">E</span>leitoral de Pernambuco</h1>
      </div>
      <!-- FIM DO C�DIGO DO INCLUDE do TOPO -->

      <div id="mesario">
        <div id="voltar_sair"><a href="./" id="sair">Sair</a></div>
        <h1>Sistema Mes�rio Volunt�rio</h1>

        <c:if test="${nivel eq 1}">
          <ul class="menu">
            <li> <a href="./ServletPreencherConsultaMesarioVoluntario.do"> Consultar Mes�rios Volunt�rios</a> </li>
          </ul>
        </c:if>

        <c:if test="${nivel eq 2}">

          <ul class="menu">
            <li> <a href="./ServletPreencherListarConvenios.do"> Analisar Conv�nio de Universidades</a> </li>

            <li> <a href="./consultarEstatisticas.jsp"> Consultar Estat�sticas            </a> </li>

            <li> <a href="./ServletPreencherComposicaoEje.do"> Gerenciar Composi��o da EJE            </a> </li>

            <li> <a href="./ServletPreencherListarConveniosValidos.do"> Listar Conv�nios V�lidos       </a></li>

            <li> <a href="./ServletPreencherConsultaMesarioVoluntario.do"> Consultar Mes�rios Volunt�rios</a> </li>

          </ul>

        </c:if>

      </div>

      <!-- INICIO DO C�DIGO DO INCLUDE do RODAP� -->
      <!-- <c:import url="/layout_aplicacoes/includes/include_rodape.html"/> -->
      <div id="caixa_assinatura">
        <div id="assinatura">STI / Coordenadoria de Desenvolvimento de Sistemas</div>
      </div>
      <!-- FIM DO C�DIGO DO INCLUDE do RODAP� -->

    </div> <!-- fim da div#tudo -->

  </body>

</html>