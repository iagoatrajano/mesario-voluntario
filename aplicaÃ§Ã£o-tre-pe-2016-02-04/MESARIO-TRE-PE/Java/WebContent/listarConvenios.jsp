<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<c:set var="listaUniversidades" value="${requestScope.listaUniversidades}" />
<%
String mensagem = (String) request.getAttribute("mensagem");
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>TRE/PE - Analisar Conv�nio</title>
<script type="text/javascript" src="/layout_aplicacoes/js/jquery.min.js" charset="iso-8859-1"></script>
<script type="text/javascript" src="/layout_aplicacoes/js/seginf_aplicacoes.js"></script>
<link href="/layout_aplicacoes/css/estilo_aplicacoes.css" rel="stylesheet" type="text/css" />
<link href="/layout_aplicacoes/css/estilo_mesario.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/javascript.js"></script>

</head>

<%
	if (mensagem != null) {
%>
<body onload="alert('<%=mensagem%>')" >
<%
	} else {
%>
<body>
<%
	}
%>
<div id="tudo">
<form id="form1" method="post" action="./ServletPreencherEditarConvenio.do">
	<input type="hidden" name="universidadeSelecionada" value="" />
	<input type="hidden" name="analisarConvenio" value="" />
	<div id="topo">
		<h1 class="tre">
		<span class="destaque_sigla_tre">T</span>
		ribunal
		<span class="destaque_sigla_tre">R</span>
		egional
		<span class="destaque_sigla_tre">E</span>
		leitoral de Pernambuco
		</h1>
	</div> 
	<div id="mesario">
	<div id="voltar_sair"><a href="menu.jsp" id="sair">Voltar</a><a href=" " id="sair">Sair</a></div>
	<h1>An�lise de Conv�nio</h1>
	<table id="analisar_convenio" border="0" cellpadding="0" cellspacing="0">
		<caption>Institui��es de Ensino Superior Conveniadas</caption>
		<thead>
			<tr>
				<th id="editar"></th>
				<th id="numero">N�mero</th>
				<th id="nome_convenio">Institui��o</th>
				<th id="cnpj">CNPJ</th>
				<th id="situacao">Situa��o</th>
				<th id="data_inicial">Data Inicial</th>
				<th id="data_final">Data Final</th>
			</tr>
		</thead>
		<!-- foreach -->
		
		<tbody>
			<c:forEach var="universidade" items="${listaUniversidades}">
				<tr style="background: url(&quot;/layout_aplicacoes/imagens/fundo_linhas_geral_B3.jpg&quot;) repeat scroll 0% 0% transparent;">
					
					<td headers="editar"><a href="./ServletPreencherEditarConvenio.do?universidadeSelecionada=${universidade.codObjeto}" > <img src="/layout_aplicacoes/imagens/lapis.gif" title="Editar" border="0" /></a> <a href="./ServletGerarTermoConvenio.do?universidadeSelecionada=${universidade.codObjeto}" ><img  title="Regerar Termo de Conv�nio" src="/layout_aplicacoes/imagens/icone_regerar.gif" border="0"/></a><!--<c:if test="${universidade.convenioValido ne 'V�lido'}"><img  title="Conv�nio inv�lido, imposs�vel regerar Termo de Conv�nio" src="/layout_aplicacoes/imagens/icone_regerar2.gif" border="0"/></c:if>--><a href="./ServletPreencherValidarConvenio.do?universidadeSelecionada=${universidade.codObjeto}" ><img src="/layout_aplicacoes/imagens/icone-mesario-valido.gif" title="Validar Conv�nio" border="0" /></a></td>
					<td headers="numero">${universidade.numeroAnoConvenio}</td>
					<td headers="nome_convenio">${universidade.sigla} - ${universidade.nomeFantasia}</td>
					<td headers="cnpj">${universidade.cnpj}</td>
					<td headers="situacao">${universidade.convenioValido}</td>
					<td headers="data_inicial"><c:if test="${universidade.dataInicialConvenio.date < 10}">0</c:if>${universidade.dataInicialConvenio.date} / <c:if test="${universidade.dataInicialConvenio.month+1 < 10}">0</c:if>${universidade.dataInicialConvenio.month+1} / ${universidade.dataInicialConvenio.year+1900}</td>
					<td headers="data_final"><c:if test="${universidade.dataFinalConvenio.date < 10}">0</c:if>${universidade.dataFinalConvenio.date} / <c:if test="${universidade.dataFinalConvenio.month+1 < 10}">0</c:if>${universidade.dataFinalConvenio.month+1} / ${universidade.dataFinalConvenio.year+1900}</td>
				</tr>
			</c:forEach>
		</tbody>
		
	</table>
	</div>
	<div id="caixa_assinatura">
		<div id="assinatura"> STI / Coordenadoria de Desenvolvimento de Sistemas </div>
	</div>
</form>
</div>
</body>
</html>