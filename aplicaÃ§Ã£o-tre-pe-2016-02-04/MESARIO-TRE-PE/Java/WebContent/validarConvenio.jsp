<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<c:set var="universidade" value="${requestScope.universidade}" />
<c:set var="desabilitado" value="${requestScope.disabled}" />

<head>
<%
String mensagem = (String) request.getAttribute("mensagem");
%>
<%
	String path = request.getContextPath();
	String baseServer = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort();  
			
%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>TRE/PE - Analisar Conv�nio</title>
<script type="text/javascript" src="<%=baseServer%>/layout_aplicacoes/js/jquery.min.js" charset="iso-8859-1"></script>
<script type="text/javascript" src="<%=baseServer%>/layout_aplicacoes/js/seginf_aplicacoes.js"></script>
<script type="text/javascript" src="js/javascript.js"></script>
<script type="text/javascript" src="<%=baseServer%>/layout_aplicacoes/js/jquery.maskedinput.min.js"></script>
<link href="<%=baseServer%>/layout_aplicacoes/css/estilo_aplicacoes.css" rel="stylesheet" type="text/css" />
<link href="<%=baseServer%>/layout_aplicacoes/css/estilo_mesario.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
   jQuery(function($){			   
	   $("#cpf").mask("?999.999.999-99");
	   $("#cnpj").mask("?99.999.999/9999-99");
	});

	function isEmpty(palavra){
		if(palavra == "" || palavra.trim == ""){
			return true;
		}
		else{
			return false;
		}
	}
	function validarRadioBtn(){
		
		if(document.cadastroConvenio.tipoPessoa[0].checked ){			
			return true;
		}else if(document.cadastroConvenio.tipoPessoa[1].checked){			
			return true;
		}
		else{
			alert("Por favor,seleciona alguma das op��es do Tipo de Pessoa Juridica.");
			return false;
		}		
		
	}

	function validarCadastro(){

		//verifica se o campo N�mero (n�o obrigat�rio) est� preeenchido, se estiver o usu�rio tem que digitar o Ano
		if (document.cadastroConvenio.numeroDoConvenio.value != "")
		{
			if(document.cadastroConvenio.anoDoConvenio.value == "")
			{
				alert("O campo Ano do n�mero do conv�nio tem que ser preenchido.");
				return false;
			}
		}
		
		//verifica se o campo Ano (n�o obrigat�rio) est� com menos de 4 d�gitos quando o usu�rio digitar o Ano
		if (document.cadastroConvenio.anoDoConvenio.value.length > 0)
		{
			if(document.cadastroConvenio.anoDoConvenio.value.length < 4)
			{
				alert("O Ano do n�mero do conv�nio cont�m menos de quatro d�gitos.");
				return false;
			}
		}

		if(isEmpty(document.cadastroConvenio.razaoSocial.value) || isEmpty(document.cadastroConvenio.nomeFantasia.value)   ||
				   isEmpty(document.cadastroConvenio.sigla.value)  || isEmpty(document.cadastroConvenio.cargo) ||
				   isEmpty(document.cadastroConvenio.nomeRep.value)|| isEmpty(document.cadastroConvenio.endereco.value)      ||
				   isEmpty(document.cadastroConvenio.bairro.value) || isEmpty(document.cadastroConvenio.cidade.value)     ||
				   isEmpty(document.cadastroConvenio.cep.value)    || isEmpty(document.cadastroConvenio.nacionalidade.value) ||	
				   isEmpty(document.cadastroConvenio.rg.value)	   || isEmpty(document.cadastroConvenio.orgaoExp.value)      ||
				   isEmpty(document.cadastroConvenio.cpf.value)    || (!(validarRadioBtn())) ||  
				   isEmpty(document.cadastroConvenio.cnpj.value)){
			   
		   alert ("OBS.: Todas as informa��es abaixo devem ser preenchidas para que o cadastro seja concluido com sucesso.");
			return false;		   
		}

		
				
		else{			

			if(!validarCNPJ(document.cadastroConvenio.cnpj.value)){
				document.cadastroConvenio.cnpj.focus();
				alert("OBS.: O CNPJ informado n�o � v�lido.");
				return false;
			}else if(document.cadastroConvenio.cep.value.length != 9){
				
				if(document.cadastroConvenio.cep.value.length < 9){
					alert("OBS.: O campo CEP cont�m menos que oito d�gitos..");
					document.cadastroConvenio.cep.focus;
					return false;
				}
				else{
					alert("OBS.: O campo CEP cont�m mais que oito d�gitos..");
					document.cadastroConvenio.cep.focus;
					return false;
				}
				return false;
			
			}else if(!validarCPF(document.cadastroConvenio.cpf.value)){
				document.cadastroConvenio.cpf.focus();
				alert("OBS.: O cpf informado n�o � v�lido.");
				return false;
			}else if(isNaN(document.cadastroConvenio.rg.value)){
				document.cadastroConvenio.rg.focus();
				alert("OBS.: O RG deve ser todo numerico.");
				return false;
			}
			
		}
		
				
		
	}

	

	function exibirMensagem(msg){
			alert(msg);
			
	}
	
//s� deixa o usu�rio digitar numeros
	function verificaNumero()
	{
		tecla = event.keyCode;
		
		if (tecla >= 48 && tecla <= 57)
			return true;
		else
			return false;
			  
	}//fim

</script>


</head>

<%
	if (mensagem != null) {
%>
<body onload="alert('<%=mensagem%>')" >
<%
	} else {
%>
<body>
<%
	}
%>
<div id="tudo">
<div id="topo">
		<h1 class="tre">
		<span class="destaque_sigla_tre">T</span>
		ribunal
		<span class="destaque_sigla_tre">R</span>
		egional
		<span class="destaque_sigla_tre">E</span>
		leitoral de Pernambuco
		</h1>
</div>
<div id="mesario">
<div id="voltar_sair"><a href="./ServletPreencherListarConvenios.do" id="sair">Voltar</a> <a href="./" id="sair">Sair</a></div>
	<h1>An�lise de Conv�nio</h1>
	
	<form id="cadastroConvenio" name="cadastroConvenio" method="post" action="./ServletValidarConvenio.do"  onSubmit="return validarCadastro()">
	<input type="hidden" name="codObjeto" value="${universidade.codObjeto}"/>
	<input type="hidden" name="flag" value="${requestScope.flag}"/>	
	
		<h2>
			Sigla: <strong>${universidade.sigla}</strong>
			Nome Fantasia: <strong>${universidade.nomeFantasia}</strong>
		</h2>
		
		<br/>	
		
		<fieldset>
		<legend>Dados do Conv�nio</legend>		
			<p class="radios_inline">
				<span class="campos_inline">
				<input type="checkbox" ${desabilitado} id="validar" name="convenioValido" value="V�lido"<c:if test="${universidade.convenioValido eq 'V�lido'}"> checked="checked" </c:if> ></input>
				<label for="validar">Conv�nio v�lido</label>
				</span>	
			</p>
	
			<p class="inputs_inline">
				<label for="data_inicial">Data Inicial:</label><input id="data_inicial" name="dataInicialConvenio" ${desabilitado} class="mascara-data" type="text" value="<c:if test="${universidade.dataInicialConvenio.date < 10}">0</c:if>${universidade.dataInicialConvenio.date}<c:if test="${universidade.dataInicialConvenio.month+1 < 10}">0</c:if>${universidade.dataInicialConvenio.month+1}${universidade.dataInicialConvenio.year+1900} "></input>
				<label for="data_final">Data Final:</label><input id="data_final" name="dataFinalConvenio" class="mascara-data" type="text" value="<c:if test="${universidade.dataFinalConvenio.date < 10}">0</c:if>${universidade.dataFinalConvenio.date}<c:if test="${universidade.dataFinalConvenio.month+1 < 10}">0</c:if>${universidade.dataFinalConvenio.month+1}${universidade.dataFinalConvenio.year+1900}"></input> <br/> <br/>
			</p>
			
			<p class="inputs_inline">
				<label for="numero">N�mero:</label><input id="numero" name="numeroDoConvenio" type="text" value="${universidade.numero}" maxlength="3" style=" width : 42px;" onKeypress="return verificaNumero();"></input><label for="ano">/</label>
				<input id="ano" name="anoDoConvenio" type="text" value="${universidade.ano}" maxlength="4" style=" width : 63px;" onKeypress="return verificaNumero();"></input>
			</p>
		</fieldset>
		
		<p>
			<input type="submit" value="Salvar" class="botoes_form" />
		</p>
	
	</form>
	</div>
	
	<div id="caixa_assinatura">
		<div id="assinatura"> STI / Coordenadoria de Desenvolvimento de Sistemas </div>
	</div>
	</div>
</body>
</html>