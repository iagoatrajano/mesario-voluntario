<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<c:set var="opcaoEstatistica" value="${requestScope.opcaoEstatistica}" />
<c:set var="exibir" value="${requestScope.exibir}" />

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>TRE/PE - Estatísticas</title>

<script type="text/javascript" src="/layout_aplicacoes/js/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="/layout_aplicacoes/js/seginf_aplicacoes.js"></script>
	
	
	<link rel="stylesheet" href="/layout_aplicacoes/css/estilo_aplicacoes.css"  type="text/css" />
	<link rel="stylesheet" href="/layout_aplicacoes/css/estilo_mesario.css" type="text/css" />	


</head>
<body>
<div id="tudo">
<div id="topo">
		<h1 class="tre">
		<span class="destaque_sigla_tre">T</span>
		ribunal
		<span class="destaque_sigla_tre">R</span>
		egional
		<span class="destaque_sigla_tre">E</span>
		leitoral de Pernambuco
		</h1>
</div>
<c:choose>
	<c:when test="${exibir ne 1}">
		<div id="mesario">
			<div id="voltar_sair"><a href="./menu.jsp" id="voltar">Voltar</a></div>
			
			
			<h1>Estatísticas</h1>
			 <ul class="menu">	
				<li> <a href="./ServletConsultarEstatisticas.do?opcao=entidade">Consultar por Universidade</a> </li>
				
				<li> <a href="./ServletConsultarEstatisticas.do?opcao=ano"> Consultar por Ano</a> </li>
				
				<li> <a href="./periodoDeConvenio.jsp">Consultar por Período</a> </li>
			</ul>
		</div>
	</c:when>

	<c:otherwise>
		<div id="mesario">
			<div id="voltar_sair"><a href="./ServletConsultarEstatisticas.do" id="voltar">Voltar</a></div>
			<h1>Estatísticas</h1>
			<p class="destaque">Quantidade de mesários voluntários inscritos por <c:out value="${opcaoEstatistica}" />:</p>
			<table id="tabela_estatisticas" cellspacing="0">
				<thead>
					<tr>
						<!-- Ano ou instituição -->
						<c:choose>
							<c:when test="${opcaoEstatistica eq 'ano'}">
								<th id="ano">Ano</th>
							</c:when>
							
							<c:otherwise>
								<th id="entidade">Entidade</th>
								<th id="convenio">Convênio</th>
							</c:otherwise>
						</c:choose>
						
						<th id="quantidade">Quantidade</th>
					</tr>
				</thead>
				
				<tbody>
					
					<c:set var="total" value="0" />
					<c:choose>
						<c:when test="${opcaoEstatistica eq 'ano'}">
						<c:forEach var="ano" items="${requestScope.listaAno}">
							<tr>
								<td headers="ano">${ano.key}</td>
								<td headers="quantidade">${ano.value}</td>
								<c:set var="total" value="${total + ano.value}" />
							</tr>
						</c:forEach>
						</c:when>
						
						<c:otherwise>
							<c:forEach var="entidade" items="${requestScope.listaUniversidades}">
								<tr>
									<td headers="entidade"><c:if test="${entidade.nomeFantasia ne 'OUTRAS'}" >${entidade.sigla} - </c:if>${entidade.nomeFantasia}</td>
									<td headers="convenio">${entidade.convenioValido}</td>
									<td headers="quantidade">${entidade.quantidadeEleitores}</td>
									<c:set var="total" value="${total + entidade.quantidadeEleitores}" />
								</tr>
							</c:forEach>
							
						</c:otherwise>
					</c:choose>
					<tr >
						<c:choose>
							<c:when test="${opcaoEstatistica eq 'ano'}">
								<td class="destaque_total" headers="ano">Total</td>
							</c:when>
							<c:otherwise>
								<td class="destaque_total" headers="entidade">Total</td>
							</c:otherwise>
						</c:choose>
						<c:if test="${opcaoEstatistica eq 'entidade'}">
							<td headers="convenio"></td>
						</c:if>
						<td class="destaque_total" headers="ano">${total}</td>
					</tr>
					
				</tbody>
				
			</table>
		</div>
	</c:otherwise>
</c:choose>
	<div id="caixa_assinatura">
		<div id="assinatura"> STI / Coordenadoria de Desenvolvimento de Sistemas </div>
	</div>
</div>
</body>
</html>