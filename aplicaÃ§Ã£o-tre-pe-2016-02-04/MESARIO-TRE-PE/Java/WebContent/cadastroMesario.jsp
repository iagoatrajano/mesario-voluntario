<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@page import="mesario.controle.mesario.Eleitor"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Vector"%>
<%@page session="true"%>


<head>
<%
	String path = request.getContextPath();
	String baseServer = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort();
%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>TRE/PE - Cadastro de Mes�rio Volunt�rio</title>
<script type="text/javascript"	src="<%=baseServer%>/layout_aplicacoes/js/jquery.min.js"	charset="iso-8859-1"></script>
<script type="text/javascript"	src="<%=baseServer%>/layout_aplicacoes/js/seginf_aplicacoes.js"></script>
<script type="text/javascript" src="<%=baseServer%>/layout_aplicacoes/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" language="javascript"	src="js/javascript.js"></script>
<link href="<%=baseServer%>/layout_aplicacoes/css/estilo_aplicacoes.css" rel="stylesheet" type="text/css" />
<link href="<%=baseServer%>/layout_aplicacoes/css/estilo_mesario.css"     rel="stylesheet" type="text/css" />

<%
	String mensagem = (String) request.getAttribute("mensagem");
	String voluntarioCadastrado = (String) request
			.getAttribute("voluntarioCadastrado");
	Eleitor eleitor = (Eleitor) request.getAttribute("ELEITOR");
	List<Vector<String>> listaOcupacao = (ArrayList<Vector<String>>) request
			.getAttribute("LISTAOCUPACAO");

	if (eleitor != null) {
		session.setAttribute("ELEITOR", eleitor);
	} else {
		session.setAttribute("ELEITOR", null);
	}
	if (listaOcupacao != null) {
		session.setAttribute("LISTAOCUPACAO", listaOcupacao);
	} else {
		session.setAttribute("LISTAOCUPACAO", null);
	}
%>

<script language="javascript" type="text/javascript">
	//<!--	
	               	
	function confirmaCadastro()
	{
		if(document.form1.endereco.value != "" && document.form1.endereco.value.trim != ""
		  && document.form1.bairro.value != "" && document.form1.bairro.value.trim != ""
		  && document.form1.cidade.value != "" && document.form1.cidade.value.trim != "" 
		  
			){
	      
	      if(document.form1.cep.value != "" && document.form1.cep.value.trim != ""){		     
	        var cep = document.form1.cep.value;	       
	        document.form1.cep.value = document.form1.cep.value.replace(/\./gi, "").replace(/-/gi, "");	       
	        cep = parseInt(cep);
	        if (isNaN(cep)) {
	            //se nao for numero
	            alert("O Campo CEP deve ser todo numerico");
	            document.form1.cep.focus();
	            return false;
	        }
	        if(document.form1.cep.value.length < 8){
	        	alert("O campo CEP cont�m menos que oito d�gitos.");
	        	document.form1.cep.focus();
		        return false;
	        }
	        
	      }
		  //Verifica e-mails
		  if(!document.form1.email.value != "" && !document.form1.email.value.trim != "")
		  {
			  if(document.form1.confirmacao_email.value != "" && document.form1.confirmacao_email.value.trim != "")
		      {
				  alert("Os emails informados n�o s�o id�nticos.");
				  document.form1.email.focus();
				  return false;
			  }
		  }
		  if(document.form1.email.value != "" && document.form1.email.value.trim != "")
		  {
			  if(!document.form1.confirmacao_email.value != "" && !document.form1.confirmacao_email.value.trim != "")
		      {
				  alert("Os emails informados n�o s�o id�nticos.");
				  document.form1.confirmacao_email.focus();
				  return false;
			     
			  }else if(document.form1.email.value != document.form1.confirmacao_email.value)
		      {
		    	  alert("Os emails informados n�o s�o id�nticos.");
				  document.form1.confirmacao_email.focus();
				  return false;
		      }
		  }

		if (document.getElementById("telefone_principal").value == "") {
			alert("O telefone deve ser informado.");
			document.form1.telefone_principal.focus();
			return false;
		}
		/*if (document.getElementById("telefone_principal").value.length < 14) {
			alert("O Campo TELEFONE cont�m menos que dez d�gitos.");
	        document.form1.telefone_principal.focus();
	        return false;
	    }*/
	      
	    if (document.form1.email.value != "" && document.form1.email.value.trim != ""
	        && !mensagemMailValido(document.form1.email)){
	        document.form1.email.select();
	        return false;
	    }	  
	      
	      if (document.getElementById("sim_universitario").checked){
		      var analise = document.getElementById("analise");
	    	  if(analise.value == "selecione"){
		    	  alert("Por favor, selecione uma das universidades conveniadas.");
		    	  return false;
	    	  }   	   
		         
	      }		

		}else{
	    	alert ("OBS.: As informa��es marcadas com asterisco (*) s�o essenciais para o seu cadastramento como mes�rio.");
			return false;
	    }

		document.getElementById("idBtnCadastrar").disabled = "disabled";
		
	}
	
	function cancelaCadastro()
	{
		alert("Opera��o cancelada!");
		location.href="./informaTitulo.jsp";
	}
	
	function voluntarioCadastrado(msg)
	{
		alert(msg);
		location.href="./informaTitulo.jsp";
	}
	
	function exibirMensagem(msg)
	{
		alert(msg);
	}

	function verificarDisable(){		

		 var analise = document.getElementById("analise");
		
		 if (document.getElementById("nao_universitario").checked)  
         {   
	        analise.disabled = true;
         } 
		 else if(document.getElementById("sim_universitario").checked){
			 analise.disabled = false;
		 } 
			
	}
	
	
	//-->
	</script>

</head>

<%
	if (mensagem != null) {
%>
<body onload="javascript:exibirMensagem('<%=mensagem%>')"  id="sistemas_site">
<%
	} else if (voluntarioCadastrado != null) {
%>
<body
	onload="javascript:voluntarioCadastrado('<%=voluntarioCadastrado%>')" id="sistemas_site" >
<%
	} else {
%>
<body id="sistemas_site">
<%
	}
%>
<div id="tudo">
<div id="mesario">


		
		<h1>Cadastro de Mes�rios Volunt�rios</h1>
		<div>
		
		<p>N� do t�tulo do eleitor:
		<span class="ident">
		  <%
		 	if (eleitor != null && !eleitor.getTitulo().equals("")) {
		 %>
		 
		<%=eleitor.getTitulo()%> <%
		 	} else {
		 %> <%="N�o encontrado."%> <%
		 	}
		 %>
		 </span>
		</p>
		<p>Nome do eleitor:
		<span class="ident">
		  <%		 
		 	if (eleitor != null && !eleitor.getNome().equals("")) {
		 %>
		
		<%=eleitor.getNome()%> <%
		 	} else {
		 %> <%=" N�o encontrado."%> <%
		 	}
		 %>
		 </span>
		</p>
		<br />
		<%
			//Divide o nome em array de String para poder pegar o primeiro nome da pessoa
			// e mostr�-lo na frase abaixo.
			//String[] primeiroNome = eleitor.getNome().split(" ");
			String[] primeiroNome = null;
			if (eleitor != null) {
				primeiroNome = eleitor.getNome().split(" ");
			}
		%> <span class="fonte-azul">Ol�,
		<%
		 	if (primeiroNome != null) {
		 %><%=primeiroNome[0]%>
		<%
			}
		%>, por favor, preencha os dados abaixo e, ao final, clique em	Cadastrar.</span><br />		
		<br /> 
		<p><span class="obs">* Campos Obrigat�rios</span></p><br />
		</div>
		
		<form id="infoMesario" name="form1" method="post" action="./ServletCadastrarEleitor.do" onSubmit="return confirmaCadastro()">
						
			<p><label for="endereco">* Endere�o: </label><input id="endereco" name="endereco" type="text" value="" maxlength="72" ></input></p>
			
			<p class="inputs_inline">
				<span class="sem_rotulo">
					<label for="numero">N�mero: </label>
					<input id="numero" name="numero" type="text" value="" maxlength="8" class="numero" />

				<label for="complemento">Complemento: </label>
					<input id="complemento" name="complemento" type="text" value="" maxlength="20" />
				</span>
			</p>
			
			
			<p><label for="bairro">* Bairro:     </label><input id="bairro"   name="bairro"   type="text" value="" maxlength="60"></input></p>
			<p class="inputs_inline">
							<span class="sem_rotulo">			
								<label for="cidade">* Cidade:</label>
								<input id="cidade" type="text" value="" name="cidade" maxlength="50"></input>
								<label class="cep" for="cep">CEP: </label>
								<input id="cep" class="mascara-cep" type="text" value="" maxlength="10" name="cep"></input>
							</span>
			</p>
			
			
			<p>
				<label for="telefone_principal">* Telefone Principal: </label>
				<input id="telefone_principal" type="text" value="" maxlength="16" name="telefone_principal"></input>
			</p>
			<p>
				<label for="telefone_opcional">Telefone Opcional: </label>
				<input id="telefone_opcional" type="text" value="" maxlength="16" name="telefone_opcional"></input>
			</p>
			
			<p><label for="ocupacao">Ocupa��o:   </label> 
					<select	name="numOcupacao" id="ocupacao">
							<option value="000" selected="selected">Selecione</option>
							<%if (listaOcupacao != null) {
									for (int i = 0; i < listaOcupacao.size(); i++) {
										Vector ocupacao = (Vector) listaOcupacao.get(i);
							%>
							<option value="<%=ocupacao.get(0)%>"><%=ocupacao.get(1)%></option>
							<%
								}
								}
							%>
					</select>
					</p>					
					<p><label for="escolaridade">Escolaridade:</label> 
					<select	id="escolaridade" name="escolaridade">
							<option selected="selected" value="0">Selecione</option>
							<option value="1">Analfabeto</option>
							<option value="2">L� e Escreve</option>
							<option value="3">1� Grau Incompleto</option>
							<option value="4">1� Grau Completo</option>
							<option value="5">2� Grau Incompleto</option>
							<option value="6">2� Grau Completo</option>
							<option value="7">Superior Incompleto</option>
							<option value="8">Superior Completo</option>
						</select></p>
						
						<p><label for="email">Email: </label> 
						<input id="email" name="email" type="text" size="50" maxlength="50"/></p>
						
						<p><label for="confirmacao_email">Confirma��o do Email: </label> 
						<input id="confirmacao_email" name="confirmacao_email" type="text" size="50" maxlength="50"/></p>

			<p class="radios_inline">
				<span>J� trabalhou como mes�rio?</span>
							
					<span class="campos_inline">			
						<input id="sim_mesario" type="radio" name="trabalhoMesario" value="1" /><label for="sim_mesario">Sim</label>				
						<input id="nao_mesario" type="radio" name="trabalhoMesario" value="2" /><label for="nao_mesario">N�o</label>				
				    </span>		
			</p>
			
			<p class="radios_inline">		
				<span>� Servidor P�blico?</span>				
				<span class="campos_inline">			
					<input id="sim_servidor" type="radio" name="servidorPublico" value="1" /><label for="sim_servidor">Sim</label>				
					<input id="nao_servidor" type="radio" name="servidorPublico" value="2" /><label for="nao_servidor">N�o</label>			
				</span>		
			</p>		
					

			<p class="radios_inline">
				<span>� universit�rio?</span>			
				<span class="campos_inline">				
					<input id="sim_universitario" type="radio" name="e_universitario" value="sim" onclick="verificarDisable()" /><label for="sim_universitario">Sim</label>				
					<input id="nao_universitario" type="radio" name="e_universitario" value="nao" onclick="verificarDisable()" checked="checked" /><label for="nao_universitario">N�o</label>				
				</span>
			
			</p>	
			
		
			
			<p><label for="universidade">Universidade: </label>
			 <select id="analise" name="listaUniversidade" disabled="disabled">
				<option selected="selected" value="selecione">Selecione</option>
				<c:forEach items="${requestScope.listaUniversidade}" var="universidade">
					<option value="${universidade.codObjeto}">${universidade.sigla} - ${universidade.nomeFantasia}
					</option>
				</c:forEach>
				<option value="0">Outra</option>
			</select></p>

			<p>
				<input type="submit" value="Cadastrar" class="botoes_form" id="idBtnCadastrar"/>
				<input type="reset"  value="Limpar"    class="botoes_form"/>
			</p>


				
	    </form>

</div>
</div>
</body>
</html>

