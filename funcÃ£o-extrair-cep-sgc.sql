create or replace FUNCTION FUNC_EXTRAIR_CEP(texto varchar) RETURN NUMBER AS
temp_texto varchar(20);
BEGIN
if texto is null
then
  return null;
else
  temp_texto:=FUNC_EXTRAIR_MASCARA(texto);
  if FUNC_APENAS_NUMEROS(temp_texto)=1
  then
    return to_number(substr(temp_texto,1,8));
  else
    return null;
  end if;
end if;

END FUNC_EXTRAIR_CEP;