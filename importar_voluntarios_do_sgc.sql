create or replace procedure IMPORTAR_VOLUNTARIOS_SGC as
cursor voluntarios is select * from vw_voluntario_sgc;
voluntario vw_voluntario_sgc%rowtype;
begin

open voluntarios;
  loop
    FETCH voluntarios INTO voluntario;
    EXIT WHEN voluntarios%notfound;
    --
    --dbms_output.put_line(voluntario.titulo);
    --
    insert into eleitor values (
    to_char(s_eleitor_pk.nextval),
    voluntario.TITULO,
    voluntario.NOME,
    voluntario.DATA_NASC,
    voluntario.NOM_MAE,
    voluntario.ZONA_ELEITORAL,
    voluntario.NUM_SECAO,
    voluntario.NUM_MUNICIPIO_ZONA,
    voluntario.MUNICIPIO_ZONA,
    voluntario.NUM_LOCAL_VOTACAO,
    voluntario.LOCAL_VOTACAO,
    voluntario.DATA_CADASTRO,
    voluntario.ENDERECO,
    voluntario.BAIRRO,
    voluntario.CIDADE,
    voluntario.CEP,
    voluntario.DDD_FONE,
    voluntario.TELEFONE,
    voluntario.DDD_FONE_TRABALHO,
    voluntario.FONE_TRABALHO,
    voluntario.DDD_FONE_OPCIONAL,
    voluntario.FONE_OPCIONAL,
    voluntario.DDD_CELULAR,
    voluntario.CELULAR,
    voluntario.EMAIL,
    voluntario.NUM_OCUPACAO,
    voluntario.OCUPACAO,
    voluntario.SERVIDOR_PUBLICO,
    voluntario.FOI_MESARIO,
    voluntario.GRAU_INSTRUCAO,
    voluntario.RAMAL,
    voluntario.UNIVERSITARIO,
    voluntario.COD_OBJETO_UNIV_CONVENIADA
    );
  end loop;
close voluntarios;

end IMPORTAR_VOLUNTARIOS_SGC;